Stanchion build utilities
v1.17-0-g62ba14c
WARNING: GitBuilder.GitBuilder() is deprecated. Rather use RealtimeAppBuilder directly, and set your build number in property config->project->buildnumber
WARNING: Current git ref is not tagged with 'vx.y(.z)' and so your build number has been set to 00000

==============================================================================
 WARNING: This project targets SQL Server exclusively.
          Some TSQL Parser warnings will be suppressed.
==============================================================================


==============================================================================
Building FraudNID v1.9.00.00000-r5.3
==============================================================================

  Build started at 01/09/20 12:44:49.

[ Build Machine ]=============================================================

  Realtime Framework            : v5.3.02.319464

[ Project ]===================================================================

  Name    : FraudNID
  Version : v1.9.00.00000-r5.3

[ Events ]====================================================================


  [ Event Definitions ]-------------------------------------------------------

  Parsing event definitions ...
    [.\resources\events\events.er]
  Found 16 event(s)
  
  Generating Java classes

  [ Windows Event Log DLL ]---------------------------------------------------

  Compiling messages.mc ...
  Building message resource library ...
  Linking message resource DLL ...

[ Performance Monitoring Extensions ]=========================================

  Compiling Performance Monitoring Extension: FraudNID ...
  Building support event database scripts
  Building class publishing database scripts

==============================================================================
 Building Database scripts for datasource "realtime".
==============================================================================

  Converting platform independent database scripts for datasource "realtime"
    [.\source\sql\fraudnid_tbl.sql]
  Copying platform dependent database scripts for datasource "realtime"
  Converting new installation data scripts for datasource "realtime"
     [.\source\sql\fraudnid_data.sql]
     [.\build\sql\support_events\support_events_data.sql]
     [.\build\sql\publish_classes\published_classes_realtime_data.sql]
  Generating upgrade data scripts for the datasource "realtime"
  Building database upgrade data scripts for datasource "realtime"
  Converting upgrade data scripts for the datasource "realtime"
     [.\build\sql\support_events\support_events_upd_data_upgrade.sql]
     [.\build\sql\publish_classes\published_classes_realtime_data_upgrade.sql]

==============================================================================
 Collating database scripts and resolving dependencies.
==============================================================================

     [.\build\sql\intermediate\realtime_converted_tbl.tsql]
     [.\build\sql\intermediate\realtime_fraudnid_data.tsql]
     [.\build\sql\intermediate\realtime_published_classes_realtime_data.tsql]
     [.\build\sql\intermediate\realtime_published_classes_realtime_data_upgrade.tsql]
     [.\build\sql\intermediate\realtime_support_events_data.tsql]
     [.\build\sql\intermediate\realtime_support_events_upd_data_upgrade.tsql]
     [.\build\sql\intermediate\realtime_converted_tbl.db2sql]
     [.\build\sql\intermediate\realtime_fraudnid_data.db2sql]
     [.\build\sql\intermediate\realtime_published_classes_realtime_data.db2sql]
     [.\build\sql\intermediate\realtime_published_classes_realtime_data_upgrade.db2sql]
     [.\build\sql\intermediate\realtime_support_events_data.db2sql]
     [.\build\sql\intermediate\realtime_support_events_upd_data_upgrade.db2sql]
  Generating Placeholder Database Scripts

[ Version Class ]=============================================================

  Building version class ...

[ Java ]======================================================================

  Compiler         : C:\Program Files\Postilion\realtime\jdk\bin\javac.exe
  Flags            : -J-client -J-ms128m -J-mx256m -g
  Output Directory	: .\build\classes
  
  Class Path : 
    C:\Program Files\Postilion\realtime\java\classes
    C:\Program Files\Postilion\realtime\java\lib\activation.jar
    C:\Program Files\Postilion\realtime\java\lib\allaire_wddx.jar
    C:\Program Files\Postilion\realtime\java\lib\asm-3.1.jar
    C:\Program Files\Postilion\realtime\java\lib\comm.jar
    C:\Program Files\Postilion\realtime\java\lib\commons-logging.jar
    C:\Program Files\Postilion\realtime\java\lib\db2jcc.jar
    C:\Program Files\Postilion\realtime\java\lib\db2jcc_license_cu.jar
    C:\Program Files\Postilion\realtime\java\lib\dom4j-1.6.1.jar
    C:\Program Files\Postilion\realtime\java\lib\iaik_jce.jar
    C:\Program Files\Postilion\realtime\java\lib\iaik_ssl.jar
    C:\Program Files\Postilion\realtime\java\lib\jaxb-api.jar
    C:\Program Files\Postilion\realtime\java\lib\jaxb-impl.jar
    C:\Program Files\Postilion\realtime\java\lib\jaxb-xjc.jar
    C:\Program Files\Postilion\realtime\java\lib\jaxb1-impl.jar
    C:\Program Files\Postilion\realtime\java\lib\jaxen-1.1-beta-6.jar
    C:\Program Files\Postilion\realtime\java\lib\JSQLConnect.jar
    C:\Program Files\Postilion\realtime\java\lib\jsr173_1.0_api.jar
    C:\Program Files\Postilion\realtime\java\lib\jtds-1.2.jar
    C:\Program Files\Postilion\realtime\java\lib\junit.jar
    C:\Program Files\Postilion\realtime\java\lib\kspwc.jar
    C:\Program Files\Postilion\realtime\java\lib\log4j-1.2.14.jar
    C:\Program Files\Postilion\realtime\java\lib\mail.jar
    C:\Program Files\Postilion\realtime\java\lib\mina-core-1.1.7.jar
    C:\Program Files\Postilion\realtime\java\lib\org.eclipse.core.commands.jar
    C:\Program Files\Postilion\realtime\java\lib\org.eclipse.equinox.common.jar
    C:\Program Files\Postilion\realtime\java\lib\org.eclipse.equinox.preferences.jar
    C:\Program Files\Postilion\realtime\java\lib\org.eclipse.equinox.registry.jar
    C:\Program Files\Postilion\realtime\java\lib\org.eclipse.jface.jar
    C:\Program Files\Postilion\realtime\java\lib\org.eclipse.jface.text.jar
    C:\Program Files\Postilion\realtime\java\lib\pool.jar
    C:\Program Files\Postilion\realtime\java\lib\psbase.jar
    C:\Program Files\Postilion\realtime\java\lib\psdb2.jar
    C:\Program Files\Postilion\realtime\java\lib\psoracle.jar
    C:\Program Files\Postilion\realtime\java\lib\psresource.jar
    C:\Program Files\Postilion\realtime\java\lib\pssqlserver.jar
    C:\Program Files\Postilion\realtime\java\lib\psutil.jar
    C:\Program Files\Postilion\realtime\java\lib\quartz-all-1.5.2.jar
    C:\Program Files\Postilion\realtime\java\lib\slf4j-api-1.5.6.jar
    C:\Program Files\Postilion\realtime\java\lib\slf4j-log4j12-1.5.6.jar
    C:\Program Files\Postilion\realtime\java\lib\swt.jar
    C:\Program Files\Postilion\realtime\java\lib\vsi.jar
    C:\Program Files\Postilion\realtime\java\lib\w3c_http.jar
    C:\Program Files\Postilion\realtime\java\lib\xercesImpl.jar
    C:\Program Files\Postilion\realtime\java\lib\xml-apis.jar
  
  Source Directories : 
    .\source\java
    .\source\java\postilion
    .\source\java\postilion\realtime
    .\source\java\postilion\realtime\fraudnid
    .\source\java\postilion\realtime\fraudnid\cache
    .\source\java\postilion\realtime\fraudnid\config
    .\source\java\postilion\realtime\fraudnid\dao
    .\source\java\postilion\realtime\fraudnid\eventrecorder
    .\source\java\postilion\realtime\fraudnid\ipc
    .\source\java\postilion\realtime\fraudnid\msg
    .\source\java\postilion\realtime\fraudnid\perfmon
    .\source\java\postilion\realtime\fraudnid\safq
    .\source\java\postilion\realtime\fraudnid\util
    .\build\java
    .\build\java\postilion
    .\build\java\postilion\realtime
    .\build\java\postilion\realtime\fraudnid
    .\build\java\postilion\realtime\fraudnid\eventrecorder
    .\build\java\postilion\realtime\fraudnid\eventrecorder\events
    .\build\java\postilion\realtime\fraudnid\perfmon
  
  Compiling Java Source Code (53 source file(s))...
  
  Note: .\build\java\postilion\realtime\fraudnid\perfmon\ResourcesENGLISH.java uses unchecked or unsafe operations.
  Note: Recompile with -Xlint:unchecked for details.
  
  Compilation completed with 0 error(s) and 0 warning(s).

[ Classpath File ]============================================================

  #include ..\..\realtime\java\.classpath
  .\classes

[ Java Launcher ]=============================================================

  Generating Windows Launcher (postjava_fraudnid.exe) ...
  Generating AIX Launcher (postjava_fraudnid) ...

==============================================================================
 Building Documentation.
==============================================================================


[ Document "User Guide (userguide)" ]=========================================

   Project Name    : ug_fraudnid
   Source Filename : doc\userguide\ug_fraudnid.hhp
   Output Filename : .\build\doc\ug_fraudnid.chm
  
   Generating Windows Compiled Help File (CHM) ...
  
  
   Completed with 0 warnings and 0 errors.

[ Document "Release Notes (releasenotes)" ]===================================

   Project Name    : rn_fraudnid
   Source Filename : doc\releasenotes\rn_fraudnid.hhp
   Output Filename : .\build\doc\rn_fraudnid.chm
  
   Generating Windows Compiled Help File (CHM) ...
  
  
   Completed with 0 warnings and 0 errors.

==============================================================================
 Building Uninstall ...
==============================================================================

   Packaging "realtime" DataSource content ...
   Packaging Database content for datasource "realtime" ...

[ Uninstall ]=================================================================


==============================================================================
 Building custom install action jar.
==============================================================================

  
   Archiver           : C:\Program Files\Postilion\realtime\jdk\bin\jar.exe
   Flags              : -cvf ".\build\uninstall\custom\custom.jar"
   Base Directory     : .\build\uninstall\custom\classes
  
   Creating JAR file (0 class file(s))...
  
  
   Completed with 0 error(s) and 0 warning(s).
   Building project file "uninstall.xml" ...
   Building project file "init_custom_actions.xml" ...
   Building project file "init_platform.xml" ...
   Building project file "init_datasources.xml" ...
   Building project file "finalize_datasource_config.xml" ...
   Building project file "initialize_uninstall.xml" ...
   Building project file "check_uninstall_dependencies.xml" ...
   Building project file "uninstall_patches.xml" ...
   Building project file "uninstall_content.xml" ...
   Building project file "uninstall_events.xml" ...
   Building project file "uninstall_perfmon.xml" ...
   Building project file "uninstall_shortcuts.xml" ...
   Building project file "uninstall_registry.xml" ...
   Building project file "uninstall_database.xml" ...
   Building project file "uninstall_realtime_database.xml" ...
   Building project file "uninstall_datasources.xml" ...
   Building project file "uninstall_application_registry.xml" ...
   Building project file "uninstall_java.xml" ...
   Building project file "uninstall_application_files.xml" ...
   Building project file "uninstall_additional_content.xml" ...
   Building project file "uninstall_patches.xml" ...
   Building project file "uninstall_content.xml" ...
   Building project file "uninstall_events.xml" ...
   Building project file "uninstall_perfmon.xml" ...
   Building project file "uninstall_shortcuts.xml" ...
   Building project file "uninstall_registry.xml" ...
   Building project file "uninstall_database.xml" ...
   Building project file "uninstall_realtime_database.xml" ...
   Building project file "uninstall_datasources.xml" ...
   Building project file "uninstall_application_registry.xml" ...
   Building project file "uninstall_java.xml" ...
   Building project file "uninstall_application_files.xml" ...
   Building project file "uninstall_additional_content.xml" ...
   Building custom content ...
   Building content jar ...
   Archiver           : C:\Program Files\Postilion\realtime\jdk\bin\jar.exe
   Flags              : -cvf ".\build\uninstall\content.jar" ""
   Base Directory     : .\build\uninstall\content_jar
  
   Creating JAR file (0 file(s))...
  
  
   Completed with 0 error(s) and 0 warning(s).

==============================================================================
 Building Rollback ...
==============================================================================


[ Rollback ]==================================================================


==============================================================================
 Building custom install action jar.
==============================================================================

  
   Archiver           : C:\Program Files\Postilion\realtime\jdk\bin\jar.exe
   Flags              : -cvf ".\build\rollback\custom\custom.jar"
   Base Directory     : .\build\rollback\custom\classes
  
   Creating JAR file (0 class file(s))...
  
  
   Completed with 0 error(s) and 0 warning(s).
   Building project file "rollback.xml" ...
   Building project file "init_custom_actions.xml" ...
   Building project file "init_platform.xml" ...
   Building project file "init_datasources.xml" ...
   Building project file "finalize_datasource_config.xml" ...
   Building project file "initialize_rollback.xml" ...
   Building project file "check_rollback_dependencies.xml" ...
   Building project file "rollback_patches.xml" ...
   Building project file "rollback_content.xml" ...
   Building project file "rollback_perfmon.xml" ...
   Building project file "rollback_java.xml" ...
   Building project file "rollback_registry.xml" ...
   Building project file "rollback_events.xml" ...
   Building project file "rollback_database.xml" ...
   Building project file "rollback_realtime_database.xml" ...
   Building project file "rollback_datasources.xml" ...
   Building project file "rollback_shortcuts.xml" ...
   Building project file "rollback_application_files.xml" ...
   Building project file "rollback_application_registry.xml" ...
   Building project file "finalize_rollback.xml" ...
   Building custom content ...
   Building content jar ...
   Archiver           : C:\Program Files\Postilion\realtime\jdk\bin\jar.exe
   Flags              : -cvf ".\build\rollback\content.jar" ""
   Base Directory     : .\build\rollback\content_jar
  
   Creating JAR file (0 file(s))...
  
  
   Completed with 0 error(s) and 0 warning(s).

==============================================================================
 Building Installation ...
==============================================================================

  Building Install Framework Jar ...
   Building Executable Jar Manifest ...
  
   Archiver           : C:\Program Files\Postilion\realtime\jdk\bin\jar.exe
   Flags              : -cvfm ".\build\install\installfw\installfw.jar" ".\build\install\installfw\manifest\manifest.mf"
   Base Directory     : .\build\install\installfw\classes
   Manifest           : .\build\install\installfw\manifest\manifest.mf
  
   Creating JAR file (606 class file(s))...
  
  
   Completed with 0 error(s) and 0 warning(s).

[ Standard Edition Install ]==================================================


==============================================================================
 Building custom install action jar.
==============================================================================

  
   Archiver           : C:\Program Files\Postilion\realtime\jdk\bin\jar.exe
   Flags              : -cvf ".\build\install\standard_edition\custom\custom.jar"
   Base Directory     : .\build\install\standard_edition\custom\classes
  
   Creating JAR file (0 class file(s))...
  
  
   Completed with 0 error(s) and 0 warning(s).
   Cleaning output directory ...
   Building project file "install.xml" ...
   Building project file "init_custom_actions.xml" ...
   Building project file "init_platform.xml" ...
   Building project file "init_environment.xml" ...
   Building project file "init_services.xml" ...
   Building project file "init_datasources.xml" ...
   Building project file "init_database.xml" ...
   Building project file "check_dependencies.xml" ...
   Building project file "screenset_main.xml" ...
   Building project file "finalize_config.xml" ...
   Building project file "finalize_datasource_config.xml" ...
   Building project file "validate_license.xml" ...
   Building project file "backup_content.xml" ...
   Building project file "initialize_backup.xml" ...
   Building project file "backup_application_registry.xml" ...
   Building project file "backup_java.xml" ...
   Building project file "backup_registry.xml" ...
   Building project file "backup_events.xml" ...
   Building project file "backup_shortcuts.xml" ...
   Building project file "backup_application_files.xml" ...
   Building project file "install_content.xml" ...
   Building project file "install_destination_dir.xml" ...
   Building project file "install_uninstall.xml" ...
   Building project file "install_datasources.xml" ...
   Building project file "install_database.xml" ...
   Building project file "install_realtime_database.xml" ...
   Building project file "upgrade_database.xml" ...
   Building project file "upgrade_realtime_database.xml" ...
   Building project file "install_events.xml" ...
   Building project file "install_java.xml" ...
   Building project file "install_documentation.xml" ...
   Building project file "install_perfmon.xml" ...
   Building project file "finalize_content.xml" ...
   Building project file "register_application.xml" ...
   Building project file "register_rollback.xml" ...
   Building project file "register_uninstall.xml" ...
   Building project file "register_app_dependencies.xml" ...
   Building project file "screenset_final.xml" ...
   Building project file "finalize_install.xml" ...
   Building custom content ...
   Building project libraries ...
   Building Windows launcher ...
   running py2exe
   creating E:\eclipse-workspace\client-7-eleven-fraudintegrationnid\build\install\standard_edition\launcher\win32\build
   creating E:\eclipse-workspace\client-7-eleven-fraudintegrationnid\build\install\standard_edition\launcher\win32\build\bdist.win32
   creating E:\eclipse-workspace\client-7-eleven-fraudintegrationnid\build\install\standard_edition\launcher\win32\build\bdist.win32\winexe
   creating E:\eclipse-workspace\client-7-eleven-fraudintegrationnid\build\install\standard_edition\launcher\win32\build\bdist.win32\winexe\collect-2.3
   creating E:\eclipse-workspace\client-7-eleven-fraudintegrationnid\build\install\standard_edition\launcher\win32\build\bdist.win32\winexe\bundle-2.3
   creating E:\eclipse-workspace\client-7-eleven-fraudintegrationnid\build\install\standard_edition\launcher\win32\build\bdist.win32\winexe\temp
   *** searching for required modules ***
   *** parsing results ***
   *** finding dlls needed ***
   *** create binaries ***
   *** byte compile python files ***
   writing byte-compilation script 'c:\docume~1\post_a~1\locals~1\temp\tmpuv4ehu.py'
   C:\Python23\python.exe -OO c:\docume~1\post_a~1\locals~1\temp\tmpuv4ehu.py
   byte-compiling C:\Python23\lib\StringIO.py to StringIO.pyo
   byte-compiling C:\Python23\lib\UserDict.py to UserDict.pyo
   byte-compiling C:\Python23\lib\_strptime.py to _strptime.pyo
   byte-compiling C:\Python23\lib\atexit.py to atexit.pyo
   byte-compiling C:\Python23\lib\base64.py to base64.pyo
   byte-compiling C:\Python23\lib\calendar.py to calendar.pyo
   byte-compiling C:\Python23\lib\codecs.py to codecs.pyo
   byte-compiling C:\Python23\lib\copy.py to copy.pyo
   byte-compiling C:\Python23\lib\copy_reg.py to copy_reg.pyo
   byte-compiling C:\Python23\lib\dis.py to dis.pyo
   byte-compiling C:\Python23\lib\dummy_thread.py to dummy_thread.pyo
   byte-compiling C:\Python23\lib\encodings\__init__.py to encodings\__init__.pyo
   byte-compiling C:\Python23\lib\encodings\aliases.py to encodings\aliases.pyo
   byte-compiling C:\Python23\lib\fnmatch.py to fnmatch.pyo
   byte-compiling C:\Python23\lib\ftplib.py to ftplib.pyo
   byte-compiling C:\Python23\lib\getopt.py to getopt.pyo
   byte-compiling C:\Python23\lib\getpass.py to getpass.pyo
   byte-compiling C:\Python23\lib\glob.py to glob.pyo
   byte-compiling C:\Python23\lib\gopherlib.py to gopherlib.pyo
   byte-compiling C:\Python23\lib\httplib.py to httplib.pyo
   byte-compiling C:\Python23\lib\inspect.py to inspect.pyo
   byte-compiling C:\Python23\lib\linecache.py to linecache.pyo
   byte-compiling C:\Python23\lib\locale.py to locale.pyo
   byte-compiling C:\Python23\lib\macpath.py to macpath.pyo
   byte-compiling C:\Python23\lib\macurl2path.py to macurl2path.pyo
   byte-compiling C:\Python23\lib\mimetools.py to mimetools.pyo
   byte-compiling C:\Python23\lib\mimetypes.py to mimetypes.pyo
   byte-compiling C:\Python23\lib\new.py to new.pyo
   byte-compiling C:\Python23\lib\ntpath.py to ntpath.pyo
   byte-compiling C:\Python23\lib\nturl2path.py to nturl2path.pyo
   byte-compiling C:\Python23\lib\opcode.py to opcode.pyo
   byte-compiling C:\Python23\lib\os.py to os.pyo
   byte-compiling C:\Python23\lib\os2emxpath.py to os2emxpath.pyo
   byte-compiling C:\Python23\lib\popen2.py to popen2.pyo
   byte-compiling C:\Python23\lib\posixpath.py to posixpath.pyo
   byte-compiling C:\Python23\lib\py_compile.py to py_compile.pyo
   byte-compiling C:\Python23\lib\quopri.py to quopri.pyo
   byte-compiling C:\Python23\lib\random.py to random.pyo
   byte-compiling C:\Python23\lib\re.py to re.pyo
   byte-compiling C:\Python23\lib\repr.py to repr.pyo
   byte-compiling C:\Python23\lib\rfc822.py to rfc822.pyo
   byte-compiling C:\Python23\lib\shutil.py to shutil.pyo
   byte-compiling C:\Python23\lib\site-packages\wx-2.8-msw-unicode\wx\__init__.py to wx\__init__.pyo
   byte-compiling C:\Python23\lib\site-packages\wx-2.8-msw-unicode\wx\__version__.py to wx\__version__.pyo
   byte-compiling C:\Python23\lib\site-packages\wx-2.8-msw-unicode\wx\_controls.py to wx\_controls.pyo
   byte-compiling C:\Python23\lib\site-packages\wx-2.8-msw-unicode\wx\_core.py to wx\_core.pyo
   byte-compiling C:\Python23\lib\site-packages\wx-2.8-msw-unicode\wx\_gdi.py to wx\_gdi.pyo
   byte-compiling C:\Python23\lib\site-packages\wx-2.8-msw-unicode\wx\_misc.py to wx\_misc.pyo
   byte-compiling C:\Python23\lib\site-packages\wx-2.8-msw-unicode\wx\_windows.py to wx\_windows.pyo
   byte-compiling C:\Python23\lib\site-packages\zipextimporter.py to zipextimporter.pyo
   byte-compiling C:\Python23\lib\socket.py to socket.pyo
   byte-compiling C:\Python23\lib\sre.py to sre.pyo
   byte-compiling C:\Python23\lib\sre_compile.py to sre_compile.pyo
   byte-compiling C:\Python23\lib\sre_constants.py to sre_constants.pyo
   byte-compiling C:\Python23\lib\sre_parse.py to sre_parse.pyo
   byte-compiling C:\Python23\lib\stat.py to stat.pyo
   byte-compiling C:\Python23\lib\string.py to string.pyo
   byte-compiling C:\Python23\lib\tempfile.py to tempfile.pyo
   byte-compiling C:\Python23\lib\threading.py to threading.pyo
   byte-compiling C:\Python23\lib\token.py to token.pyo
   byte-compiling C:\Python23\lib\tokenize.py to tokenize.pyo
   byte-compiling C:\Python23\lib\traceback.py to traceback.pyo
   byte-compiling C:\Python23\lib\types.py to types.pyo
   byte-compiling C:\Python23\lib\urllib.py to urllib.pyo
   byte-compiling C:\Python23\lib\urllib2.py to urllib2.pyo
   byte-compiling C:\Python23\lib\urlparse.py to urlparse.pyo
   byte-compiling C:\Python23\lib\uu.py to uu.pyo
   byte-compiling C:\Python23\lib\warnings.py to warnings.pyo
   byte-compiling C:\Python23\lib\weakref.py to weakref.pyo
   byte-compiling C:\Python23\lib\xml\__init__.py to xml\__init__.pyo
   byte-compiling C:\Python23\lib\xml\dom\NodeFilter.py to xml\dom\NodeFilter.pyo
   byte-compiling C:\Python23\lib\xml\dom\__init__.py to xml\dom\__init__.pyo
   byte-compiling C:\Python23\lib\xml\dom\domreg.py to xml\dom\domreg.pyo
   byte-compiling C:\Python23\lib\xml\dom\expatbuilder.py to xml\dom\expatbuilder.pyo
   byte-compiling C:\Python23\lib\xml\dom\minicompat.py to xml\dom\minicompat.pyo
   byte-compiling C:\Python23\lib\xml\dom\minidom.py to xml\dom\minidom.pyo
   byte-compiling C:\Python23\lib\xml\dom\pulldom.py to xml\dom\pulldom.pyo
   byte-compiling C:\Python23\lib\xml\dom\xmlbuilder.py to xml\dom\xmlbuilder.pyo
   byte-compiling C:\Python23\lib\xml\parsers\__init__.py to xml\parsers\__init__.pyo
   byte-compiling C:\Python23\lib\xml\parsers\expat.py to xml\parsers\expat.pyo
   byte-compiling C:\Python23\lib\xml\sax\__init__.py to xml\sax\__init__.pyo
   byte-compiling C:\Python23\lib\xml\sax\_exceptions.py to xml\sax\_exceptions.pyo
   byte-compiling C:\Python23\lib\xml\sax\expatreader.py to xml\sax\expatreader.pyo
   byte-compiling C:\Python23\lib\xml\sax\handler.py to xml\sax\handler.pyo
   byte-compiling C:\Python23\lib\xml\sax\saxutils.py to xml\sax\saxutils.pyo
   byte-compiling C:\Python23\lib\xml\sax\xmlreader.py to xml\sax\xmlreader.pyo
   byte-compiling C:\Python23\lib\zipfile.py to zipfile.pyo
   byte-compiling E:\eclipse-workspace\client-7-eleven-fraudintegrationnid\build\install\standard_edition\launcher\win32\postsetup.py to postsetup.pyo
   removing c:\docume~1\post_a~1\locals~1\temp\tmpuv4ehu.py
   *** copy extensions ***
   copying C:\Python23\DLLs\_socket.pyd -> E:\eclipse-workspace\client-7-eleven-fraudintegrationnid\build\install\standard_edition\launcher\win32\build\bdist.win32\winexe\collect-2.3
   copying C:\Python23\DLLs\_sre.pyd -> E:\eclipse-workspace\client-7-eleven-fraudintegrationnid\build\install\standard_edition\launcher\win32\build\bdist.win32\winexe\collect-2.3
   copying C:\Python23\DLLs\_ssl.pyd -> E:\eclipse-workspace\client-7-eleven-fraudintegrationnid\build\install\standard_edition\launcher\win32\build\bdist.win32\winexe\collect-2.3
   copying C:\Python23\DLLs\_winreg.pyd -> E:\eclipse-workspace\client-7-eleven-fraudintegrationnid\build\install\standard_edition\launcher\win32\build\bdist.win32\winexe\collect-2.3
   copying C:\Python23\DLLs\datetime.pyd -> E:\eclipse-workspace\client-7-eleven-fraudintegrationnid\build\install\standard_edition\launcher\win32\build\bdist.win32\winexe\collect-2.3
   copying C:\Python23\DLLs\pyexpat.pyd -> E:\eclipse-workspace\client-7-eleven-fraudintegrationnid\build\install\standard_edition\launcher\win32\build\bdist.win32\winexe\collect-2.3
   copying C:\Python23\DLLs\zlib.pyd -> E:\eclipse-workspace\client-7-eleven-fraudintegrationnid\build\install\standard_edition\launcher\win32\build\bdist.win32\winexe\collect-2.3
   copying C:\Python23\lib\site-packages\win32\win32api.pyd -> E:\eclipse-workspace\client-7-eleven-fraudintegrationnid\build\install\standard_edition\launcher\win32\build\bdist.win32\winexe\collect-2.3
   copying C:\Python23\lib\site-packages\wx-2.8-msw-unicode\wx\_controls_.pyd -> E:\eclipse-workspace\client-7-eleven-fraudintegrationnid\build\install\standard_edition\launcher\win32\build\bdist.win32\winexe\collect-2.3\wx
   copying C:\Python23\lib\site-packages\wx-2.8-msw-unicode\wx\_core_.pyd -> E:\eclipse-workspace\client-7-eleven-fraudintegrationnid\build\install\standard_edition\launcher\win32\build\bdist.win32\winexe\collect-2.3\wx
   copying C:\Python23\lib\site-packages\wx-2.8-msw-unicode\wx\_gdi_.pyd -> E:\eclipse-workspace\client-7-eleven-fraudintegrationnid\build\install\standard_edition\launcher\win32\build\bdist.win32\winexe\collect-2.3\wx
   copying C:\Python23\lib\site-packages\wx-2.8-msw-unicode\wx\_misc_.pyd -> E:\eclipse-workspace\client-7-eleven-fraudintegrationnid\build\install\standard_edition\launcher\win32\build\bdist.win32\winexe\collect-2.3\wx
   copying C:\Python23\lib\site-packages\wx-2.8-msw-unicode\wx\_windows_.pyd -> E:\eclipse-workspace\client-7-eleven-fraudintegrationnid\build\install\standard_edition\launcher\win32\build\bdist.win32\winexe\collect-2.3\wx
   *** copy dlls ***
   copying C:\Python23\lib\site-packages\wx-2.8-msw-unicode\wx\wxmsw28uh_adv_vc.dll -> E:\eclipse-workspace\client-7-eleven-fraudintegrationnid\build\install\standard_edition\launcher\win32\build\bdist.win32\winexe\collect-2.3
   copying C:\Python23\lib\site-packages\wx-2.8-msw-unicode\wx\wxmsw28uh_core_vc.dll -> E:\eclipse-workspace\client-7-eleven-fraudintegrationnid\build\install\standard_edition\launcher\win32\build\bdist.win32\winexe\collect-2.3
   copying C:\Python23\lib\site-pac  adding: S1/Realtime/FraudNID_v1.9_build00000_r5.3/setup.exe (deflated 16%)
  adding: S1/Realtime/FraudNID_v1.9_build00000_r5.3/setupc.exe (deflated 12%)
  adding: S1/Realtime/FraudNID_v1.9_build00000_r5.3/ug_fraudnid.chm (deflated 8%)
  adding: S1/Realtime/FraudNID_v1.9_build00000_r5.3/rn_fraudnid.chm (deflated 61%)
  adding: S1/Realtime/FraudNID_v1.9_build00000_r5.3/readme.txt (deflated 56%)
Welcome to the Winzip SE python masquerade
   Input file is FraudNID_v1.9_build00000_r5.3.zip
   Output file is FraudNID_v1.9_build00000_r5.3.exe
   Title is FraudNID v1.9 build00000 r5.3
Output of MakeSFX:
--------------------Compiling: Stanchion Self Extractor---------------------

Reading source ZIP file...

Saving archive settings...

Finishing up ...





Stanchion Self Extractor - 0 error(s), 0 warning(s)



Build time: 0.45 seconds (23.94 MB/S)

kages\wx-2.8-msw-unicode\wx\wxbase28uh_vc.dll -> E:\eclipse-workspace\client-7-eleven-fraudintegrationnid\build\install\standard_edition\launcher\win32\build\bdist.win32\winexe\collect-2.3
   copying C:\Python23\w9xpopen.exe -> E:\eclipse-workspace\client-7-eleven-fraudintegrationnid\build\install\standard_edition\launcher\win32
   copying C:\WINDOWS\system32\python23.dll -> E:\eclipse-workspace\client-7-eleven-fraudintegrationnid\build\install\standard_edition\launcher\win32\build\bdist.win32\winexe\bundle-2.3
   setting sys.winver for 'E:\eclipse-workspace\client-7-eleven-fraudintegrationnid\build\install\standard_edition\launcher\win32\build\bdist.win32\winexe\bundle-2.3\python23.dll' to 'py2exe'
   copying C:\WINDOWS\system32\pywintypes23.dll -> E:\eclipse-workspace\client-7-eleven-fraudintegrationnid\build\install\standard_edition\launcher\win32\build\bdist.win32\winexe\collect-2.3
   copying C:\Python23\lib\site-packages\wx-2.8-msw-unicode\wx\wxbase28uh_net_vc.dll -> E:\eclipse-workspace\client-7-eleven-fraudintegrationnid\build\install\standard_edition\launcher\win32\build\bdist.win32\winexe\collect-2.3
   copying C:\Python23\lib\site-packages\wx-2.8-msw-unicode\wx\wxmsw28uh_html_vc.dll -> E:\eclipse-workspace\client-7-eleven-fraudintegrationnid\build\install\standard_edition\launcher\win32\build\bdist.win32\winexe\collect-2.3
   copying C:\Python23\lib\site-packages\py2exe\run.exe -> E:\eclipse-workspace\client-7-eleven-fraudintegrationnid\build\install\standard_edition\launcher\win32\setupc.exe
   Adding python23.dll as resource to E:\eclipse-workspace\client-7-eleven-fraudintegrationnid\build\install\standard_edition\launcher\win32\setupc.exe
   Adding zlib.pyd as resource to E:\eclipse-workspace\client-7-eleven-fraudintegrationnid\build\install\standard_edition\launcher\win32\setupc.exe
   copying C:\Python23\lib\site-packages\py2exe\run_w.exe -> E:\eclipse-workspace\client-7-eleven-fraudintegrationnid\build\install\standard_edition\launcher\win32\setup.exe
   Adding python23.dll as resource to E:\eclipse-workspace\client-7-eleven-fraudintegrationnid\build\install\standard_edition\launcher\win32\setup.exe
   Adding zlib.pyd as resource to E:\eclipse-workspace\client-7-eleven-fraudintegrationnid\build\install\standard_edition\launcher\win32\setup.exe
   
   *** binary dependencies ***
   Your executable(s) also depend on these dlls which are not included,
   you may or may not need to distribute them.
   
   Make sure you have the license if you distribute any of them, and
   make sure you don't distribute files belonging to the operating system.
   
      OLEAUT32.dll - C:\WINDOWS\system32\OLEAUT32.dll
      USER32.dll - C:\WINDOWS\system32\USER32.dll
      POWRPROF.dll - C:\WINDOWS\system32\POWRPROF.dll
      SHELL32.dll - C:\WINDOWS\system32\SHELL32.dll
      ole32.dll - C:\WINDOWS\system32\ole32.dll
      WINMM.dll - C:\WINDOWS\system32\WINMM.dll
      MSVCRT.dll - C:\Python23\MSVCRT.dll
      comdlg32.dll - C:\WINDOWS\system32\comdlg32.dll
      ADVAPI32.dll - C:\WINDOWS\system32\ADVAPI32.dll
      GDI32.dll - C:\WINDOWS\system32\GDI32.dll
      MSVCRT.dll - C:\WINDOWS\system32\MSVCRT.dll
      COMCTL32.dll - C:\WINDOWS\system32\COMCTL32.dll
      VERSION.dll - C:\WINDOWS\system32\VERSION.dll
      KERNEL32.dll - C:\WINDOWS\system32\KERNEL32.dll
      WSOCK32.dll - C:\WINDOWS\system32\WSOCK32.dll
      MSVCP60.dll - C:\Python23\lib\site-packages\wx-2.8-msw-unicode\wx\MSVCP60.dll
      RPCRT4.dll - C:\WINDOWS\system32\RPCRT4.dll
   Building AIX launcher ...

==============================================================================
 Packaging install
==============================================================================


[ Self-extracting zip file for Windows ]======================================

Write value  78c5a56b84c21ffe45d57ed46119bc48  to file  ..\FraudNID_v1.9_build00000_r5.3.exe.md5.txt
   Not packaging an AIX release because this is a SQL Server only application

==============================================================================
Build completed in 33.93 seconds.
==============================================================================

