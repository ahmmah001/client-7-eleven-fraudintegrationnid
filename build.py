import os
import sys
import time
import GitUtils
import GitBuilder
import RealtimeAppBuilder

#==============================================================================#
# Realtime Framework Builder Configuration                                     #
#==============================================================================#

config = {}
__YEAR__ = time.strftime("%Y")

#------------------------------------------------------------------------------#
# Project																	   #
#------------------------------------------------------------------------------#

config["Project"] = \
{
	"Name" : "FraudNID",
	"Version" : "v1.10-r5.3",
	"TargetSQLServerOnly" : True,
}

#------------------------------------------------------------------------------#
# Events																	   #
#------------------------------------------------------------------------------#

config["Events"] = \
{
	"EventSources" : ["FraudNID"],
}

#------------------------------------------------------------------------------#
# Java 																		   #
#------------------------------------------------------------------------------#

config["Java"] = \
{
    "BasePackage" : "postilion.realtime.fraudnid",
    "SourceDirs" :   \
    [
        (".\\source\\java", RealtimeAppBuilder.INCLUDE_RECURSE),
    ],
    "RegisteredClasses" : \
    [
        "postilion.realtime.fraudnid.FraudNID",
    ]
}

#------------------------------------------------------------------------------#
# Documentation                                                                #
#------------------------------------------------------------------------------#

config["Documentation"] = \
{
    "userguide" :
    {
        "Name"                      : "User Guide",
        "Type"                      : "CHM",
        "SourceDir"                 : "doc\\userguide",
        "Project"                   : "ug_fraudnid",
        "Replacements"              :
        {
            "Files"                 : ["Title.htm"]
        }
    },
    "releasenotes" :
    {
        "Name"                      : "Release Notes",
        "Type"                      : "CHM",
        "SourceDir"                 : "doc\\releasenotes",
        "Project"                   : "rn_fraudnid",
        "Replacements"              :
            {
                "Files" : ["rn_fraudnid.html"],
                "Map"   :   \
                [
                    ("__LAST_MODIFIED__", GitUtils.getReleaseDate()),
                    ("__YEAR__", __YEAR__),
                ]
            }
    },
}

#------------------------------------------------------------------------------#
# Database  																   #
#------------------------------------------------------------------------------#

config["Database"] = \
	[
		(
			"realtime",
			{
                "CreateDatabase" : False,
                "SourceDirs" : \
                [
                    (".\\source\\sql", RealtimeAppBuilder.INCLUDE_NO_RECURSE),
                ],
				"InstallObjects"  : \
				[
					("SQL",
						[
							("STATEMENT", "INSERT INTO cfg_custom_classes (unique_name, category, display_name, class_name, parameters, description) VALUES ('NODE INTEGRATION:FraudNID', 'NODE INTEGRATION', 'FraudNID', 'postilion.realtime.fraudnid.FraudNID', '<parameters><MapperClassName>postilion.realtime.fraudnid.msg.Mapper_v1</MapperClassName></parameters>', 'Pre-authorizes transactions via the Fraud Management System.')", RealtimeAppBuilder.DB_PLATFORM_TSQL_ONLY),
						]
					)
				],
				"UninstallObjects" : \
				[
					("SQL",
						[
							("STATEMENT", "DELETE FROM tm_integration_pipeline where integration_driver = 'NODE INTEGRATION:FraudNID'"),
							("STATEMENT", "DELETE FROM cfg_custom_classes where unique_name = 'NODE INTEGRATION:FraudNID'"),
							("STATEMENT", "DELETE FROM cfg_class_registry where class_name = 'postilion.realtime.fraudnid.FraudNID'"),
						]
					),
				],
			}
		)
	]

#------------------------------------------------------------------------------#
# PerfMon                                                                      #
#------------------------------------------------------------------------------#

config["PerfMon"] = \
{
    "Documentation" : "doc\\userguide\\Performance_Counters.htm",
    "Definitions" : \
    [
        {
            "ImportPerfDefns"   : ".\\resources\\perfmon\\fraudnid.py"
        },
    ]
}

#------------------------------------------------------------------------------#
# Release																	   #
#------------------------------------------------------------------------------#

config["Release"] = \
{
	"Packaging"				: \
	[
		(RealtimeAppBuilder.WINDOWS_ONLY, "build\\install\\standard_edition\\setup.exe", "setup.exe"),
		(RealtimeAppBuilder.WINDOWS_ONLY, "build\\install\\standard_edition\\setupc.exe", "setupc.exe"),
		("ALL_PLATFORMS", "build\\doc\\ug_fraudnid.chm", "ug_fraudnid.chm"),
		("ALL_PLATFORMS", "build\\doc\\rn_fraudnid.chm", "rn_fraudnid.chm"),
		("ALL_PLATFORMS", "doc\\readme.txt", "readme.txt"),
	]
}

#==============================================================================#
# Realtime Framework Builder												   #
#==============================================================================#

# Get the target and any target arguments that are present
target = RealtimeAppBuilder.getTargetName(sys.argv)
target_args = RealtimeAppBuilder.getTargetArguments(sys.argv)

# Build the target project.
GitBuilder.GitBuilder(config).buildProject(build_target=target , build_target_args = target_args)
