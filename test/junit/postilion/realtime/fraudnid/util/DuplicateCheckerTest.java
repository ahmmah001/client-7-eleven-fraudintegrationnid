/*----------------------------------------------------------------------------*\
 *                                                                            *
 * Copyright (c) 2019 by Montant Limited. All rights reserved.                *
 *                                                                            *
 * This software is the confidential and proprietary property of              *
 * Montant Limited and may not be disclosed, copied or distributed            *
 * in any form without the express written permission of Montant Limited.     *
 *                                                                            *
\*----------------------------------------------------------------------------*/
package postilion.realtime.fraudnid.util;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

public class DuplicateCheckerTest
{
	@BeforeClass
	public static void setupClass()
	{
		DuplicateChecker.INSTANCE.clear();
	}

	@Test
	public void test() throws InterruptedException
	{
		assertEquals("Expect -1 for no hit", -1, DuplicateChecker.INSTANCE.isDuplicated("NewKey", 10000l));
		assertTrue("Expect a timestamp for a hit", DuplicateChecker.INSTANCE.isDuplicated("NewKey", 10000l) > -1);
		assertEquals("Expect -1 for no hit", -1, DuplicateChecker.INSTANCE.isDuplicated("NewKey2", 500));
		Thread.sleep(510);
		assertEquals("Expect -1 for expired hit", -1, DuplicateChecker.INSTANCE.isDuplicated("NewKey2", 500));
		assertTrue("Expect a timestamp for a hit", DuplicateChecker.INSTANCE.isDuplicated("NewKey2", 10000l) > -1);
	}
}
