/*----------------------------------------------------------------------------*\
 *                                                                            *
 * Copyright (c) 2019 by Montant Limited. All rights reserved.                *
 *                                                                            *
 * This software is the confidential and proprietary property of              *
 * Montant Limited and may not be disclosed, copied or distributed            *
 * in any form without the express written permission of Montant Limited.     *
 *                                                                            *
\*----------------------------------------------------------------------------*/
package postilion.realtime.fraudnid.util;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;

import postilion.realtime.fraudnid.eventrecorder.TestLogger;
import postilion.realtime.fraudnid.msg.Iso8583FMS.MsgType;
import postilion.realtime.fraudnid.util.TimerManager.TimerUser;
import postilion.realtime.sdk.message.bitmap.Iso8583Post;
import postilion.realtime.sdk.message.bitmap.Iso8583Rev93.Bit;
import postilion.realtime.sdk.util.XPostilion;

public class TimerManagerTest
	implements
	TimerUser
{
	static TimerManager	cut;
	boolean					failed	= false;

	@BeforeClass
	public static void setupClass()
	{
		cut = TimerManager.INSTANCE;
		cut.setLogger(new TestLogger());
	}

	@After
	public void after()
	{
		if (failed)
		{
			fail("A failure was recorded - please review the output");
		}
	}
	
	@Test
	public void testEchoData() throws XPostilion
	{
		Iso8583Post msg = new Iso8583Post();
		msg.putMsgType(MsgType._0100_AUTH_REQ);
		msg.putField(Bit._002_PAN, "098765432123456");
		cut.startTimer(new StanchionTimer("EchoData1", 100, msg), this);
		waitFor(150);
	}

	@Test
	public void testExpiryTimes()
	{
		long sleepTime = 0;
		for (long l = 1; l < 10000; l += 333)
		{
			cut.startTimer(new StanchionTimer(String.valueOf(l), l, this), this);
			if (sleepTime < l)
			{
				sleepTime = l;
			}
		}
		// Let the timers expire
		waitFor(sleepTime + 50);
	}

	@Test
	public void testExpiryTimesDescending()
	{
		long sleepTime = 0;
		for (long l = 15000; l > 0; l -= 299)
		{
			cut.startTimer(new StanchionTimer(String.valueOf(l), l, this), this);
			if (sleepTime < l)
			{
				sleepTime = l;
			}
		}
		// Let the timers expire
		waitFor(sleepTime + 50);
	}

	@Test
	public void testStopNonexistentTimer() throws Exception
	{
		// Just happy it doesn't throw an exception
		cut.stopTimer("Doesn't exist");
	}

	@Test
	public void testStopExistingTimer() throws Exception
	{
		long sleepTime = 0;
		String stopper = null;
		for (long l = 10000; l > 0; l -= 299)
		{
			if (l % 2 == 0)
			{
				// Some timers must be stopped
				stopper = "stop" + String.valueOf(l);
				cut.startTimer(new StanchionTimer(stopper, l, this), this);
			}
			else
			{
				cut.startTimer(new StanchionTimer(String.valueOf(l), l, this), this);
				if (stopper != null)
				{
					cut.stopTimer(stopper);
					stopper = null;
				}
				if (sleepTime < l)
				{
					sleepTime = l;
				}
			}
		}
		// Let the timers expire
		waitFor(sleepTime + 50);
	}

	private void waitFor(long ms)
	{
		try
		{
			Thread.sleep(ms);
		}
		catch(InterruptedException e)
		{
		}
	}

	@Override
	public void onTimerExpired(StanchionTimer timer, long elapsedTime)
	{
		System.out.println("Timer " + timer + " expired");
		String idS = timer.getId();
		if (idS.startsWith("stop"))
		{
			// Stop cases
			failed = true;
			fail("Timer " + timer + " shouldn't have expired, but did so after " + elapsedTime + "ms.");
		}
		else
		{
			// Timers shouldn't expire more than somewhere around this many
			// milliseconds after they should have
			long upperBound = elapsedTime + 20;
			long period = timer.getPeriod();
			if (elapsedTime > upperBound)
			{
				failed = true;
				fail("Timer " + timer.getId() + " should have expired much sooner. Elapsed time was " + elapsedTime
					+ " but timeout was " + period);
			}
			if (elapsedTime < period)
			{
				failed = true;
				fail("Timer " + timer.getId() + " expired too soon. Elapsed time was " + elapsedTime + " but timeout was " + period);
			}
		}
		// Specific tests
		if(timer.getId().equals("EchoData1"))
		{
			assertNotNull("Echo data must not be null", timer.getEchoData());
			assertTrue("Echo data must be the message we attached", timer.getEchoData() instanceof Iso8583Post);
			assertEquals("Message must match the one we attached", MsgType._0100_AUTH_REQ, ((Iso8583Post)timer.getEchoData()).getMsgType());
			try
			{
				assertEquals("Message must match the one we attached", "098765432123456", ((Iso8583Post)timer.getEchoData()).getField(Bit._002_PAN));
			}
			catch(XPostilion e)
			{
				e.printStackTrace();
				fail("Caught " + e.getClass().getSimpleName());
			}
		}
	}
}
