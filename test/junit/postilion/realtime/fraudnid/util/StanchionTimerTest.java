/*----------------------------------------------------------------------------*\
 *                                                                            *
 * Copyright (c) 2019 by Montant Limited. All rights reserved.                *
 *                                                                            *
 * This software is the confidential and proprietary property of              *
 * Montant Limited and may not be disclosed, copied or distributed            *
 * in any form without the express written permission of Montant Limited.     *
 *                                                                            *
\*----------------------------------------------------------------------------*/
package postilion.realtime.fraudnid.util;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import org.junit.Test;

public class StanchionTimerTest
{
	@Test
	public void testEquals()
	{
		StanchionTimer t1 = new StanchionTimer("Timer1", 1000, null);
		StanchionTimer t2 = new StanchionTimer("Timer1", 2000, null);
		assertTrue("Timers should be equal because the ID is the same", t1.equals(t2));
		assertEquals("Timers should be equal because the ID is the same", t1, t2);
	}

	@Test
	public void testNotEquals()
	{
		StanchionTimer t1 = new StanchionTimer("Timer1", 1000, null);
		StanchionTimer t2 = new StanchionTimer("Timer2", 2000, null);
		assertFalse("Timers should be different because ID is different", t1.equals(t2));
		assertThat("Timers should be different because ID is different", t1, not(t2));
	}
}
