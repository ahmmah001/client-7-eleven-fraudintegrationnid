/*----------------------------------------------------------------------------*\
 *                                                                            *
 * Copyright (c) 2019 by Montant Limited. All rights reserved.                *
 *                                                                            *
 * This software is the confidential and proprietary property of              *
 * Montant Limited and may not be disclosed, copied or distributed            *
 * in any form without the express written permission of Montant Limited.     *
 *                                                                            *
\*----------------------------------------------------------------------------*/

package postilion.realtime.fraudnid.msg;

import static org.junit.Assert.*;

import org.junit.Test;

public class Iso8583FMSTest
{
	@Test
	public void testMD5Hash() throws Exception
	{
		String input = "1234567890123";
		String result = Iso8583FMS.getHashValue(input, "MD5");
		System.out.println(result);
		assertEquals("ee76971984d25138a199ac90553401dd", result);
	}
	
	@Test
	public void testSHA256Hash() throws Exception
	{
		String input = "1234567890123";
		String result = Iso8583FMS.getHashValue(input, "SHA-256");
		System.out.println(result);
		assertEquals("bca2b41a2b25e137c83fee346af7bd1e0f52bd560583ca07a1b42f9944c5c50b", result);
	}
}
