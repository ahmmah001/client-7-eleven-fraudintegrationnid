/*----------------------------------------------------------------------------*\
 *                                                                            *
 * Copyright (c) 2018 by Montant Limited. All rights reserved.                *
 *                                                                            *
 * This software is the confidential and proprietary property of              *
 * Montant Limited and may not be disclosed, copied or distributed            *
 * in any form without the express written permission of Montant Limited.     *
 *                                                                            *
\*----------------------------------------------------------------------------*/
package postilion.realtime.fraudnid.safq;

import static org.junit.Assert.*;

import org.junit.Test;

import postilion.realtime.fraudnid.eventrecorder.TestLogger;

public class CleanerTest
{
	Cleaner cut = new Cleaner(1000, "TestSink", new TestLogger(), 1);

	@Test
	public void testSomeRunsAndStop() throws InterruptedException
	{
		cut.testMode = true;
		cut.exception = false;
		new Thread(cut).start();;
		Thread.sleep(1005);
		assertEquals(1, cut.cleans);
		Thread.sleep(1005);
		assertEquals(2, cut.cleans);
		cut.stop();
		Thread.sleep(1005);
		assertEquals(3, cut.cleans);
		assertTrue(cut.stopped);
	}
	
	@Test
	public void testExceptionsRuns() throws InterruptedException {
		cut.testMode = true;
		cut.exception = true;
		new Thread(cut).start();;
		Thread.sleep(1010);
		assertEquals(1, cut.cleans);
		Thread.sleep(2000);
		assertEquals(2, cut.cleans);
		cut.stop();
		Thread.sleep(1010);
		assertEquals(3, cut.cleans);
		assertTrue(cut.stopped);
	}
	
	@Test
	public void testPeriodChange() throws InterruptedException {
		cut.testMode = true;
		cut.exception = false;
		cut.setPeriod(2000);
		new Thread(cut).start();;
		Thread.sleep(2010);
		assertEquals(1, cut.cleans);
		cut.setPeriod(1000);
		Thread.sleep(2020);
		assertEquals(3, cut.cleans);
		Thread.sleep(1010);
		assertEquals(4, cut.cleans);
		cut.setPeriod(3000);
		Thread.sleep(3010);
		assertEquals(5, cut.cleans);
		cut.stop();
		Thread.sleep(1010);
		assertTrue(cut.stopped);
	}
}
