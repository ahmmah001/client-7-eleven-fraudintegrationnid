/*----------------------------------------------------------------------------*\
 *                                                                            *
 * Copyright (c) 2018 by Montant Limited. All rights reserved.                *
 *                                                                            *
 * This software is the confidential and proprietary property of              *
 * Montant Limited and may not be disclosed, copied or distributed            *
 * in any form without the express written permission of Montant Limited.     *
 *                                                                            *
\*----------------------------------------------------------------------------*/
package postilion.realtime.fraudnid.eventrecorder;

import postilion.realtime.fraudnid.eventrecorder.ILogger;
import postilion.realtime.fraudnid.msg.Iso8583FMS;
import postilion.realtime.sdk.eventrecorder.AEventRecord;
import postilion.realtime.sdk.ipc.IEndpoint;

public class TestLogger
	implements
	ILogger
{
	@Override
	public void enableMsgTracing()
	{
	}

	@Override
	public void disableMsgTracing()
	{
	}

	@Override
	public void recordEvent(AEventRecord event)
	{
		System.out.println(event.getFormattedMessage());
	}

	@Override
	public void debug(Object text)
	{
		System.out.println(text);
	}

	@Override
	public void debug(String text, Object details)
	{
		System.out.println(text + "\n  " + details.toString());
	}
	
	@Override
	public boolean isTraceOn()
	{
		return true;
	}

	@Override
	public void logRequest(Iso8583FMS msg, IEndpoint endpoint) throws Exception
	{
		System.out.println("Message out:\n" + msg.toString());
	}

	@Override
	public void logResponse(Iso8583FMS msg, IEndpoint endpoint) throws Exception
	{
		System.out.println("Message in:\n" + msg.toString());
	}

}
