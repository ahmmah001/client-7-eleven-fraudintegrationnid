/*----------------------------------------------------------------------------*
 *
 * Copyright (c) 2019 by Montant Limited. All rights reserved.
 *
 * This software is the confidential and proprietary property of
 * Montant Limited and may not be disclosed, copied or distributed
 * in any form without the express written permission of Montant Limited.
 *
 *----------------------------------------------------------------------------*/

package postilion.realtime.fraudnid.ipc;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class BackoffPolicyTest
{
	@Test
	public void testTenMillisecondBoundary() throws InterruptedException
	{
		BackoffPolicy p = new BackoffPolicy().setInitialBackoffMillis(1).setMaxBackoffMillis(30000);
		for(int x = 0; x < 50; x++)
		{
			long t = p.nextBackoffMillis();
			assertTrue("Yield time must be a multiple of 10 to avoid clock drift", t % 10 == 0);
			Thread.sleep(155);
		}
	}
}
