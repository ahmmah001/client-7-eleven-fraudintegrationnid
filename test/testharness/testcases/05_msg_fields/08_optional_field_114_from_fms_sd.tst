<|
|======================================================================
| Tests the construction of field 114 (optional 4) from the FMS config
| Obtain the value from Structured Data
|======================================================================
|>

[add flow directory "../../flows"]

[use flow _connect]

# Set up the config data for fictional company 1010, put field 127.22.FIELD_FOR_114 into field 114
[execute sql statement
	[
		"INSERT INTO fraudnid_swh_cat_fms_config(id, id_fms, tran_type, name_interface, card_type, service, field_account_ref_num, field_amount, field_optional_2) values (1010, 1010, 'TypeOne|TypeTwo', 'InterfaceOne|InterfaceTwo', 'CardType', 'Service', '102|FIELD_ACCNR', '4|FIELD_AMOUNT', 'FIELD_FOR_114')"
	]
]

[use flow _resync_nid]

[use flow _0200
	[send "0200" from Source overrides send_request
		[
			"127.22" : 
				\ISO8583Post.structuredData("TVP_PARAMS", 
				"FIELD_9998", "1010",
				"FIELD_00", "CompanyName", 
				"FIELD_128", "001", 
				"FIELD_127", "123", 
				"FIELD_129", "321", 
				"FIELD_IP", "1.2.3.4", 
				"HOST_DATETIME_FULL", "Feb 05 06h55:04.403",
				"FIELD_FOR_114", "OptionalValue"
				)\
			"127.33" : "6666"
		]
	]
	
	[receive "0200" at FMS overrides recv_fms_request
		[
			"107"	:	"TypeOne"
			"108"	:	"InterfaceOne"
			"109"	:	"Service"
			"110"	:	"CardType"
			# Should match the value from the field from the config above
			"112"	:	"0"
			"114"	:	"OptionalValue"
			"115"	:	"0"
		]
	]
	
	[receive "0210" at Source overrides receive_request
		[
			"102"	 : "1234567890"
			"127.22" : 
				\ensureContains("[Fraud Checked: true]")\ & 
				\ensureContains("213Fraud:RspTime")\ & 
				\ensureContains("119Fraud:Req")\ & 
				\ensureContains("119Fraud:Rsp")\ & 
				\ensureContains("[MTI = '0200'][Field 4 = '000000000100'][Field 37 = '000000001RDM'][Field 41 = '12345678'][Field 42 = '123456789012345'][Field 105 = 'CompanyName'][Field 106 = '*'][Field 107 = 'TypeOne'][Field 108 = 'InterfaceOne'][Field 109 = 'Service'][Field 110 = 'CardType'][Field 111 = '0'][Field 112 = '0'][Field 114 = 'OptionalValue'][Field 115 = '0'][Field 116 = 'Feb 05 06h55:04.403'][Field 117 = '001'][Field 118 = '123'][Field 119 = '321'][Field 120 = '0'][Field 121 = '1.2.3.4']")\ & 
				\ensureContains("[MTI = '0210'][Field 37 = '000000001RDM'][Field 39 = '00'][Field 41 = '12345678'][Field 42 = '123456789012345'][Field 108 = 'InterfaceOne']")\
			"127.33" : "6666"
		]
	]
]

[execute sql statement
	[
		"DELETE FROM fraudnid_swh_cat_fms_config where id = 1010"
	]
]

[use flow _resync_nid]

[use flow _disconnect]