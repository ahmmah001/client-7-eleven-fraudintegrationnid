<|
|======================================================================
|For Payback transactions declined with RC 05, field 20 & 21 must 
| contain the response code plus a description
|======================================================================
|>

[add flow directory "../../flows"]

[use flow _connect]

[use flow _0200_fraud
	[send "0200" from Source overrides send_request
		[
			"127.22" : 
				\ISO8583Post.structuredData("TVP_PARAMS", 
				"FIELD_9998", "90",
				"FIELD_00", "CompanyName", 
				"FIELD_128", "001", 
				"FIELD_127", "123", 
				"FIELD_129", "321", 
				"FIELD_IP", "1.2.3.4",
				"HOST_DATETIME_FULL", "Feb 05 06h55:04.403")\
			"127.33" : "6666"
		]
	]
	
	[receive "0200" at FMS overrides recv_fms_request
		[
			"107"	:	"00"
			"108"	:	"Payback"
			"109"	:	"Service"
			"110"	:	"CardType"
			"112"	:	"0"
			"114"	:	"0"
			"115"	:	"0"
		]
	]
	
	[send "0210" from FMS overrides send_fms_response
		[
			"39"	:	"05"
		]
	]
	
	[receive "0210" at Source overrides receive_request
		[
			"39"		:	"05"
			"102"		:	"1234567890"
			"127.9"		:	"Do not honor"
			"127.22"	:
				\ensureContains("[Fraud Checked: true]")\ & 
				\ensureContains("213Fraud:RspTime")\ & 
				\ensureContains("119Fraud:Req")\ & 
				\ensureContains("119Fraud:Rsp")\ & 
				\ensureContains("18FIELD_2021505 Do not honor")\ &
				\ensureContains("18FIELD_2121505 Do not honor")\ &
				\ensureContains("[MTI = '0200'][Field 4 = '000000000100'][Field 37 = '000000001RDM'][Field 41 = '12345678'][Field 42 = '123456789012345'][Field 105 = 'CompanyName'][Field 106 = '*'][Field 107 = '00'][Field 108 = 'Payback'][Field 109 = 'Service'][Field 110 = 'CardType'][Field 111 = '0'][Field 112 = '0'][Field 114 = '0'][Field 115 = '0'][Field 116 = 'Feb 05 06h55:04.403'][Field 117 = '001'][Field 118 = '123'][Field 119 = '321'][Field 120 = '0'][Field 121 = '1.2.3.4']")\ & 
				\ensureContains("[MTI = '0210'][Field 37 = '000000001RDM'][Field 39 = '05'][Field 41 = '12345678'][Field 42 = '123456789012345'][Field 108 = 'Payback']")\
			"127.33" : "6666"
		]
	]
]

[use flow _disconnect]