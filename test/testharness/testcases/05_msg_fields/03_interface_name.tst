<|
|======================================================================
| Tests the construction of Field 127.22 - StructuredData sub-field
| FIELD_21/8. The content is sourced from the resp_codes_txt table.
| DEPRECATED
| This test is deprecated - fields 21/8 are only for transactions 
| declined with RC = 05
|======================================================================
|>
