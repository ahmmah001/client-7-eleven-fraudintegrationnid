<|
|======================================================================
| Tests the construction of Field 106 - Account Number/Reference and 
| Field 109 - Service Name. Their contents depend on the FMS ID, the 
| FMS configuration and the configuration in the issuers_tdn table.
|======================================================================
|>

[add flow directory "../../flows"]

[use flow _connect]

[use flow _0200
	[send "0200" from Source overrides send_request
		[
			"3" 	 : "010000"
			"127.22" : 
				\ISO8583Post.structuredData("TVP_PARAMS", 
				"FIELD_9998", "13",
				"FIELD_00", "CompanyName", 
				"FIELD_128", "001", 
				"FIELD_127", "123", 
				"FIELD_129", "321", 
				"FIELD_IP", "1.2.3.4", 
				"HOST_DATETIME_FULL", "Feb 05 06h55:04.403",
				"FIELD_ACCNR", "11111111112222222222", 
				"FIELD_131", "123", 
				"FIELD_AMOUNT", "200"
				)\
			"127.33" : "6677"
		]
	]
	
	[receive "0200" at FMS overrides recv_fms_request
		[
			"4"   : "000000000200"
			"106" : "2222222222"
			"107" : "TypeTwo"
			"108" : "InterfaceTwo"
			"109" : "ServiceTest"
		]
	]
	
	[receive "0210" at Source overrides receive_request
		[
			"127.22" : 
				\ensureContains("[Fraud Checked: true]")\ & 
				\ensureContains("213Fraud:RspTime")\ & 
				\ensureContains("119Fraud:Req")\ & 
				\ensureContains("119Fraud:Rsp")\ & 
				\ensureContains("[MTI = '0200'][Field 4 = '000000000200'][Field 37 = '000000001RDM'][Field 41 = '12345678'][Field 42 = '123456789012345'][Field 105 = 'CompanyName'][Field 106 = '*'][Field 107 = 'TypeTwo'][Field 108 = 'InterfaceTwo'][Field 109 = 'ServiceTest'][Field 110 = 'CardType'][Field 111 = '0'][Field 112 = '0'][Field 114 = '0'][Field 115 = '0'][Field 116 = 'Feb 05 06h55:04.403'][Field 117 = '001'][Field 118 = '123'][Field 119 = '321'][Field 120 = '0'][Field 121 = '1.2.3.4']")\ & 
				\ensureContains("[MTI = '0210'][Field 37 = '000000001RDM'][Field 39 = '00'][Field 41 = '12345678'][Field 42 = '123456789012345'][Field 108 = 'InterfaceTwo']")\
		]
	]
]

[use flow _disconnect]

[use flow _reset_nid_params]