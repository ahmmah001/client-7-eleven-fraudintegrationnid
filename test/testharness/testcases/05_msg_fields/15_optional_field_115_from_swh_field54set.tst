<|
|======================================================================
| Tests the construction of field 115 (optional 3) from the SWH config.
| As Field 54 is set, Field 115 will be set to the value obtained from config.
|======================================================================
|>

[add flow directory "../../flows"]

[execute sql statement
	[
		"INSERT INTO swh_postilionconfiguracion(sink_node, nomClave, strValor, id_Compania) values ('CtrlFraudes', 'skuCashBackBancomer', 'TestValue', 1)"
	]
]

[use flow _resync_nid]

[use flow _connect]

[use flow _0200
	[send "0200" from Source overrides send_request
		[
			"127.22" :
				\ISO8583Post.structuredData("TVP_PARAMS",
				"FIELD_9998", "25",
				"FIELD_00", "CompanyName",
				"FIELD_54", "0001840C000000000100",
				"FIELD_128", "001",
				"FIELD_127", "123",
				"FIELD_129", "321",
				"FIELD_IP", "1.2.3.4",
				"HOST_DATETIME_FULL", "Feb 05 06h55:04.403")\
			"127.33" : "6666"
		]
	]

	[receive "0200" at FMS overrides recv_fms_request
		[
			"107"	:	"TypeOne"
			"108"	:	"InterfaceOne"
			"109"	:	"Service"
			"110"	:	"CardType"
			"112"	:	"0"
			"114"	:	"0001840C000000000100"
			"115"	:	"TestValue"
		]
	]

	[receive "0210" at Source overrides receive_request
		[
			"102"	 : "1234567890"
			"127.22" :
				\ensureContains("[Fraud Checked: true]")\ &
				\ensureContains("213Fraud:RspTime")\ &
				\ensureContains("119Fraud:Req")\ &
				\ensureContains("119Fraud:Rsp")\ &
				\ensureContains("[MTI = '0200'][Field 4 = '000000000100'][Field 37 = '000000001RDM'][Field 41 = '12345678'][Field 42 = '123456789012345'][Field 105 = 'CompanyName'][Field 106 = '*'][Field 107 = 'TypeOne'][Field 108 = 'InterfaceOne'][Field 109 = 'Service'][Field 110 = 'CardType'][Field 111 = '0'][Field 112 = '0'][Field 114 = '0001840C000000000100'][Field 115 = 'TestValue'][Field 116 = 'Feb 05 06h55:04.403'][Field 117 = '001'][Field 118 = '123'][Field 119 = '321'][Field 120 = '0'][Field 121 = '1.2.3.4']")\ &
				\ensureContains("[MTI = '0210'][Field 37 = '000000001RDM'][Field 39 = '00'][Field 41 = '12345678'][Field 42 = '123456789012345'][Field 108 = 'InterfaceOne']")\
			"127.33" : "6666"
		]
	]
]

[execute sql statement
	[
		"DELETE FROM swh_postilionconfiguracion where nomClave = 'skuCashBackBancomer'"
	]
]

[use flow _resync_nid]

[use flow _disconnect]
