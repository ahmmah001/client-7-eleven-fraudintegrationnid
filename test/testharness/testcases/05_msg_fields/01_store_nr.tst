<|
|======================================================================
| Tests the construction of Field 42 - Store Number to FMS. This value 
| is retrieved either from StructuredData.FIELD_42, if present, or 
| Field 42 - Card Acceptor ID Code. If the length is greater than 
| 15, the value is trimmed to a length of 15.
|======================================================================
|>

[add flow directory "../../flows"]

[use flow _connect]

# From StructuredData.FIELD_42 & Trim Length.
[use flow _0200
	[send "0200" from Source overrides send_request
		[
			"127.22" : 
				\ISO8583Post.structuredData("TVP_PARAMS", 
				"FIELD_9998", "1",
				"FIELD_00", "CompanyName", 
				"FIELD_128", "001", 
				"FIELD_127", "123", 
				"FIELD_129", "321", 
				"FIELD_IP", "1.2.3.4", 
				"HOST_DATETIME_FULL", "Feb 05 06h55:04.403",
				"FIELD_42", "11111222223333344444"
				)\
		]
	]
	
	[receive "0200" at FMS overrides recv_fms_request
		[
			"42"  : "222223333344444"
		]
	]
	
	[receive "0210" at Source overrides receive_request
		[
			"102"	 : "1234567890"
			"127.22" : 
				\ensureContains("[Fraud Checked: true]")\ & 
				\ensureContains("213Fraud:RspTime")\ & 
				\ensureContains("119Fraud:Req")\ & 
				\ensureContains("119Fraud:Rsp")\ & 
				\ensureContains("[MTI = '0200'][Field 4 = '000000000100'][Field 37 = '000000001RDM'][Field 41 = '12345678'][Field 42 = '222223333344444'][Field 105 = 'CompanyName'][Field 106 = '*'][Field 107 = '00'][Field 108 = 'InterfaceName'][Field 109 = 'Service'][Field 110 = 'CardType'][Field 111 = '0'][Field 112 = '0'][Field 114 = '0'][Field 115 = '0'][Field 116 = 'Feb 05 06h55:04.403'][Field 117 = '001'][Field 118 = '123'][Field 119 = '321'][Field 120 = '0'][Field 121 = '1.2.3.4']")\ & 
				\ensureContains("[MTI = '0210'][Field 37 = '000000001RDM'][Field 39 = '00'][Field 41 = '12345678'][Field 42 = '222223333344444'][Field 108 = 'InterfaceName']")\
			"127.33" : "6666"
		]
	]
]

# From Field 42 - Card Acceptor ID Code.
[use flow _0200
	[send "0200" from Source overrides send_request
		[
			"42"  	 : "123456789012345"
			"127.22" : 
				\ISO8583Post.structuredData("TVP_PARAMS", 
				"FIELD_9998", "1",
				"FIELD_00", "CompanyName", 
				"FIELD_128", "001", 
				"FIELD_127", "123", 
				"FIELD_129", "321", 
				"FIELD_IP", "1.2.3.4", 
				"HOST_DATETIME_FULL", "Feb 05 06h55:04.403"
				)\
		]
	]
	
	[receive "0200" at FMS overrides recv_fms_request
		[
			"42"  : "123456789012345"
		]
	]
	
	[receive "0210" at Source overrides receive_request
		[
			"127.22" : 
				\ensureContains("[Fraud Checked: true]")\ & 
				\ensureContains("213Fraud:RspTime")\ & 
				\ensureContains("119Fraud:Req")\ & 
				\ensureContains("119Fraud:Rsp")\ & 
				\ensureContains("[MTI = '0200'][Field 4 = '000000000100'][Field 37 = '000000001RDM'][Field 41 = '12345678'][Field 42 = '123456789012345'][Field 105 = 'CompanyName'][Field 106 = '*'][Field 107 = '00'][Field 108 = 'InterfaceName'][Field 109 = 'Service'][Field 110 = 'CardType'][Field 111 = '0'][Field 112 = '0'][Field 114 = '0'][Field 115 = '0'][Field 116 = 'Feb 05 06h55:04.403'][Field 117 = '001'][Field 118 = '123'][Field 119 = '321'][Field 120 = '0'][Field 121 = '1.2.3.4']")\ & 
				\ensureContains("[MTI = '0210'][Field 37 = '000000001RDM'][Field 39 = '00'][Field 41 = '12345678'][Field 42 = '123456789012345'][Field 108 = 'InterfaceName']")\
		]
	]
]

[use flow _disconnect]