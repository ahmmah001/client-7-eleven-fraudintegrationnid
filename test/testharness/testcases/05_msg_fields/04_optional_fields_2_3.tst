<|
|======================================================================
| Tests the construction of Field 114/115 - Optional Field 2/3. These 
| fields are only set for BBCA transactions and their content is 
| sourced from StructuredData.FIELD_8001 & StructuredData.FIELD_8000, 
| unless fields field_optional_2 through 3 are also set in the FMS 
| config for the transaction. Here, there is no FMS config to consider
|======================================================================
|>

[add flow directory "../../flows"]

[use flow _connect]

[use flow _0200
	[send "0200" from Source overrides send_request
		[
			"127.22" : 
				\ISO8583Post.structuredData("TVP_PARAMS", 
				"FIELD_9998", "25",
				"FIELD_00", "CompanyName", 
				"FIELD_128", "001", 
				"FIELD_127", "123", 
				"FIELD_129", "321", 
				"FIELD_IP", "1.2.3.4", 
				"HOST_DATETIME_FULL", "Feb 05 06h55:04.403",
				"FIELD_41", "0987654321", 
				"FIELD_8000", "1", 
				"FIELD_8001", "2"
				)\
			"127.33" : "6666"
		]
	]
	
	[receive "0200" at FMS overrides recv_fms_request
		[
			"41"  : "87654321"
			"107"  : "TypeOne"
			"108"  : "InterfaceOne"
			"109"  : "Service"
			"114"  : "2"
			"115"  : "1"
		]
	]
	
	[receive "0210" at Source overrides receive_request
		[
			"102"	 : "1234567890"
			"127.22" : 
				\ensureContains("[Fraud Checked: true]")\ & 
				\ensureContains("213Fraud:RspTime")\ & 
				\ensureContains("119Fraud:Req")\ & 
				\ensureContains("119Fraud:Rsp")\ & 
				\ensureContains("[MTI = '0200'][Field 4 = '000000000100'][Field 37 = '000000001RDM'][Field 41 = '87654321'][Field 42 = '123456789012345'][Field 105 = 'CompanyName'][Field 106 = '*'][Field 107 = 'TypeOne'][Field 108 = 'InterfaceOne'][Field 109 = 'Service'][Field 110 = 'CardType'][Field 111 = '0'][Field 112 = '0'][Field 114 = '2'][Field 115 = '1'][Field 116 = 'Feb 05 06h55:04.403'][Field 117 = '001'][Field 118 = '123'][Field 119 = '321'][Field 120 = '0'][Field 121 = '1.2.3.4']")\ & 
				\ensureContains("[MTI = '0210'][Field 37 = '000000001RDM'][Field 39 = '00'][Field 41 = '87654321'][Field 42 = '123456789012345'][Field 108 = 'InterfaceOne']")\
			"127.33" : "6666"
		]
	]
]

[use flow _disconnect]