<|
|======================================================================
| Field 116 is usually provided in 127.22, but if not, it should default
| to the server's local date/time in the format yyyyMMddHHmmss
|======================================================================
|>

[add flow directory "../../flows"]

[use flow _connect]

[use flow _0200
	[send "0200" from Source overrides send_request
		[
			"127.22" : 
				\ISO8583Post.structuredData("TVP_PARAMS", 
				"FIELD_9998", "25",
				"FIELD_00", "CompanyName", 
				"FIELD_128", "001", 
				"FIELD_127", "123", 
				"FIELD_129", "321", 
				"FIELD_IP", "1.2.3.4", 
				"FIELD_8000", "1", 
				"FIELD_8001", "2"
				)\
			"127.33" : "6666"
		]
	]
	
	[receive "0200" at FMS overrides recv_fms_request
		[
			"107"  	: "TypeOne"
			"108"  	: "InterfaceOne"
			"109"  	: "Service"
			"114"  	: "2"
			"116"	: \ensureMatchesRegEx("^(19[0-9]{2}|2[0-9]{3})(0[1-9]|1[012])([123]0|[012][1-9]|31)([01][0-9]|2[0-3])[0-5][0-9][0-5][0-9]")\
			"115"  	: "1"
		]
	]
	
	[receive "0210" at Source overrides receive_request
		[
			"102"	 : "1234567890"
			"127.22" : 
				\ensureContains("[Fraud Checked: true]")\ & 
				\ensureContains("213Fraud:RspTime")\ & 
				\ensureContains("119Fraud:Req")\ & 
				\ensureContains("119Fraud:Rsp")\ 
			"127.33" : "6666"
		]
	]
]

[use flow _disconnect]