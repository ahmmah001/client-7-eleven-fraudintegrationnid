<|
|======================================================================
| Tests the construction of field 115 (optional 3) from Structured
| Data, tag FIELD_54
|======================================================================
|>

[add flow directory "../../flows"]

[use flow _connect]

[use flow _0200
	[send "0200" from Source overrides send_request
		[
			"127.22" : 
				\ISO8583Post.structuredData("TVP_PARAMS", 
				"FIELD_9998", "25",
				"FIELD_00", "CompanyName", 
				"FIELD_128", "001", 
				"FIELD_127", "123", 
				"FIELD_129", "321", 
				"FIELD_IP", "1.2.3.4",
				"FIELD_54", "fromField54",				
				"HOST_DATETIME_FULL", "Feb 05 06h55:04.403")\
			"127.33" : "6666"
		]
	]
	
	[receive "0200" at FMS overrides recv_fms_request
		[
			"107"	:	"TypeOne"
			"108"	:	"InterfaceOne"
			"109"	:	"Service"
			"110"	:	"CardType"
			"112"	:	"0"
			"114"	:	"fromField54"
			"115"	:	"0"
		]
	]
	
	[receive "0210" at Source overrides receive_request
		[
			"102"	 : "1234567890"
			"127.22" : 
				\ensureContains("[Fraud Checked: true]")\ & 
				\ensureContains("213Fraud:RspTime")\ & 
				\ensureContains("119Fraud:Req")\ & 
				\ensureContains("119Fraud:Rsp")\ & 
				\ensureContains("[MTI = '0200'][Field 4 = '000000000100'][Field 37 = '000000001RDM'][Field 41 = '12345678'][Field 42 = '123456789012345'][Field 105 = 'CompanyName'][Field 106 = '*'][Field 107 = 'TypeOne'][Field 108 = 'InterfaceOne'][Field 109 = 'Service'][Field 110 = 'CardType'][Field 111 = '0'][Field 112 = '0'][Field 114 = 'fromField54'][Field 115 = '0'][Field 116 = 'Feb 05 06h55:04.403'][Field 117 = '001'][Field 118 = '123'][Field 119 = '321'][Field 120 = '0'][Field 121 = '1.2.3.4']")\ & 
				\ensureContains("[MTI = '0210'][Field 37 = '000000001RDM'][Field 39 = '00'][Field 41 = '12345678'][Field 42 = '123456789012345'][Field 108 = 'InterfaceOne']")\
			"127.33" : "6666"
		]
	]
]

[use flow _disconnect]