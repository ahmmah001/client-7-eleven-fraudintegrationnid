<|
|======================================================================
| Standard 0200 requests paired with a successful 0200 fraud checks.
| Note: Mostly irrelevent as performance is limited by TestHarness.
|======================================================================
|>

[add flow directory "../../flows"]

[use flow _connect]

[connect "FMS"]
[ensure FMS connected]

[repeat 50 times
	[use flow _0200_bulk]
]

[use flow _disconnect]