<|
|======================================================================
| 0200 request, successful 0200 fraud request, successful 0210 response
| from the authorizer, 0420 from source, 0430 from TM/Sink and finally
| a successful 0420/0430 to/from FMS.
|======================================================================
|>

[add flow directory "../../flows"]

[use flow _connect]

[use flow _0200_0420
	[send "0200" from Source overrides send_request
		[
			"37" : "101010101010"
		]
	]
	
	[receive "0210" at Source overrides receive_request
		[
		]
	]
	
	[receive "0420" at FMS overrides recv_fms_reversal
		[
			"105" : "CompanyName"
			"116" : "Feb 05 06h55:04.403"
			"117" : "001"
			"118" : "123"
			"119" : "321"
			"121" : "1.2.3.4"
		]
	]
	
	[send "0430" from FMS overrides send_fms_rev_rsp
		[
			"39"  : "00"
		]
	]
]

[use flow _disconnect]