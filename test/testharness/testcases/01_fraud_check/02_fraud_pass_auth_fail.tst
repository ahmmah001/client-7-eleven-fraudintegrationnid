<|
|======================================================================
| 0200 request paired with a successful 0200 fraud check, followed 
| by a decline by the authorizer, resulting in an 0420 fraud reversal.
|======================================================================
|>

[add flow directory "../../flows"]

[use flow _connect]

[use flow _0200
	[send "0200" from Source overrides send_request
		[
		]
	]
	
	[receive "0200" at FMS overrides recv_fms_request
		[
		]
	]
	
	[send "0210" from FMS overrides send_fms_response
		[
			"39"  : "00"
		]
	]
	
	[receive "0200" at AuthSinkSap1 overrides recv_request
		[
		]
	]
	
	[send "0210" from AuthSinkSap1 overrides send_response
		[		
			"39"  	 : "01"		
		]
	]
	
	[receive "0210" at Source overrides receive_request
		[
			"39"	 : "01"
			"127.22" : 
				\ensureContains("[Fraud Checked: true]")\ & 
				\ensureContains("213Fraud:RspTime")\ & 
				\ensureContains("119Fraud:Req")\ & 
				\ensureContains("119Fraud:Rsp")\ & 
				\ensureContains("[MTI = '0200'][Field 4 = '000000000100'][Field 37 = '000000001RDM'][Field 41 = '12345678'][Field 42 = '123456789012345'][Field 105 = 'CompanyName'][Field 106 = '*'][Field 107 = '00'][Field 108 = 'InterfaceName'][Field 109 = 'Service'][Field 110 = 'CardType'][Field 111 = '0'][Field 112 = '0'][Field 114 = '0'][Field 115 = '0'][Field 116 = 'Feb 05 06h55:04.403'][Field 117 = '001'][Field 118 = '123'][Field 119 = '321'][Field 120 = '0'][Field 121 = '1.2.3.4']")\ & 
				\ensureContains("[MTI = '0210'][Field 37 = '000000001RDM'][Field 39 = '00'][Field 41 = '12345678'][Field 42 = '123456789012345'][Field 108 = 'InterfaceName']")\
		]
	]
]

# Ensure the reversal was written to the SAFQ table.
[execute sql statement
	[
		"SELECT 1 FROM fraudnid_persistent_safq " + 
		"WHERE id = '000000001RDMInterfaceName' " +
		"AND sent = 0"
	]
	ensure row count = 1
]

[use flow _0420_fraud
	# Receive 0420 @ FMS.
	[receive "0420" at FMS overrides recv_fms_reversal
		[
		]
	]
	
	# Send 0430 @ NID.
	[send "0430" from FMS overrides send_fms_rev_rsp
		[
			"39"  : "00"
		]
	]
]

[use flow _disconnect]