<|
|======================================================================
| Standard 0200 with no fraud check because Fraud system is down.
| Message isn't modified with any fraud data.
|======================================================================
|>

[add flow directory "../../flows"]
[add profile directory "../../profiles"]

[use flow _connect]

# Clean the event log to make sure relics of 
# other tests aren't going to fail us
[execute sql statement
	[
		"DELETE FROM support_event_log_open " + 
		"WHERE event_id IN ('20003', '20002') " +  
		"AND interface = 'FraudNID' " +  
		"AND entity = 'AuthSink'"
	]
]

[send "0200" from Source as send_request
	import profile::send_request
	[
		"100" : "00001"
		"102" : "1234567890"
		"127.22" : 
				\ISO8583Post.structuredData("TVP_PARAMS", 
				"FIELD_9998", "1",
				"FIELD_00", "CompanyName", 
				"FIELD_128", "001", 
				"FIELD_127", "123", 
				"FIELD_129", "321", 
				"FIELD_IP", "1.2.3.4", 
				"HOST_DATETIME_FULL", "Feb 05 06h55:04.403" 
				)\
		"127.33" : "6666"
	]
]

[receive "0200" at AuthSinkSap1 as recv_request
	import profile::recv_request
	[
		"100" 	 : "00001"
		"102" 	 : \ignore()\
		"123" 	 : \ignore()\
		"127.22" : \ignore()\
		"127.33" : \ignore()\
	]
]

[send "0210" from AuthSinkSap1 as send_response
	import profile::send_response
	[
		"39"  : "00"		
		"100" : "00001"
		"127.33" : "6666"	
	]
]

[receive "0210" at Source as receive_request
	import profile::recv_response
	[
		"102"	 : "1234567890"
		"127.22" : \ensureAbsent()\
		"127.33" : "6666"
	]
]

[use flow _disconnect]