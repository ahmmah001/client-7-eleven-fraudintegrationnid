<|
|======================================================================
| Standard 0200 with an unsuccessful (timed-out) fraud request. 0210 is
| updated and returned to the source.
|======================================================================
|>

[add flow directory "../../flows"]
[add profile directory "../../profiles"]

[use flow _connect]
[connect FMS]
[ensure FMS connected]

# Clean the event log to make sure relics of 
# other tests aren't going to fail us
[execute sql statement
	[
		"DELETE FROM support_event_log_open " + 
		"WHERE event_id IN ('20003', '20002') " +  
		"AND interface = 'FraudNID' " +  
		"AND entity = 'AuthSink'"
	]
]

[send "0200" from Source as send_request
	import profile::send_request
	[
		"100" : "00001"
		"102" : "1234567890"
		"127.22" : 
				\ISO8583Post.structuredData("TVP_PARAMS", 
				"FIELD_9998", "1",
				"FIELD_00", "CompanyName", 
				"FIELD_128", "001", 
				"FIELD_127", "123", 
				"FIELD_129", "321", 
				"FIELD_IP", "1.2.3.4", 
				"HOST_DATETIME_FULL", "Feb 05 06h55:04.403" 
				)\
		"127.33" : "6666"
	]
]

[receive "0200" at FMS as recv_fms_request
	import profile::receive_fms_req
	[
		"4"   : "000000000100"
		"37"  : "000000001RDM"
		"41"  : "12345678"
		"42"  : "123456789012345"
		"105" : "CompanyName"
		"106" : "1234567890"
		"107" : "00"
		"108" : "InterfaceName"
		"109" : "Service"
		"110" : "CardType"
		"111" : "0"
		"112" : "0"
		"116" : "Feb 05 06h55:04.403"
		"117" : "001"
		"118" : "123"
		"119" : "321"
		"120" : "0"
		"121" : "1.2.3.4"
	]
]

[wait for 45 seconds]

[receive "0200" at AuthSinkSap1 as recv_request
	import profile::recv_request
	[
		"100" 	 : "00001"
		"102" 	 : \ignore()\
		"123" 	 : \ignore()\
		"127.22" : \ignore()\
		"127.33" : \ignore()\
	]
]

[send "0210" from AuthSinkSap1 as send_response
	import profile::send_response
	[
		"39"  : "00"		
		"100" : "00001"
		"127.33" : "6666"	
	]
]

[receive "0210" at Source as receive_request
	import profile::recv_response
	[
		"102"	 : "1234567890"
		"127.22" : 
				\ensureContains("[Fraud Checked: false]")\ & 
				\ensureContains("119Fraud:Req")\ & 
				\ensureContains("[Fraud Response Time: timeout]")\ & 
				\ensureContains("[MTI = '0200'][Field 4 = '000000000100'][Field 37 = '000000001RDM'][Field 41 = '12345678'][Field 42 = '123456789012345'][Field 105 = 'CompanyName'][Field 106 = '*'][Field 107 = '00'][Field 108 = 'InterfaceName'][Field 109 = 'Service'][Field 110 = 'CardType'][Field 111 = '0'][Field 112 = '0'][Field 114 = '0'][Field 115 = '0'][Field 116 = 'Feb 05 06h55:04.403'][Field 117 = '001'][Field 118 = '123'][Field 119 = '321'][Field 120 = '0'][Field 121 = '1.2.3.4']")\ &  
				\ensureNotContains("[MTI = '0210']")\
		"127.33" : "6666"
	]
]

# Check for a request timeout event.
[execute sql statement
	[
		"SELECT 1 FROM support_event_log_open " + 
		"WHERE event_id = '20003' " +  
		"AND interface = 'FraudNID' " +  
		"AND entity = 'AuthSink'"
	]
	ensure row count = 1
]

[use flow _disconnect]