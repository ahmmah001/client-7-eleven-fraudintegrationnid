<|
|======================================================================
| Load a new mapper, check processing, reload the previous mapper and
| check procesing.
|======================================================================
|>

[add flow directory "../../flows"]

[execute sql statement
	[
		"DELETE FROM support_event_log_params " + 
		"WHERE msg_param_value = 'New mapper class loaded: postilion.realtime.fraudnid.msg.Mapper_v0'"
	]
]

[use flow _connect]

[use flow _0200]

[wait for 5 seconds]

# New mapper.
[execute sql statement
	[
		"UPDATE cfg_custom_classes " + 
		"SET parameters = '<Parameters><MapperClassName>postilion.realtime.fraudnid.msg.Mapper_v0</MapperClassName></Parameters>' " + 
		"WHERE unique_name = 'NODE INTEGRATION:FraudNID'"
	]
	ensure update count = 1
]

[wait for 5 seconds]

# Load the new mapper.
[use flow _reload_mapper]

# Check for reload event.
[execute sql statement
	[
		"SELECT 1 FROM support_event_log_params " + 
		"WHERE msg_param_value = 'New mapper class loaded: postilion.realtime.fraudnid.msg.Mapper_v0'"
	]
	ensure row count = 1
]

[wait for 5 seconds]

[use flow _0200]

[wait for 5 seconds]

[execute sql statement
	[
		"UPDATE cfg_custom_classes " + 
		"SET parameters = '<Parameters><MapperClassName>postilion.realtime.fraudnid.msg.Mapper_v1</MapperClassName></Parameters>' " +
		"WHERE unique_name = 'NODE INTEGRATION:FraudNID'"
	]
	ensure update count = 1
]

[execute sql statement
	[
		"DELETE FROM support_event_log_params " + 
		"WHERE msg_param_value = 'New mapper class loaded: postilion.realtime.fraudnid.msg.Mapper_v1'"
	]
]

# Load the previous mapper.
[use flow _reload_mapper]

# Check for reload event.
[execute sql statement
	[
		"SELECT 1 FROM support_event_log_params " + 
		"WHERE msg_param_value = 'New mapper class loaded: postilion.realtime.fraudnid.msg.Mapper_v1'"
	]
	ensure row count = 1 
]

[wait for 5 seconds]

[use flow _0200]

[use flow _disconnect]