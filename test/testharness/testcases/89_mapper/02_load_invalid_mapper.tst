<|
|======================================================================
| Load a non-existent mapper and check processing.
|======================================================================
|>

[add flow directory "../../flows"]

[execute sql statement
	[
		"DELETE FROM support_event_log_params " + 
		"WHERE msg_param_value LIKE 'The given mapper class could not be loaded.%'"
	]
]

[use flow _connect]

[use flow _0200]

[wait for 5 seconds]

# New mapper.
[execute sql statement
	[
		"UPDATE cfg_custom_classes " + 
		"SET parameters = '<Parameters><MapperClassName>postilion.realtime.fraudnid.msg.Mapper_v999</MapperClassName></Parameters>' " + 
		"WHERE unique_name = 'NODE INTEGRATION:FraudNID'"
	]
	ensure update count = 1
]

[wait for 5 seconds]

# Load the new mapper.
[connect "PassThroughInterface"]
[send command to "PassThroughInterface"
	["text" : "SET AuthSink INTEGRATION FraudNID1 RELOADMAPPER"]
]
[receive response from "PassThroughInterface"
	["text" : "COULD NOT PROCESS COMMAND"]
]
[disconnect "PassThroughInterface"]

[execute sql statement
	[
		"SELECT 1 FROM support_event_log_params " + 
		"WHERE msg_param_value LIKE 'The given mapper class could not be loaded.%'"
	]
	ensure row count = 1
]

[wait for 5 seconds]

[use flow _0200]

[use flow _disconnect]

[execute sql statement
	[
		"UPDATE cfg_custom_classes " + 
		"SET parameters = '<Parameters><MapperClassName>postilion.realtime.fraudnid.msg.Mapper_v1</MapperClassName></Parameters>' " +
		"WHERE unique_name = 'NODE INTEGRATION:FraudNID'"
	]
	ensure update count = 1
]