<|
|======================================================================
| Reload the current mapper and ensure processing succeeds.
|======================================================================
|>

[add flow directory "../../flows"]

[execute sql statement
	[
		"DELETE FROM support_event_log_params " + 
		"WHERE msg_param_value = 'New mapper class loaded: postilion.realtime.fraudnid.msg.Mapper_v1'"
	]
]

[use flow _connect]

[use flow _0200]

[wait for 5 seconds]

# Reload the mapper.
[execute sql statement
	[
		"DELETE FROM support_event_log_params " + 
		"WHERE msg_param_value = 'New mapper class loaded: postilion.realtime.fraudnid.msg.Mapper_v1'"
	]
]

[use flow _reload_mapper]

# Check for reload event.
[execute sql statement
	[
		"SELECT 1 FROM support_event_log_params " + 
		"WHERE msg_param_value = 'New mapper class loaded: postilion.realtime.fraudnid.msg.Mapper_v1'"
	]
	ensure row count = 1
]

[wait for 5 seconds]

[use flow _0200]

[use flow _disconnect]