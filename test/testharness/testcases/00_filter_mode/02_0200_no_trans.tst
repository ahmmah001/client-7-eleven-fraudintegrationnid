<|
|======================================================================
| Standard 0200 request without a fraud check.
|======================================================================
|>

[add flow directory "../../flows"]

[execute sql statement
	[
		"UPDATE tm_integration_pipeline " + 
		"SET integration_driver_parameters = 'DISABLE 127.0.0.1 55000 120 180 1440 10 3 false CtrlFraudes 30 7' " + 
		"WHERE name = 'FraudNID1'"
	]
	ensure update count = 1
]

[use flow _resync]

[use flow _connect]

[use flow _0200_no_fms_connect
	[send "0200" from Source overrides send_request
		[
		]
	]
	
	[receive "0200" at AuthSinkSap1 overrides recv_request
		[
		]
	]
	
	[send "0210" from AuthSinkSap1 overrides send_response
		[		
			"39"  	 : "00"		
		]
	]
	
	[receive "0210" at Source overrides receive_request
		[
			"127.9"  : \ensureAbsent()\
			"127.22" : \ensureAbsent()\
		]
	]
]

[use flow _disconnect]

[use flow _reset_nid_params]