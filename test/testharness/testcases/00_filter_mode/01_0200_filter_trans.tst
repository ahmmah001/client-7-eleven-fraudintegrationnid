<|
|======================================================================
| Filtered 0200 request paired with a successful 0200 fraud check and 
| an un-filtered 0200 without a fraud check.
|======================================================================
|>

[add flow directory "../../flows"]

[execute sql statement
	[
		"UPDATE tm_integration_pipeline " + 
		"SET integration_driver_parameters = 'FILTER 127.0.0.1 55000 120 180 1440 10 3 false CtrlFraudes 30 7' " + 
		"WHERE name = 'FraudNID1'"
	]
	ensure update count = 1
]

[use flow _resync]

[use flow _connect]

# Filter Active: Tran Type and Extended Tran Type.

[use flow _0200
	[send "0200" from Source overrides send_request
		[
			"127.33" : "6666"
		]
	]
	
	[receive "0200" at FMS overrides recv_fms_request
		[
			"4"   : "000000000100"
			"37"  : "000000001RDM"
			"41"  : "12345678"
			"42"  : "123456789012345"
			"106" : "1234567890"
			"105" : "CompanyName"
			"107" : "00"
			"108" : "InterfaceName"
			"109" : "Service"
			"110" : "CardType"
			"111" : "0"
			"112" : "0"
			"116" : "Feb 05 06h55:04.403"
			"117" : "001"
			"118" : "123"
			"119" : "321"
			"120" : "0"
			"121" : "1.2.3.4"
		]
	]
	
	[send "0210" from FMS overrides send_fms_response
		[
			"39"  : "00"
		]
	]
	
	[receive "0200" at AuthSinkSap1 overrides recv_request
		[
		]
	]
	
	[send "0210" from AuthSinkSap1 overrides send_response
		[		
			"39"  	 : "00"		
		]
	]
	
	[receive "0210" at Source overrides receive_request
		[
			"127.22" : 
				\ensureContains("[Fraud Checked: true]")\ & 
				\ensureContains("213Fraud:RspTime")\ & 
				\ensureContains("119Fraud:Req")\ & 
				\ensureContains("119Fraud:Rsp")\ & 
				\ensureContains("[MTI = '0200'][Field 4 = '000000000100'][Field 37 = '000000001RDM'][Field 41 = '12345678'][Field 42 = '123456789012345'][Field 105 = 'CompanyName'][Field 106 = '*'][Field 107 = '00'][Field 108 = 'InterfaceName'][Field 109 = 'Service'][Field 110 = 'CardType'][Field 111 = '0'][Field 112 = '0'][Field 114 = '0'][Field 115 = '0'][Field 116 = 'Feb 05 06h55:04.403'][Field 117 = '001'][Field 118 = '123'][Field 119 = '321'][Field 120 = '0'][Field 121 = '1.2.3.4']")\ & 
				\ensureContains("[MTI = '0210'][Field 37 = '000000001RDM'][Field 39 = '00'][Field 41 = '12345678'][Field 42 = '123456789012345'][Field 108 = 'InterfaceName']")\
		]
	]
]

# Filter Active: Tran Type only.

[use flow _0200
	[send "0200" from Source overrides send_request
		[
			"3" 	: "010000"
			"127.33": \notPresent()\
		]
	]
]

# Filter Inactive: No Fraud Check.

[use flow _0200_no_fms_connect
	[receive "0210" at Source overrides receive_request
		[
			"127.9"  : \ensureAbsent()\
			"127.22" : \ensureAbsent()\
		]
	]
]

[use flow _disconnect]