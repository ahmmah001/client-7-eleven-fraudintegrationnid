<|
|======================================================================
| Create a large saf q and handle it with various scenarios
|======================================================================
|>

[add flow directory "../../flows"]
[add profile directory "../../profiles"]

[create process
   [
      "NET STOP PassThroughInterface"
   ]
]

# Clean the store-and-forward queue
[create process
   [
      "postjava_fraudnid", "postilion.realtime.fraudnid.helpers.ReversalGenerator", "clean"
   ]
   ensure exit code = 0
]

# Insert series of reversal records
[create process
   [
    #                                                                               make records 
    #                                                                                   |     this many
    #                                                                                   |        |   sink name   
	#                                                                                   |        |      |     start at 0 for RRN
	#                                                                                   |        |      |         |
      "postjava_fraudnid", "postilion.realtime.fraudnid.helpers.ReversalGenerator", "generate", "6", "AuthSink", "0"
   ]
   ensure exit code = 0
]

[create process
   [
      "NET START PassThroughInterface"
   ]
]

[connect FMS]
[ensure FMS connected]

# Receive and don't respond to the first 5 reversals.
# After 3 attempts with the same RRN, the next transaction in the queue should be received 
# (the RRN should increment by 1 to indicate the next transaction, with no skips in the middle)

[receive "0420" at FMS as recv_fms_reversal1_1
	import profile::receive_fms_req
	[
		"37"	:	"000000000000"
	]
]

[receive "0420" at FMS as recv_fms_reversal1_2
	import profile::receive_fms_req
	[
		"37"	:	"000000000000"
	]
]
[receive "0420" at FMS as recv_fms_reversal1_3
	import profile::receive_fms_req
	[
		"37"	:	"000000000000"
	]
]
# 2nd reversal - let it timeout too
[receive "0420" at FMS as recv_fms_reversal2_1
	import profile::receive_fms_req
	[
		"37"	:	"000000000001"
	]
]
[receive "0420" at FMS as recv_fms_reversal2_2
	import profile::receive_fms_req
	[
		"37"	:	"000000000001"
	]
]
[receive "0420" at FMS as recv_fms_reversal2_3
	import profile::receive_fms_req
	[
		"37"	:	"000000000001"
	]
]

# 3rd timeout
[receive "0420" at FMS as recv_fms_reversal3_1
	import profile::receive_fms_req
	[
		"37"	:	"000000000002"
	]
]
[receive "0420" at FMS as recv_fms_reversal3_2
	import profile::receive_fms_req
	[
		"37"	:	"000000000002"
	]
]
[receive "0420" at FMS as recv_fms_reversal3_3
	import profile::receive_fms_req
	[
		"37"	:	"000000000002"
	]
]

[receive "0420" at FMS as recv_fms_reversal4_1
	import profile::receive_fms_req
	[
		"37"	:	"000000000003"
	]
]
[disconnect FMS]

# Stop and start the interchange, should pick up from 2nd transmission of 3rd transaction

[create process
   [
      "NET STOP PassThroughInterface"
   ]
]
[create process
   [
      "NET START PassThroughInterface"
   ]
]

[connect FMS]
[ensure FMS connected]

[receive "0420" at FMS as recv_fms_reversal4_2
	import profile::receive_fms_req
	[
		"37"	:	"000000000003"
	]
]
[receive "0420" at FMS as recv_fms_reversal4_3
	import profile::receive_fms_req
	[
		"37"	:	"000000000003"
	]
]
# Let 3 timeout, receive 4th and timeout once, then respond. Should move to 5
[receive "0420" at FMS as recv_fms_reversal5_1
	import profile::receive_fms_req
	[
		"37"	:	"000000000004"
	]
]
[receive "0420" at FMS as recv_fms_reversal5_2
	import profile::receive_fms_req
	[
		"37"	:	"000000000004"
	]
]
[send "0430" from FMS as send_fms_rev_rsp5_2
	import profile::send_fms_rsp
	[
		"37"  : \copyFrom("recv_fms_reversal5_2", "37")\
		"41"  : \copyFrom("recv_fms_reversal5_2", "41")\
		"42"  : \copyFrom("recv_fms_reversal5_2", "42")\
		"39"  : "00"
		"108"  : \copyFrom("recv_fms_reversal5_2", "108")\
	]
]
[receive "0420" at FMS as recv_fms_reversal6_1
	import profile::receive_fms_req
	[
		"37"	:	"000000000005"
	]
]
[send "0430" from FMS as send_fms_rev_rsp6_1
	import profile::send_fms_rsp
	[
		"37"  : \copyFrom("recv_fms_reversal6_1", "37")\
		"41"  : \copyFrom("recv_fms_reversal6_1", "41")\
		"42"  : \copyFrom("recv_fms_reversal6_1", "42")\
		"39"  : "00"
		"108"  : \copyFrom("recv_fms_reversal6_1", "108")\
	]
]
# done - shouldn't receive any more because the queue should now be empty

[wait for 15 seconds]

[disconnect FMS]
