<|
|======================================================================
| 0200 request (success) paired with a successful 0200 fraud check, 
| subsequently reversed by the originator. The reversal response is 
| not sent by the upstream entity, thus reversal retries are attempted 
| till the allowed number of retries is exceeded, after which the 
| cleaner removes the transaction from the safq.
| 
| Flow
| ----
| * Do not send the 0430 response from FMS so as to elicit retries.
| * Do not process subsequent reversal advices.
| * The safq should be cleared after the allowed number of retries has 
| 	been exceeded in the next clean cycle.
|======================================================================
|>

[add flow directory "../../flows"]
[add profile directory "../../profiles"]

[use flow _connect]

[connect "FMS"]
[ensure FMS connected]

[send "0200" from Source as send_request
	import profile::send_request
	[
		"37"  : "990000001RDM"
		"100" : "00001"
		"102" : "1234567890"
		"127.22" : 
				\ISO8583Post.structuredData("TVP_PARAMS", 
				"FIELD_9998", "1",
				"FIELD_00", "CompanyName", 
				"FIELD_128", "001", 
				"FIELD_127", "123", 
				"FIELD_129", "321", 
				"FIELD_IP", "1.2.3.4", 
				"HOST_DATETIME_FULL", "Feb 05 06h55:04.403" 
				)\
		"127.33" : "6666"
	]
]

[receive "0200" at FMS as recv_fms_request
	import profile::receive_fms_req
	[
		"4"   : "000000000100"
		"37"  : "990000001RDM"
		"41"  : "12345678"
		"42"  : "123456789012345"
		"106" : "1234567890"
		"105" : "CompanyName"
		"107" : "00"
		"108" : "InterfaceName"
		"109" : "Service"
		"110" : "CardType"
		"111" : "0"
		"112" : "0"
		"116" : "Feb 05 06h55:04.403"
		"117" : "001"
		"118" : "123"
		"119" : "321"
		"120" : "0"
		"121" : "1.2.3.4"
	]
]

[send "0210" from FMS as send_fms_response
	import profile::send_fms_rsp
	[
		"37"  : \copyFrom("recv_fms_request", "37")\
		"41"  : \copyFrom("recv_fms_request", "41")\
		"42"  : \copyFrom("recv_fms_request", "42")\
		"39"  : "00"
		"108"  : \copyFrom("recv_fms_request", "108")\
	]
]

[receive "0200" at AuthSinkSap1 as recv_request
	import profile::recv_request
	[
		"100" 	 : "00001"
		"102" 	 : \ignore()\
		"123" 	 : \ignore()\
		"127.22" : \ignore()\
		"127.33" : \ignore()\
	]
]

[send "0210" from AuthSinkSap1 as send_response
	import profile::send_response
	[
		"39"  : "00"		
		"100" : "00001"
		"127.33" : "6666"	
	]
]

[receive "0210" at Source as receive_request
	import profile::recv_response
	[
		"102"	 : "1234567890"
		"127.22" : 
			\ensureContains("[Fraud Checked: true]")\ & 
			\ensureContains("213Fraud:RspTime")\ & 
			\ensureContains("119Fraud:Req")\ & 
			\ensureContains("119Fraud:Rsp")\ & 
			\ensureContains("[MTI = '0200'][Field 4 = '000000000100'][Field 37 = '990000001RDM'][Field 41 = '12345678'][Field 42 = '123456789012345'][Field 105 = 'CompanyName'][Field 106 = '*'][Field 107 = '00'][Field 108 = 'InterfaceName'][Field 109 = 'Service'][Field 110 = 'CardType'][Field 111 = '0'][Field 112 = '0'][Field 114 = '0'][Field 115 = '0'][Field 116 = 'Feb 05 06h55:04.403'][Field 117 = '001'][Field 118 = '123'][Field 119 = '321'][Field 120 = '0'][Field 121 = '1.2.3.4']")\ & 
			\ensureContains("[MTI = '0210'][Field 37 = '990000001RDM'][Field 39 = '00'][Field 41 = '12345678'][Field 42 = '123456789012345'][Field 108 = 'InterfaceName']")\
		"127.33" : "6666"
	]
]

[send "0420" from Source as send_rev
	import profile::send_request
	[
		"37" : \copyFrom("send_request", "37")\
		"127.11" : \copyFrom("send_request", "127.2")\
	]
]

[receive "0430" at Source as recv_rev_tm_rsp
	import profile::recv_response
	[
		"35"	:	\ignore()\
	]
]

[receive "0420" at AuthSinkSap1 as recv_rev_adv
	import profile::recv_request
	[
		"35"	:	\ignore()\
	]
]

[send "0430" from AuthSinkSap1 as send_rev_response
	import profile::send_fms_rsp
	[
		"39" 	: "00"
		"127.2" : \copyFrom("recv_rev_adv", "127.2")\
	]
]

# Ensure the reversal was written to the SAFQ table.
[execute sql statement
	[
		"SELECT 1 FROM fraudnid_persistent_safq WHERE sent = 0 AND id = '990000001RDMInterfaceName'"
	]
	ensure row count = 1
]

[use flow _restart]

[connect "FMS"]
[ensure FMS connected]


[receive "0420" at FMS as recv_fms_reversal
	import profile::receive_fms_req
	[
		"105" : "CompanyName"
		"116" : "Feb 05 06h55:04.403"
		"117" : "001"
		"118" : "123"
		"119" : "321"
		"121" : "1.2.3.4"
	]
]

[wait for 30 seconds]

[receive "0420" at FMS
	import profile::receive_fms_req
	[
		"105" : "CompanyName"
		"116" : "Feb 05 06h55:04.403"
		"117" : "001"
		"118" : "123"
		"119" : "321"
		"121" : "1.2.3.4"
	]
]

[wait for 30 seconds]

# Update the 0420 message to make it fall out of the retention period
[execute sql statement
	[
		"UPDATE fraudnid_persistent_safq SET queue_time = DATEADD(DAY, -8, queue_time) WHERE id = '990000001RDMInterfaceName'"
	]
	ensure update count = 1
]

[receive "0420" at FMS
	import profile::receive_fms_req
	[
		"105" : "CompanyName"
		"116" : "Feb 05 06h55:04.403"
		"117" : "001"
		"118" : "123"
		"119" : "321"
		"121" : "1.2.3.4"
	]
]

#wait for a period to allow the cleaner to clear stale queries.
[wait for 30 seconds]

# Ensure the SAF record has been removed - the retention period has elapsed and allowed retries have been exceeded.
[execute sql statement
	[
		"SELECT 1 FROM fraudnid_persistent_safq WHERE sent = 0 AND id = '990000001RDMInterfaceName'"
	]
	ensure row count = 0
]

[use flow _disconnect]