<|
|======================================================================
| Restart the interchange part-way through transmission of the head of 
| the queue. It should send the remaining retries and move on to the 
| next transaction.
|======================================================================
|>

[add flow directory "../../flows"]
[add profile directory "../../profiles"]

[create process
   [
      "NET STOP PassThroughInterface"
   ]
]

# Clean the store-and-forward queue
[create process
   [
      "postjava_fraudnid", "postilion.realtime.fraudnid.helpers.ReversalGenerator", "clean"
   ]
   ensure exit code = 0
]

# Insert series of reversal records
[create process
   [
    #                                                                               make records 
    #                                                                                   |     this many
    #                                                                                   |        |   sink name   
	#                                                                                   |        |      |     start at 0 for RRN
	#                                                                                   |        |      |         |
      "postjava_fraudnid", "postilion.realtime.fraudnid.helpers.ReversalGenerator", "generate", "2", "AuthSink", "0"
   ]
   ensure exit code = 0
]

[create process
   [
      "NET START PassThroughInterface"
   ]
]

[connect FMS]
[ensure FMS connected]


# Receive and don't respond to the first reversal.
[receive "0420" at FMS as recv_fms_reversal1_1
	import profile::receive_fms_req
	[
		"37"	:	"000000000000"
	]
]
[wait for 5 seconds]

[disconnect FMS]
[create process
   [
      "NET STOP PassThroughInterface"
   ]
]
[wait for 5 seconds]

[create process
   [
      "NET START PassThroughInterface"
   ]
]
[connect FMS]
[ensure FMS connected]


[receive "0420" at FMS as recv_fms_reversal1_2
	import profile::receive_fms_req
	[
		"37"	:	"000000000000"
	]
]
[receive "0420" at FMS as recv_fms_reversal1_3
	import profile::receive_fms_req
	[
		"37"	:	"000000000000"
	]
]
# Now expect to received the next one in the sequence
[receive "0420" at FMS as recv_fms_reversal2_1
	import profile::receive_fms_req
	[
		"37"	:	"000000000001"
	]
]
# Let it go
[send "0430" from FMS as send_fms_rev_rsp2_1
	import profile::send_fms_rsp
	[
		"37"  : \copyFrom("recv_fms_reversal2_1", "37")\
		"41"  : \copyFrom("recv_fms_reversal2_1", "41")\
		"42"  : \copyFrom("recv_fms_reversal2_1", "42")\
		"39"  : "00"
		"108"  : \copyFrom("recv_fms_reversal2_1", "108")\
	]
]

# done - shouldn't receive any more because the queue should now be empty

[wait for 15 seconds]

[disconnect FMS]
