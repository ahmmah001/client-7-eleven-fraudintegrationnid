/*----------------------------------------------------------------------------*
 *                                                 
 * Copyright (c) 2018 by Montant Limited. All rights reserved.
 *                                                                             
 * This software is the confidential and proprietary property of 
 * Montant Limited and may not be disclosed, copied or distributed 
 * in any form without the express written permission of Montant Limited.
 *----------------------------------------------------------------------------*/

package postilion.realtime.fraudnid.msg;

import java.util.Hashtable;
import postilion.realtime.fraudnid.FraudNID;
import postilion.realtime.fraudnid.config.NIDConfig;
import postilion.realtime.fraudnid.config.TranFmsData;
import postilion.realtime.sdk.message.bitmap.Iso8583;
import postilion.realtime.sdk.message.bitmap.Iso8583Post;
import postilion.realtime.sdk.message.bitmap.StructuredData;
import postilion.realtime.sdk.util.convert.Pack;

/**
 * This class is used for testing the RELOADMAPPER functionality. This class 
 * should be the same as the latest working/active Mapper class in order for 
 * the relevant test cases to function. If this class is not updated, message 
 * construction/processing may be out-dated when this class is inactive, which 
 * will cause test cases to fail on transaction tests.
 */
public class Mapper_v0 implements IMapper
{
	NIDConfig config = null;
	
	/*------------------------------------------------------------------------*/
	public Mapper_v0()
	{
		// Do nothing.
	}
	
	/*------------------------------------------------------------------------*/
	public void init(NIDConfig config) 
	throws Exception
	{
		this.config = config;
	}
	
	/*------------------------------------------------------------------------*/
	public Iso8583FMS constructTranReqToFMS(
			Iso8583Post msgFromTM, 
			int interfaceIndex, 
			TranFmsData tranFmsData)
	throws Exception
	{
		Iso8583FMS msgToFMS = new Iso8583FMS();
		
		// Message Type.
		msgToFMS.putMsgType(Iso8583FMS.MsgType._0200_TRAN_REQ);
		
		// Field 4 - Amount.
		constructAmountToFMS(msgFromTM, tranFmsData, interfaceIndex, msgToFMS);
		
		// Field 37 - Master Transaction ID.
		constructMasterTranIdToFMS(msgFromTM, msgToFMS);
		
		// Field 41 - POS/ECR ID.
		constructPosEcrIdToFMS(msgFromTM, msgToFMS);
		
		// Field 42 - Store Number.
		constructStoreNumberToFMS(msgFromTM, msgToFMS);
		
		// Field 105 - Company.
		constructCompanyNameToFMS(msgFromTM, msgToFMS);
		
		// Field 106 - Account Number/Reference.
		String accNr = constructAccNrOrRefToFMS(msgFromTM, interfaceIndex, tranFmsData, msgToFMS);
		
		// Field 107 - Transaction Type.
		constructTranTypeToFMS(interfaceIndex, tranFmsData, msgToFMS);
		
		// Field 108 - Interface Name.
		constructInterfaceNameToFMS(interfaceIndex, tranFmsData, msgToFMS);
		
		// Field 109 - Service Name.
		constructServiceNameToFMS(msgFromTM, tranFmsData, config.htTdnIssuerData, msgToFMS);
		
		// Field 110 - Card Type.
		constructCardTypeToFMS(msgFromTM, tranFmsData, msgToFMS);
		
		// Field 111 - Service Number.
		constructServiceNumberToFMS(accNr, tranFmsData, msgToFMS);
		
		// Field 112 - Optional 1.
		msgToFMS.putField(Iso8583FMS.Bit._112_OPTIONAL_1, _0_ZERO_STR);
		
		// Field 114 - Optional 2.
		constructOptionalField2ToFMS(msgFromTM, tranFmsData, msgToFMS);
		
		// Field 115 - Optional 3.
		constructOptionalField3ToFMS(msgFromTM, tranFmsData, msgToFMS);
		
		// Field 116 - Transaction Date/Time.
		constructTranDateTimeToFMS(msgFromTM, msgToFMS);
		
		// Field 117 - Shift Number.
		constructShiftNumberToFMS(msgFromTM, msgToFMS);
		
		// Field 118 - Casher/Employee Number.
		constructCashierEmployeeNumberToFMS(msgFromTM, msgToFMS);
		
		// Field 119 - Ticket Number.
		constructTicketNumberToFMS(msgFromTM, msgToFMS);
		
		// Field 120 - Reserved.
		msgToFMS.putField(Iso8583FMS.Bit._120_RESERVED, _0_ZERO_STR);
		
		// Field 121 - POS/ECR IP Address.
		constructPosEcrIpAddressToFMS(msgFromTM, msgToFMS);
		
		return msgToFMS;
	}
	
	/*------------------------------------------------------------------------*/
	public Iso8583FMS constructAcquirerRevAdvToFMS(Iso8583FMS msgToFMS)
	throws Exception
	{
		// Instantiation and Fields.
		Iso8583FMS revAdvToFMS = (Iso8583FMS) msgToFMS.clone();
		
		// Message Type.
		revAdvToFMS.putMsgType(Iso8583FMS.MsgType._0420_ACQUIRER_REV_ADV);

		return revAdvToFMS;
	}
	
	/*------------------------------------------------------------------------*/
	public Iso8583Post constructDeclineToTM(
			Iso8583Post msgFromTM, 
			Iso8583FMS msgToFMS, 
			Iso8583FMS msgFromFMS)
	throws Exception
	{
		// Clone the original request.
		Iso8583Post msgToTM = (Iso8583Post) msgFromTM.clone();
		
		// Message Type.
		msgToTM.putMsgType(
				Iso8583Post.MsgType.getResponse(msgFromTM.getMsgType()));
		
		// Field 39 - Response Code.
		msgToTM.copyFieldFrom(Iso8583FMS.Bit._039_RSP_CODE, msgFromFMS);
		
		// Field 127.9 - Additional Node Data.
		String fraudRspText = 
			constructAdditionalNodeDataToTM(
					msgFromFMS, 
					msgToTM, 
					config.htRspCodeMap, 
					true);
		
		// Field 127.22 - StructuredData.
		constructStructuredData(msgToTM, msgToFMS, msgFromFMS, fraudRspText);
		
		return msgToTM;
	}

	/*------------------------------------------------------------------------*/
	public void processMsgFromRemoteSink(
			Iso8583Post msgToTM, 
			Iso8583FMS fraudReq, 
			Iso8583FMS fraudRsp)
	throws Exception
	{
		// Retrieve the fraud response mapped text description.
		String fraudRspText = 
			constructAdditionalNodeDataToTM(
					fraudRsp, 
					msgToTM, 
					config.htRspCodeMap, 
					false);
		
		// Field 127.22 - StructuredData.
		constructStructuredData(msgToTM, fraudReq, fraudRsp, fraudRspText);
	}

	/*------------------------------------------------------------------------*/
	public void processMsgFromTranmgrSinkNode(
			Iso8583Post msgFromTM, 
			Iso8583FMS fraudReq)
	throws Exception
	{
		// Retrieve the fraud response mapped text description.
		String fraudRspText = 
				constructAdditionalNodeDataToRemote(msgFromTM, config.htRspCodeMap);
			
		// Field 127.22 - StructuredData.
		constructStructuredData(msgFromTM, fraudReq, null, fraudRspText);
	}
	
	/*------------------------------------------------------------------------*/
	protected void constructAmountToFMS(
			Iso8583Post msgFromTM, 
			TranFmsData tranFmsData, 
			int interfaceIndex, 
			Iso8583FMS msgToFMS)
	throws Exception
	{
		String amount = EMPTY_STRING;
		String amountField = 
				tranFmsData.getFieldAmount().split(
						PIPE_DELIM)[interfaceIndex];
		
		if (amountField != null 
				&& amountField.contains(StructDataTags.FIELD_STR))
		{
			amount = msgFromTM.getStructuredData().get(amountField);
		}
		else
		{
			amount = msgFromTM.getField(amountField);
		}
		
		msgToFMS.putField(
				Iso8583FMS.Bit._004_AMOUNT_TRANSACTION, 
				Pack.resize(amount, 12, '0', false));
	}
	
	/*------------------------------------------------------------------------*/
	protected void constructMasterTranIdToFMS(
			Iso8583Post msgFromTM, 
			Iso8583FMS msgToFMS) 
	throws Exception
	{
		msgToFMS.putField(
				Iso8583FMS.Bit._037_RETRIEVAL_REF_NR, 
				Pack.resize(
					msgFromTM.getField(Iso8583Post.Bit._037_RETRIEVAL_REF_NR), 
					12, 
					'0', 
					true));
	}
	
	/*------------------------------------------------------------------------*/
	protected void constructPosEcrIdToFMS(
			Iso8583Post msgFromTM, 
			Iso8583FMS msgToFMS) 
	throws Exception
	{
		String value = EMPTY_STRING;
		String structDataVal = 
				msgFromTM.getStructuredData().get(
						StructDataTags.BOX_NR_FIELD_41);
		
		if (!FraudNID.isStrNullOrEmpty(structDataVal))
		{
			value = structDataVal;
		}
		else
		{
			value = 
				msgFromTM.getField(Iso8583Post.Bit._041_CARD_ACCEPTOR_TERM_ID);
		}
		
		msgToFMS.putField(
				Iso8583FMS.Bit._041_CARD_ACCEPTOR_TERM_ID, 
				Pack.resize(
					value, 
					8, 
					'0', 
					true));
	}
	
	/*------------------------------------------------------------------------*/
	protected void constructStoreNumberToFMS(
			Iso8583Post msgFromTM, 
			Iso8583FMS msgToFMS) 
	throws Exception
	{
		String value = null;
		String structDataVal = 
				msgFromTM.getStructuredData().get(
						StructDataTags.CARD_ACC_ID_FIELD_42);
		
		if (!FraudNID.isStrNullOrEmpty(structDataVal))
		{
			value = structDataVal.substring(0, 15);
		}
		else
		{
			value = 
				msgFromTM.getField(Iso8583Post.Bit._042_CARD_ACCEPTOR_ID_CODE);
		}
		
		msgToFMS.putField(Iso8583FMS.Bit._042_CARD_ACCEPTOR_ID_CODE, value);
	}
	
	/*------------------------------------------------------------------------*/
	protected void constructCompanyNameToFMS(
			Iso8583Post msgFromTM, 
			Iso8583FMS msgToFMS) 
	throws Exception
	{
		String structDataVal = 
				msgFromTM.getStructuredData().get(
						StructDataTags.COMPANY_FIELD_00);
		
		if (!FraudNID.isStrNullOrEmpty(structDataVal))
		{
			msgToFMS.putField(Iso8583FMS.Bit._105_COMPANY, structDataVal);
		}
	}
	
	/*------------------------------------------------------------------------*/
	protected String constructAccNrOrRefToFMS(
			Iso8583Post msgFromTM, 
			int interfaceIndex, 
			TranFmsData tranFmsData, 
			Iso8583FMS msgToFMS) 
	throws Exception
	{
		String accNr = EMPTY_STRING;
		String accField = 
				tranFmsData.getFieldAccountRefNum().split(
									PIPE_DELIM)[interfaceIndex];
		
		// Retrieve the account number/reference from the configured field.
		if (accField != null 
				&& accField.contains(StructDataTags.FIELD_STR))
		{
			accNr = msgFromTM.getStructuredData().get(accField);
		}
		else
		{
			accNr = msgFromTM.getField(accField);
		}
		
		// Evaluate the FMS ID to determine if special processing is required.
		// Retrieve the telephone number for TDN/DataLogic transactions.
		int idFms = Integer.valueOf(tranFmsData.getIdFms());
		if (idFms == TranFmsData.IdFms._13_TDN_CASH 
			|| idFms == TranFmsData.IdFms._27_TDN_TCD_WITHOUT_POINTS 
			|| idFms == TranFmsData.IdFms._28_TDN_TCD_POINTS
			|| idFms == TranFmsData.IdFms._14_DATALOGIC_CASH 
			|| idFms == TranFmsData.IdFms._29_DATALOGIC_TCD_WITHOUT_POINTS 
			|| idFms == TranFmsData.IdFms._30_DATALOGIC_TCD_POINTS)
		{
			if (interfaceIndex == 1 
				|| idFms == TranFmsData.IdFms._13_TDN_CASH 
				|| idFms == TranFmsData.IdFms._14_DATALOGIC_CASH)
			{
				accNr = accNr.substring(accNr.length() - 10, accNr.length());	
			}
		}
		
		// Prepare the value to set in the message.
		String toSetInMsg = accNr;
		
		// Store the "un-hashed" account/reference number.
		String notMd5AccNr = new String(accNr);
		
		// Get the MD5 hash of the value if configured to do so.
		if(!FraudNID.isStrNullOrEmpty(accNr) && !NIDConfig.SupportedHashFunction.FALSE_NONE.contentEquals(config.accountHashAlgorithm))
		{
			toSetInMsg = Iso8583FMS.getHashValue(accNr,"MD5");
		}
		
		msgToFMS.putField(Iso8583FMS.Bit._106_ACCOUNT_NR_REF, toSetInMsg);
		
		return notMd5AccNr;
	}
	
	/*------------------------------------------------------------------------*/
	protected void constructTranTypeToFMS(
			int interfaceIndex, 
			TranFmsData tranFmsData, 
			Iso8583FMS msgToFMS) 
	throws Exception
	{
		String tranType = 
				tranFmsData.getTranType().split(
						PIPE_DELIM)[interfaceIndex];
		
		if (!FraudNID.isStrNullOrEmpty(tranType))
		{
			msgToFMS.putField(Iso8583FMS.Bit._107_TRAN_TYPE, tranType);
		}
	}
	
	/*------------------------------------------------------------------------*/
	protected void constructInterfaceNameToFMS(
			int interfaceIndex, 
			TranFmsData tranFmsData, 
			Iso8583FMS msgToFMS) 
	throws Exception
	{
		String nameInterface = 
				tranFmsData.getNameInterface().split(
						PIPE_DELIM)[interfaceIndex];
		
		if (!FraudNID.isStrNullOrEmpty(nameInterface))
		{
			msgToFMS.putField(
					Iso8583FMS.Bit._108_INTERFACE_NAME, 
					nameInterface);
		}
	}
	
	/*------------------------------------------------------------------------*/
	protected void constructServiceNameToFMS(
			Iso8583Post msgFromTM, 
			TranFmsData tranFmsData, 
			Hashtable<String, String> htTdnIssuerData, 
			Iso8583FMS msgToFMS) 
	throws Exception
	{
		StructuredData structData = msgFromTM.getStructuredData();
		String service = structData.get(StructDataTags.SERVICE_FIELD_9997);
		
		if (FraudNID.isStrNullOrEmpty(service))
		{
			service = tranFmsData.getService();
			
			// Evaluate the FMS ID to determine if special processing is 
			// required.
			int idFms = Integer.valueOf(tranFmsData.getIdFms());
			if(idFms == TranFmsData.IdFms._13_TDN_CASH 
				|| idFms == TranFmsData.IdFms._27_TDN_TCD_WITHOUT_POINTS 
				|| idFms == TranFmsData.IdFms._28_TDN_TCD_POINTS) 
			{
				String issuer = EMPTY_STRING;
				String issuerTdn = 
					htTdnIssuerData.get(
							structData.get(StructDataTags.TDN_SKU_FIELD_131));
				String[] issuers = service.split(PIPE_DELIM);
				
				for (String value : issuers)
				{
					if (issuerTdn != null 
						&& value.toLowerCase().contains(issuerTdn.toLowerCase()))
					{
						issuer = value;
						
						// If a match has been found, exit the loop.
						break;
					}
				}
				
				service = issuer.split(HASH_DELIM)[0];
			}
			else if (idFms == TranFmsData.IdFms._14_DATALOGIC_CASH 
					|| idFms == TranFmsData.IdFms._29_DATALOGIC_TCD_WITHOUT_POINTS  
					|| idFms == TranFmsData.IdFms._30_DATALOGIC_TCD_POINTS)
			{
				String issuer = EMPTY_STRING;
				String[] issuers = 
						tranFmsData.getService().split(PIPE_DELIM);
				
				for (String value: issuers)
				{
					if (value.contains(structData.get(
							StructDataTags.DATALOGIC_ISSUER_FIELD_2)))
					{
						issuer = value;
						
						// If a match has been found, exit the loop.
						break;
					}
				}
				
				service = issuer.split(HASH_DELIM)[0];
			}
		}
		
		msgToFMS.putField(Iso8583FMS.Bit._109_SERVICE_NAME, service);
	}
	
	/*------------------------------------------------------------------------*/
	protected void constructCardTypeToFMS(
			Iso8583Post msgFromTM, 
			TranFmsData tranFmsData, 
			Iso8583FMS msgToFMS) 
	throws Exception
	{
		String value = null;
		String structDataVal = 
				msgFromTM.getStructuredData().get(
						StructDataTags.CARD_TYPE_FIELD_9996);
		
		if (!FraudNID.isStrNullOrEmpty(structDataVal))
		{
			value = structDataVal;
		}
		else
		{
			value = tranFmsData.getCardType();
		}
		
		msgToFMS.putField(Iso8583FMS.Bit._110_CARD_TYPE, value);
	}
	
	/*------------------------------------------------------------------------*/
	protected void constructServiceNumberToFMS(
			String accNr, 
			TranFmsData tranFmsData, 
			Iso8583FMS msgToFMS) 
	throws Exception
	{
		String serviceNumber = TranFmsData.IdFms._0_DEFAULT_SERVICE_NR;
		
		// Evaluate the FMS ID to determine if special processing is required.
		int idFms = Integer.valueOf(tranFmsData.getIdFms());
		if (idFms == TranFmsData.IdFms._3_BBVA_PAYMENT_SERVICES 
			|| idFms == TranFmsData.IdFms._6_BANORTE_PAYMENT_SERVICES)
		{
			serviceNumber = accNr;
		}
		
		msgToFMS.putField(Iso8583FMS.Bit._111_SERVICE_NR, serviceNumber);
	}
	
	/*------------------------------------------------------------------------*/
	protected void constructOptionalField2ToFMS(
			Iso8583Post msgFromTM, 
			TranFmsData tranFmsData, 
			Iso8583FMS msgToFMS) 
	throws Exception
	{
		String value = _0_ZERO_STR;
		
		// Evaluate the FMS ID to determine if special processing is required.
		if (Integer.valueOf(tranFmsData.getIdFms()) == 
				TranFmsData.IdFms._25_BBCA_BUY_MG_WITHOUT_POINTS)
		{
			String structDataVal = 
					msgFromTM.getStructuredData().get(
							StructDataTags.FIELD_8001);
			
			if(!FraudNID.isStrNullOrEmpty(structDataVal))
			{
				value = structDataVal;
			}
		}
		
		msgToFMS.putField(Iso8583FMS.Bit._114_OPTIONAL_2, value);
	}
	
	/*------------------------------------------------------------------------*/
	protected void constructOptionalField3ToFMS(
			Iso8583Post msgFromTM, 
			TranFmsData tranFmsData, 
			Iso8583FMS msgToFMS) 
	throws Exception
	{
		String value = _0_ZERO_STR;
		
		// Evaluate the FMS ID to determine if special processing is required.
		if (Integer.valueOf(tranFmsData.getIdFms()) == 
				TranFmsData.IdFms._25_BBCA_BUY_MG_WITHOUT_POINTS)
		{
			String structDataVal = 
					msgFromTM.getStructuredData().get(
							StructDataTags.FIELD_8000);
			
			if(!FraudNID.isStrNullOrEmpty(structDataVal))
			{
				value = structDataVal;
			}
		}
		
		msgToFMS.putField(Iso8583FMS.Bit._115_OPTIONAL_3, value);
	}
	
	/*------------------------------------------------------------------------*/
	protected void constructTranDateTimeToFMS(
			Iso8583Post msgFromTM, 
			Iso8583FMS msgToFMS) 
	throws Exception
	{
		String structDataVal = 
				msgFromTM.getStructuredData().get(
						StructDataTags.DATE_TIME_TRAN_HOST);
		
		if(!FraudNID.isStrNullOrEmpty(structDataVal))
		{
			msgToFMS.putField(
					Iso8583FMS.Bit._116_TRAN_DATE_TIME, 
					structDataVal);
		}
	}
	
	/*------------------------------------------------------------------------*/
	protected void constructShiftNumberToFMS(
			Iso8583Post msgFromTM, 
			Iso8583FMS msgToFMS) 
	throws Exception
	{
		String structDataVal = 
				msgFromTM.getStructuredData().get(
						StructDataTags.TURN_NR_FIELD_128);
		
		if(!FraudNID.isStrNullOrEmpty(structDataVal))
		{
			msgToFMS.putField(Iso8583FMS.Bit._117_SHIFT_NR, structDataVal);
		}
	}
	
	/*------------------------------------------------------------------------*/
	protected void constructCashierEmployeeNumberToFMS(
			Iso8583Post msgFromTM, 
			Iso8583FMS msgToFMS) 
	throws Exception
	{
		String structDataVal = 
				msgFromTM.getStructuredData().get(
						StructDataTags.CASHIER_EMPL_NR_FIELD_127);
		
		if(!FraudNID.isStrNullOrEmpty(structDataVal))
		{
			msgToFMS.putField(
					Iso8583FMS.Bit._118_CASHIER_EMPLOYEE_NR, 
					structDataVal);
		}
	}
	
	/*------------------------------------------------------------------------*/
	protected void constructTicketNumberToFMS(
			Iso8583Post msgFromTM, 
			Iso8583FMS msgToFMS) 
	throws Exception
	{
		String structDataVal = 
				msgFromTM.getStructuredData().get(
						StructDataTags.TICKET_NR_FIELD_129);
		
		if(!FraudNID.isStrNullOrEmpty(structDataVal))
		{
			msgToFMS.putField(Iso8583FMS.Bit._119_TICKET_NR, structDataVal);
		}
	}
	
	/*------------------------------------------------------------------------*/
	protected void constructPosEcrIpAddressToFMS(
			Iso8583Post msgFromTM, 
			Iso8583FMS msgToFMS) 
	throws Exception
	{
		String structDataVal = 
				msgFromTM.getStructuredData().get(
						StructDataTags.FIELD_IP);
		
		if(!FraudNID.isStrNullOrEmpty(structDataVal))
		{
			msgToFMS.putField(Iso8583FMS.Bit._121_POS_ECR_IP, structDataVal);
		}
	}
	
	/*------------------------------------------------------------------------*/
	protected String constructAdditionalNodeDataToTM(
			Iso8583FMS msgFromFMS,
			Iso8583Post msgToTM, 
			Hashtable<String, String> htRspCodeMap,
			boolean updateMsgField)
	throws Exception
	{
		String rspText = EMPTY_STRING;
		if (msgFromFMS != null)
		{
			rspText = htRspCodeMap.get(msgFromFMS.getResponseCode());
		}
		 
		if (FraudNID.isStrNullOrEmpty(rspText))
		{
			rspText = AUTH_RSP_NOTIFY_CAS;
		}
		
		if (updateMsgField)
		{
			msgToTM.putPrivField(
					Iso8583Post.PrivBit._009_ADDITIONAL_NODE_DATA, 
					rspText);
		}
		
		return rspText;
	}
	
	/*------------------------------------------------------------------------*/
	protected String constructAdditionalNodeDataToRemote(
			Iso8583Post msgToTM, 
			Hashtable<String, String> htRspCodeMap)
	throws Exception
	{
		String rspText = AUTH_RSP_NOTIFY_CAS;
		
		if (msgToTM.isFieldSet(Iso8583.Bit._039_RSP_CODE))
		{
			String mappedText = htRspCodeMap.get(msgToTM.getResponseCode());
			if (!FraudNID.isStrNullOrEmpty(mappedText))
			{
				rspText = mappedText;
			}
		}
		
		return rspText;
	}
	
	/*------------------------------------------------------------------------*/
	protected void constructStructuredData(
			Iso8583Post msg,
			Iso8583FMS fraudReq, 
			Iso8583FMS fraudRsp,
			String fraudRspText)
	throws Exception
	{
		StructuredData structData = new StructuredData();
		boolean checked = false;
		
		long reqTime = 0;
		if (fraudReq != null)
		{
			reqTime = fraudReq.getReqSendTime();
			
			// Fraud:Req.
			structData.put(
				StructDataTags.FRAUD_REQ, 
				FRAUD_REQ_PREFIX + 
					fraudReq.toStructDataString(config.accountHashAlgorithm) + 
					FRAUD_REQ_SUFFIX);
		}
		
		if (fraudRsp == null)
		{
			structData.put(
					StructDataTags.FRAUD_CHECKED, 
					FRAUD_CHECKED_PREFIX + 
						checked + 
						FRAUD_CHECKED_SUFFIX);
			
			structData.put(
					StructDataTags.FRAUD_RSP_TIME, 
					FRAUD_RSP_TIME_PREFIX + 
						FRAUD_SYSTEM_TIMEOUT + 
						FRAUD_RSP_TIME_SUFFIX);
		}
		else
		{
			// Fraud:Rsp.
			structData.put(
				StructDataTags.FRAUD_RSP, 
				FRAUD_RSP_PREFIX + 
					fraudRsp.toStructDataString(config.accountHashAlgorithm) + 
					FRAUD_RSP_SUFFIX);
			
			// Fraud:Checked.
			if (fraudRsp.isFieldSet(Iso8583FMS.Bit._039_RSP_CODE)) 
			{
				checked = true;
			}
			
			structData.put(
					StructDataTags.FRAUD_CHECKED, 
					FRAUD_CHECKED_PREFIX + 
						checked + 
						FRAUD_CHECKED_SUFFIX);
			
			// Fraud:RspTime.
			structData.put(
					StructDataTags.FRAUD_RSP_TIME, 
					FRAUD_RSP_TIME_PREFIX + 
						String.valueOf(fraudRsp.getRspReceiveTime() - reqTime) + 
						FRAUD_RSP_TIME_SUFFIX);
			
			// FIELD_8 & FIELD_21.
			String interfaceName = 
					fraudRsp.getField(Iso8583FMS.Bit._108_INTERFACE_NAME);
			if (!FraudNID.isStrNullOrEmpty(interfaceName))
			{
				if (InterfaceName.PAYBACK.equals(interfaceName.toLowerCase()))
				{
					structData.put(StructDataTags.FIELD_21, fraudRspText);
				}
				else if (InterfaceName.PADEMOBILE.equals(interfaceName.toLowerCase()))
				{
					structData.put(StructDataTags.FIELD_8, fraudRspText);
				}
			}
		}
		
		msg.putStructuredData(structData);
	}
	
	/*------------------------------------------------------------------------*/
	
	public static final String EMPTY_STRING 				 = "";
	public static final String _0_ZERO_STR 					 = "0";
	public static final String HASH_DELIM 					 = "#";
	public static final String PIPE_DELIM 		 			 = "\\|";
	public static final String FRAUD_REQ_PREFIX			 	 = "[Fraud Request: ";
	public static final String FRAUD_REQ_SUFFIX			 	 = "]:Fraud Request]";
	public static final String FRAUD_RSP_PREFIX			 	 = "[Fraud Response: ";
	public static final String FRAUD_RSP_SUFFIX			 	 = "]:Fraud Response]";
	public static final String FRAUD_CHECKED_PREFIX			 = "[Fraud Checked: ";
	public static final String FRAUD_CHECKED_SUFFIX			 = "]:Fraud Checked]";
	public static final String FRAUD_RSP_TIME_PREFIX		 = "[Fraud Response Time: ";
	public static final String FRAUD_RSP_TIME_SUFFIX		 = "]:Fraud Response Time]";
	public static final String FRAUD_SYSTEM_TIMEOUT 		 = "timeout";
	public static final String AUTH_RSP_NOTIFY_CAS 			 = 
									"Respuesta del autorizador. Avise al CAS.";
	
	/*------------------------------------------------------------------------*/
	public static class StructDataTags
	{
		public static final String FRAUD_CHECKED 			 = "Fraud:Checked";
		public static final String FRAUD_RSP_TIME 			 = "Fraud:RspTime";
		public static final String FRAUD_REQ 				 = "Fraud:Req";
		public static final String FRAUD_RSP 				 = "Fraud:Rsp";
		public static final String FIELD_STR		 		 = "FIELD";
		public static final String FIELD_IP 				 = "FIELD_IP";
		public static final String COMPANY_FIELD_00 		 = "FIELD_00";
		public static final String DATALOGIC_ISSUER_FIELD_2  = "FIELD_2";
		public static final String FIELD_8   				 = "FIELD_8";
		public static final String FIELD_21   				 = "FIELD_21";
		public static final String BOX_NR_FIELD_41  		 = "FIELD_41";
		public static final String CARD_ACC_ID_FIELD_42 	 = "FIELD_42";
		public static final String CASHIER_EMPL_NR_FIELD_127 = "FIELD_127";
		public static final String TURN_NR_FIELD_128 		 = "FIELD_128";
		public static final String TICKET_NR_FIELD_129 		 = "FIELD_129";
		public static final String TDN_SKU_FIELD_131		 = "FIELD_131";
		public static final String FIELD_8000 				 = "FIELD_8000";
		public static final String FIELD_8001 				 = "FIELD_8001";
		public static final String SEQ_NR_FIELD_9994 		 = "FIELD_9994";
		public static final String CARD_TYPE_FIELD_9996 	 = "FIELD_9996";
		public static final String SERVICE_FIELD_9997 		 = "FIELD_9997";
		public static final String DATE_TIME_TRAN_HOST 		 = "HOST_DATETIME_FULL";
	}
	
	/*------------------------------------------------------------------------*/
	public static class InterfaceName
	{
		public static final String PAYBACK 					 = "payback";
		public static final String PADEMOBILE 				 = "pademobile";
	}
	
	/*------------------------------------------------------------------------*/
}