@setlocal
@PATH=%PATH%;%java_home%\bin
javac -g -d "%postiliondir%\fraudnid\java\classes" -cp "..\..\build\classes;%postiliondir%\realtime\java\classes;%postiliondir%\testharness\java\classes" java\postilion\realtime\fraudnid\msg\*.java
javac -g -d "%postiliondir%\fraudnid\java\classes" -cp "..\..\build\classes;%postiliondir%\realtime\java\classes;%postiliondir%\testharness\java\classes" ..\..\source\test\java\postilion\realtime\fraudnid\helpers\*.java
javac -g -d "%postiliondir%\passthroughinterface\java\classes" -cp "..\..\build\classes;%postiliondir%\realtime\java\classes;%postiliondir%\testharness\java\classes" ..\..\source\test\java\postilion\realtime\passthroughinterface\*.java
@endlocal

