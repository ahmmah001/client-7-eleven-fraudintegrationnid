Fraud Integration NID
================================================================================
 
Follow the instructions below to run the test cases in this directory:

1. Install the following:
	> Realtime Framework v5.3.02 up to Patch 195.
	> PassThroughInterface v3.0 with no patches.
	> Fraud Integration NID (This product).
	
2. Add "#include ..\..\testharness\java\.classpath.win32" to the
   %postiliondir%\fraudnid\java\.classpath file. The test mapper class will be
   built to the TestHarness java\classes folder.

2. Execute run.cmd which will stop the services, build the test class/es, load 
   the configuration, start the services and run the test suite.

================================================================================
Notes
================================================================================
PerfMon
	- To test with PerfMon, load the Counter Configuration in ..\perfmon and 
	  start the counter set prior to starting the test suite.
	- Config File created on Windows XP. Newer Windows may not recognize it.