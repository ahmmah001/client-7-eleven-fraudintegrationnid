USE realtime
GO-- Prepare the realtime database.


EXEC ppc_rtfw_clean 1,0
EXEC tm_tv_manual_reset

-- issuers_tdn Table.
IF EXISTS (SELECT * FROM sysobjects WHERE name='issuers_tdn' AND xtype='U')
BEGIN
	DROP TABLE issuers_tdn
END 
GO

CREATE TABLE [dbo].[issuers_tdn] ([sku] varchar(50) NULL, [issuer] varchar(50) NULL)
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('dbo.sp_get_issuers_tdn'))
BEGIN
	DROP PROCEDURE dbo.sp_get_issuers_tdn
END 
GO

INSERT INTO issuers_tdn (sku, issuer) VALUES ('123', 'Ser')
GO

-- issuers_tdn Stored Procedure.
EXEC
	('
	CREATE PROCEDURE [dbo].[sp_get_issuers_tdn]
	AS
	BEGIN
      SELECT itdn.sku, itdn.issuer as resp_text 
      FROM issuers_tdn itdn
	END')
GO

-- resp_codes_txt Table.
IF EXISTS (SELECT * FROM sysobjects WHERE name='resp_codes_txt' AND xtype='U')
BEGIN
	DROP TABLE resp_codes_txt
END
GO

CREATE TABLE [dbo].[resp_codes_txt] ([node_name] [varchar](50) NULL, [resp_code] [char](2) NULL, [resp_text] [varchar](100) NULL)
GO

INSERT INTO resp_codes_txt (node_name, resp_code, resp_text) VALUES ('CtrlFraudes', '05', 'Do not honor')
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('dbo.sp_get_response_codes'))
BEGIN
	DROP PROCEDURE dbo.sp_get_response_codes
END

-- resp_codes_txt Stored Procedure.
EXEC
	('
	CREATE PROCEDURE [dbo].[sp_get_response_codes]
	@sink_node_name VARCHAR(30)
	AS
	BEGIN
		SET NOCOUNT ON;
		SELECT * FROM resp_codes_txt
	END')
GO

-- SWH_CAT_FMS - a pre-existing table that gets migrated during installation
IF EXISTS (SELECT * FROM sysobjects WHERE name='SWH_CAT_FMS' AND xtype='U')
BEGIN
	DROP TABLE SWH_CAT_FMS
END
GO

CREATE TABLE [dbo].[SWH_CAT_FMS](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[desc_tran] [varchar](100) NOT NULL,
	[id_fms] [int] NOT NULL,
	[tran_type] [varchar](10) NOT NULL,
	[name_interface] [varchar](100) NOT NULL,
	[service] [varchar](500) NULL,
	[card_type] [int] NOT NULL,
	[field_account_ref_num] [varchar](100) NOT NULL,
	[field_amount] [varchar](100) NOT NULL,
	[field_optional_1] [varchar](50) NULL,
	[field_optional_2] [varchar](50) NULL,
	[field_optional_3] [varchar](50) NULL)
GO

-- Free-floating table with interface-specific configuration
IF EXISTS (SELECT * FROM sysobjects WHERE name='swh_postilionconfiguracion' AND xtype='U')
BEGIN
	DROP TABLE swh_postilionconfiguracion
END
GO


CREATE TABLE [dbo].[swh_postilionconfiguracion](
	[id_Configuracion] [int] IDENTITY(1,1) NOT NULL,
	[sink_node] [varchar](85) NULL,
	[nomClave] [varchar](50) NULL,
	[strValor] [varchar](50) NULL,
	[id_Compania] [int] NULL)
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('dbo.swh_postilionconfiguracion_sel'))
BEGIN
	DROP PROCEDURE swh_postilionconfiguracion_sel
END
GO

-- Procedure for the interface specific config (also not under our control)
EXEC
	('
		CREATE PROCEDURE [dbo].[swh_postilionconfiguracion_sel]
		(
			@sink_node VARCHAR(85),
			@id_compania INT
		)
		AS
			SELECT *
			FROM
				dbo.swh_postilionconfiguracion
			WHERE
				sink_node = @sink_node
				AND id_compania = @id_compania
	')
GO


-- fraudnid_persistent_safq
DELETE FROM fraudnid_persistent_safq
