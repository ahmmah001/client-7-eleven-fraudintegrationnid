[add profile directory "../profiles"]

[connect "FMS"]
[ensure FMS connected]

[send "0200" from Source as send_request
	import profile::send_request
	[
		"100" : "00001"
		"102" : "1234567890"
		"127.22" : 
				\ISO8583Post.structuredData("TVP_PARAMS", 
				"FIELD_9998", "1",
				"FIELD_00", "CompanyName", 
				"FIELD_128", "001", 
				"FIELD_127", "123", 
				"FIELD_129", "321", 
				"FIELD_IP", "1.2.3.4", 
				"HOST_DATETIME_FULL", "Feb 05 06h55:04.403" 
				)\
		"127.33" : "6666"
	]
]

[receive "0200" at FMS as recv_fms_request
	import profile::receive_fms_req
	[
		"4"   : "000000000100"
		"37"  : \compareTo("send_request", "37")\
		"41"  : "12345678"
		"42"  : "123456789012345"
		"106" : "1234567890"
		"105" : "CompanyName"
		"107" : "00"
		"108" : "InterfaceName"
		"109" : "Service"
		"110" : "CardType"
		"111" : "0"
		"112" : "0"
		"116" : "Feb 05 06h55:04.403"
		"117" : "001"
		"118" : "123"
		"119" : "321"
		"120" : "0"
		"121" : "1.2.3.4"
	]
]

[send "0210" from FMS as send_fms_response
	import profile::send_fms_rsp
	[
		"37"  : \copyFrom("recv_fms_request", "37")\
		"41"  : \copyFrom("recv_fms_request", "41")\
		"42"  : \copyFrom("recv_fms_request", "42")\
		"39"  : "00"
		"108"  : \copyFrom("recv_fms_request", "108")\
	]
]

[receive "0200" at AuthSinkSap1 as recv_request
	import profile::recv_request
	[
		"100" 	 : "00001"
		"102" 	 : \ignore()\
		"123" 	 : \ignore()\
		"127.22" : \ignore()\
		"127.33" : \ignore()\
	]
]

[send "0210" from AuthSinkSap1 as send_response
	import profile::send_response
	[
		"39"  : "00"		
		"100" : "00001"
		"127.33" : "6666"	
	]
]

[receive "0210" at Source as receive_request
	import profile::recv_response
	[
		"102"	 : "1234567890"
		"127.22" : 
			\ensureContains("[Fraud Checked: true]")\ & 
			\ensureContains("213Fraud:RspTime")\ & 
			\ensureContains("119Fraud:Req")\ & 
			\ensureContains("119Fraud:Rsp")\ & 
			\ensureContains("[MTI = '0200'][Field 4 = '000000000100'][Field 37 = '" + \copyFrom("send_request", "37")\ + "'][Field 41 = '12345678'][Field 42 = '123456789012345'][Field 105 = 'CompanyName'][Field 106 = '*'][Field 107 = '00'][Field 108 = 'InterfaceName'][Field 109 = 'Service'][Field 110 = 'CardType'][Field 111 = '0'][Field 112 = '0'][Field 114 = '0'][Field 115 = '0'][Field 116 = 'Feb 05 06h55:04.403'][Field 117 = '001'][Field 118 = '123'][Field 119 = '321'][Field 120 = '0'][Field 121 = '1.2.3.4']")\ & 
			\ensureContains("[MTI = '0210'][Field 37 = '" + \copyFrom("send_request", "37")\ + "'][Field 39 = '00'][Field 41 = '12345678'][Field 42 = '123456789012345'][Field 108 = 'InterfaceName']")\
		"127.33" : "6666"
	]
]

[send "0420" from Source as send_rev
	import profile::send_request
	[
		"37"	: \copyFrom("send_request", "37")\
		"127.11" : \copyFrom("send_request", "127.2")\
	]
]

[receive "0430" at Source as recv_rev_tm_rsp
	import profile::recv_response
	[
		# Some versions of TM will return track 2 data in the response
		"35"	:	\ignore()\
	]
]

[receive "0420" at AuthSinkSap1 as recv_rev_adv
	import profile::recv_request
	[
		# Some versions of TM will return track 2 data in the response
		"35"	:	\ignore()\
	]
]

[send "0430" from AuthSinkSap1 as send_rev_response
	import profile::send_fms_rsp
	[
		"39" 	: "00"
		"127.2" : \copyFrom("recv_rev_adv", "127.2")\
	]
]

[wait for 5 seconds]

[receive "0420" at FMS as recv_fms_reversal
	import profile::receive_fms_req
	[
		"105" : "CompanyName"
		"116" : "Feb 05 06h55:04.403"
		"117" : "001"
		"118" : "123"
		"119" : "321"
		"121" : "1.2.3.4"
	]
]

[send "0430" from FMS as send_fms_rev_rsp
	import profile::send_fms_rsp
	[
		"37"  : \copyFrom("recv_fms_reversal", "37")\
		"41"  : \copyFrom("recv_fms_reversal", "41")\
		"42"  : \copyFrom("recv_fms_reversal", "42")\
		"39"  : "00"
		"108"  : \copyFrom("recv_fms_reversal", "108")\
	]
]

[disconnect "FMS"]