@echo off

@echo Clean-up old test output.
RMDIR /S /Q output
MKDIR output

@echo.
@echo Stop the Services.
CALL stop_services.cmd | gtee output/test_output.txt

@echo.
@echo Build the Test Class/es.
CALL build.cmd | gtee output/test_output.txt

@echo.
@echo Load the Database Configuration.
CALL load_config.cmd | gtee -a output/test_output.txt

@echo.
@echo Start the Services.
CALL start_services.cmd | gtee -a output/test_output.txt

@echo.
@echo Run the Test Suite.
CALL run_tests.cmd | gtee -a output/test_output.txt

@echo.
@echo Copy the trace files.
XCOPY "%POSTILIONDIR%\Trace\PassThroughInterface\AuthSink\*.html" ".\output\" /y /s | gtee -a output/test_output.txt
XCOPY "%POSTILIONDIR%\Trace\Transaction Manager\Transaction Manager\*.html" ".\output\" /y /s | gtee -a output/test_output.txt