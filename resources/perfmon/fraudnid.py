config["PerfMon"] = \
{
    "Task"                : "FraudNID",
    "Package"             : "postilion.realtime.fraudnid",
    "PerformanceCounters" : \
    [
        ("FRAUD_NID_DIAG",
        {
            "DetailLevel"       :   "NOVICE",
            "DefaultCounter"    :   "MSGS_SENT_PER_SEC",
            "AllowInstances"    :   "true",
            "Counters"          :   \
            [
                ("MSGS_SENT_PER_SEC",
                {
                    "DefaultScale"  :   "NONE",
                    "DetailLevel"   :   "NOVICE",
                    "Type"          :   "PERF_COUNTER_COUNTER"
                }),
                ("MSGS_RECEIVED_PER_SEC",
                {
                    "DefaultScale"  :   "NONE",
                    "DetailLevel"   :   "NOVICE",
                    "Type"          :   "PERF_COUNTER_COUNTER"
                }),
                ("TIMEOUTS_PER_SEC",
                {
                    "DefaultScale"  :   "NONE",
                    "DetailLevel"   :   "NOVICE",
                    "Type"          :   "PERF_COUNTER_COUNTER"
                }),
                ("STORE_AND_FORWARD_QUEUE_DEPTH",
                {
                    "DefaultScale"  :   "NONE",
                    "DetailLevel"   :   "NOVICE",
                    "Type"          :   "PERF_COUNTER_RAWCOUNT"
                }),
                 ("AVERAGE_RSP_TIME",
                {
                    "DefaultScale"  :   "NONE",
                    "DetailLevel"   :   "NOVICE",
                    "Type"          :   
                    (
                        "PERF_SAMPLE_FRACTION",
                        "PERF_AVERAGE_BASE"
                    )
                }),
                ("PERCENT_APPROVED_TRANS",
                {
                    "DefaultScale"  :   "NONE",
                    "DetailLevel"   :   "NOVICE",
                    "Type"          :   
                    (
                        "PERF_SAMPLE_FRACTION",
                        "PERF_AVERAGE_BASE"
                    )
                }), 
                ("PERCENT_DECLINED_TRANS",
                {
                    "DefaultScale"  :   "NONE",
                    "DetailLevel"   :   "NOVICE",
                    "Type"          :   
                    (
                        "PERF_SAMPLE_FRACTION",
                        "PERF_AVERAGE_BASE"
                    )
                }),
            ]
        }),
    ],
    "PerformanceText" : \
    [
        ("ENGLISH",
        {
            "FRAUD_NID_DIAG" : 
            (
                "Postilion:FraudNID Internal Diagnostics",
                "Counters tracking internal diagnostic values for use in monitoring."
            ),
            "MSGS_SENT_PER_SEC" : 
            (
                "Requests per second.",
                "The number of messages sent to the fraud system, per second."
            ),
            "MSGS_RECEIVED_PER_SEC" : 
            (
                "Responses per second.",
                "The number of responses received from the fraud system, per second."
            ),
            "TIMEOUTS_PER_SEC" : 
            (
                "Message timeouts per second.",
                "The number of timeouts per second."
            ),
            "STORE_AND_FORWARD_QUEUE_DEPTH" : 
            (
                "Store & Forward queue depth.",
                "Store-and-forward depth."
            ),
            "AVERAGE_RSP_TIME" : 
            (
                "Average message response time.",
                "Average response time of transactions handled by the fraud system."
            ),
            "AVERAGE_RSP_TIME_BASE" : 
            (
                "Average message response time base.",
                "Average response time of transactions handled by the fraud system."
            ),
            "PERCENT_APPROVED_TRANS" : 
            (
                "Percentage approved transactions.",
                "Percentage of approved transactions."
            ),
            "PERCENT_APPROVED_TRANS_BASE" : 
            (
                "Percentage approved transactions base.",
                "Percentage of approved transactions."
            ),
            "PERCENT_DECLINED_TRANS" : 
            (
                "Percentage decline transactions.",
                "Percentage of declined transactions."
            ),
            "PERCENT_DECLINED_TRANS_BASE" : 
            (
                "Percentage decline transactions base.",
                "Percentage of declined transactions."
            ),
        }),
    ],
}