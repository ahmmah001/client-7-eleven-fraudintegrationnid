Fraud Integration NID
=====================

The Fraud Integration Node Integration Driver is a NID designed to intercept 
transactions from Transaction Manager destined for a remote entity. Upon 
receipt of a qualifying transaction, a fraud request message will be 
generated and sent to the Fraud Management System, directly from within the 
NID, for processing. If the fraud check passes, the original transaction 
will be forwarded to the sink, if not, the NID will decline the original 
transaction and return it to Transaction Manager.

This NID is also responsible for managing reversal messages to the Fraud 
Management System. Reversals will be generated and sent if the sink entity 
declines a transaction for which a fraud check was performed or if the 
source entity reverses a previous transaction. These transactions are 
served by a Store & Forward Queue integrated into the NID.

The implementation includes a "versioned" class approach to manage message 
construction/processing/mapping. Each change/update/fix will be placed in a 
new Mapper_v# class, allowing the mapper class to be loaded using the 
'RELOADMAPPER' command without restarting the service associated to the NID.

Please read the User Guide (ug_fraudnid.chm) before installing the 
application. The user guide is displayed by launching the help file from an 
appropriate application such as the Windows Explorer.

Please read the Release Notes (rn_fraudnid.chm) before installing the
application. The release notes are displayed by launching the file from an 
appropriate application such as the Windows Explorer.

The application is installed by running the setup.exe program.