/*----------------------------------------------------------------------------*\
 *                                                                            *
 * Copyright (c) 2018 by Montant Limited. All rights reserved.                *
 *                                                                            *
 * This software is the confidential and proprietary property of              *
 * Montant Limited and may not be disclosed, copied or distributed            *
 * in any form without the express written permission of Montant Limited.     *
 *                                                                            *
\*----------------------------------------------------------------------------*/
package postilion.realtime.fraudnid.helpers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;

import postilion.realtime.fraudnid.dao.StoreAndForwardQueueDao;
import postilion.realtime.fraudnid.eventrecorder.ILogger;
import postilion.realtime.fraudnid.msg.Iso8583FMS;
import postilion.realtime.fraudnid.safq.SAFQTran;
import postilion.realtime.sdk.eventrecorder.AEventRecord;
import postilion.realtime.sdk.ipc.IEndpoint;
import postilion.realtime.sdk.jdbc.JdbcManager;
import postilion.realtime.sdk.message.bitmap.Iso8583Post.MsgType;
import postilion.realtime.sdk.util.DateTime;
import postilion.realtime.sdk.util.convert.Pack;

public class ReversalGenerator
{
	private static postilion.realtime.sdk.ipc.SecurityManager	sec;
	private static ILogger													logger	= new ILogger()
																								{
																									@Override
																									public void recordEvent(AEventRecord event)
																									{
																									}

																									@Override
																									public void logResponse(Iso8583FMS msg,
																										IEndpoint endpoint)
																										throws Exception
																									{
																									}

																									@Override
																									public void logRequest(Iso8583FMS msg,
																										IEndpoint endpoint)
																										throws Exception
																									{
																									}

																									@Override
																									public boolean isTraceOn()
																									{
																										return false;
																									}

																									@Override
																									public void enableMsgTracing()
																									{
																									}

																									@Override
																									public void disableMsgTracing()
																									{
																									}

																									@Override
																									public void debug(String text,
																										Object details)
																									{
																									}

																									@Override
																									public void debug(Object text)
																									{
																									}
																								};

	public static void cleanAll() throws SQLException
	{
		Connection cn = null;
		PreparedStatement stmt = null;
		try
		{
			final String sql = "DELETE FROM fraudnid_persistent_safq";
			cn = JdbcManager.getDefaultConnection();
			stmt = cn.prepareStatement(sql);
			stmt.executeUpdate();
			JdbcManager.commit(cn, stmt);
		}
		finally
		{
			JdbcManager.cleanup(cn, stmt);
		}
	}

	/**
	 * Create, in the saf table, reversals
	 * 
	 * @param count
	 *           Number of reversals to create
	 * @param sinkNode
	 *           Sink node against which to create them
	 * @param seed
	 *           starting value for the key
	 * @throws Exception
	 */
	public static void generateReversals(int count, String sinkNode, int seed) throws Exception
	{
		Iso8583FMS reversal = new Iso8583FMS();
		reversal.putMsgType(MsgType._0420_ACQUIRER_REV_ADV);
		reversal.putField(4, "000000000100");
		reversal.putField(41, "12345678");
		reversal.putField(42, "123456789012345");
		reversal.putField(105, "CompanyName");
		reversal.putField(106, "1234567890");
		reversal.putField(107, "00");
		reversal.putField(108, sinkNode);
		reversal.putField(109, "Service");
		reversal.putField(110, "CardType");
		reversal.putField(111, "0");
		reversal.putField(112, "0");
		reversal.putField(114, "0");
		reversal.putField(115, "0");
		reversal.putField(116, new DateTime().get("yyyyMMdd HH:mm:ss.SSS"));
		reversal.putField(117, "001");
		reversal.putField(118, "123");
		reversal.putField(119, "321");
		reversal.putField(120, "0");
		reversal.putField(121, "1.2.3.4");
		for (int x = 0; x < count; x++)
		{
			String rrn = makeRrn(seed++);
			reversal.putField(37, rrn);
			insertReversal(rrn + sinkNode, sinkNode, reversal);
			// Need at least a millisecond gap between records to keep the order
			// This isn't a real-world limitation, but the order is important for
			// the tests. And yes, the 1ms specified below will be more than 1ms
			Thread.sleep(1);
		}
	}

	private static void insertReversal(String key, String sinkNode, Iso8583FMS reversal) throws Exception
	{
		SAFQTran record = new SAFQTran();
		record.setId(key);
		record.setMsgData(sec.encrypt(reversal.toMsg()));
		record.setMti("0420");
		record.setQueueTime(new Date());
		record.setRequestTime(null);
		record.setResponseCode(null);
		record.setResponseTime(null);
		record.setRetriesRemaining(3);
		record.setSent(false);
		record.setSinkNodeName(sinkNode);
		StoreAndForwardQueueDao.insert(record, logger);
	}

	public static String makeRrn(int ct)
	{
		return Pack.resize(String.valueOf(ct), 12, '0', false);
	}

	public static void main(String[] args) throws Exception
	{
		if (args.length > 0)
		{
			if (args[0].equalsIgnoreCase("clean"))
			{
				cleanAll();
			}
			else if (args[0].equalsIgnoreCase("generate"))
			{
				if (args.length != 4)
				{
					usageError();
				}
				else
				{
					try
					{
						sec = new postilion.realtime.sdk.ipc.SecurityManager();
						generateReversals(Integer.parseInt(args[1]), args[2], Integer.parseInt(args[3]));
					}
					catch(NumberFormatException e)
					{
						usageError();
					}
				}
			}
			else
			{
				usageError();
			}
		}
		else
		{
			usageError();
		}
	}

	private static void usageError() throws Exception
	{
		throw new Exception("Invalid command line - try \"clean\" or \"generate <count> <sinknode name> <rrn seed>");
	}
}
