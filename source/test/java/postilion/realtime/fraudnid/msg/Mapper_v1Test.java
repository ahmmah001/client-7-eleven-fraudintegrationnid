/*----------------------------------------------------------------------------*\
 *                                                                            *
 * Copyright (c) 2018 by Montant Limited. All rights reserved.                *
 *                                                                            *
 * This software is the confidential and proprietary property of              *
 * Montant Limited and may not be disclosed, copied or distributed            *
 * in any form without the express written permission of Montant Limited.     *
 *                                                                            *
\*----------------------------------------------------------------------------*/

package postilion.realtime.fraudnid.msg;

import static org.junit.Assert.*;

import org.junit.Test;

import postilion.realtime.fraudnid.msg.Mapper_v1.StructDataTags;
import postilion.realtime.sdk.message.bitmap.Iso8583Post;
import postilion.realtime.sdk.message.bitmap.Iso8583Rev93.Bit;
import postilion.realtime.sdk.message.bitmap.StructuredData;

public class Mapper_v1Test
{
	Mapper_v1 m = new Mapper_v1();
	
	@Test
	public void testConstructStoreNumberToFMS_fromSdWithSpaces() throws Exception
	{
		Iso8583Post msgFromTm = new Iso8583Post();
		Iso8583FMS msgToFms = new Iso8583FMS();
		StructuredData sd = new StructuredData();
		sd.put(StructDataTags.CARD_ACC_ID_FIELD_42, "STORENUMBER12  ");
		msgFromTm.putStructuredData(sd);
		m.constructStoreNumberToFMS(msgFromTm, msgToFms);
		assertEquals("00STORENUMBER12", msgToFms.getField(Bit._042_CARD_ACCEPTOR_ID_CODE));
	}
	@Test
	public void testConstructStoreNumberToFMS_fromSdGoldilocks() throws Exception
	{
		Iso8583Post msgFromTm = new Iso8583Post();
		Iso8583FMS msgToFms = new Iso8583FMS();
		StructuredData sd = new StructuredData();
		sd.put(StructDataTags.CARD_ACC_ID_FIELD_42, "STORENUMBER1234");
		msgFromTm.putStructuredData(sd);
		m.constructStoreNumberToFMS(msgFromTm, msgToFms);
		assertEquals("STORENUMBER1234", msgToFms.getField(Bit._042_CARD_ACCEPTOR_ID_CODE));
	}
	@Test
	public void testConstructStoreNumberToFMS_fromSdWithSpacesPrefix() throws Exception
	{
		Iso8583Post msgFromTm = new Iso8583Post();
		Iso8583FMS msgToFms = new Iso8583FMS();
		StructuredData sd = new StructuredData();
		sd.put(StructDataTags.CARD_ACC_ID_FIELD_42, "  STORENUMBER12");
		msgFromTm.putStructuredData(sd);
		m.constructStoreNumberToFMS(msgFromTm, msgToFms);
		assertEquals("00STORENUMBER12", msgToFms.getField(Bit._042_CARD_ACCEPTOR_ID_CODE));
	}
	@Test
	public void testConstructStoreNumberToFMS_fromP42WithSpaces() throws Exception
	{
		Iso8583Post msgFromTm = new Iso8583Post();
		Iso8583FMS msgToFms = new Iso8583FMS();
		StructuredData sd = new StructuredData();
		msgFromTm.putStructuredData(sd);
		msgFromTm.putField(Bit._042_CARD_ACCEPTOR_ID_CODE, "STORENUMBER12  ");
		m.constructStoreNumberToFMS(msgFromTm, msgToFms);
		assertEquals("00STORENUMBER12", msgToFms.getField(Bit._042_CARD_ACCEPTOR_ID_CODE));
	}
	@Test
	public void testConstructStoreNumberToFMS_fromP42WithSpacesPrefix() throws Exception
	{
		Iso8583Post msgFromTm = new Iso8583Post();
		Iso8583FMS msgToFms = new Iso8583FMS();
		StructuredData sd = new StructuredData();
		msgFromTm.putStructuredData(sd);
		msgFromTm.putField(Bit._042_CARD_ACCEPTOR_ID_CODE, "  STORENUMBER12");
		m.constructStoreNumberToFMS(msgFromTm, msgToFms);
		assertEquals("00STORENUMBER12", msgToFms.getField(Bit._042_CARD_ACCEPTOR_ID_CODE));
	}
	@Test
	public void testConstructStoreNumberToFMS_fromP42Goldilocks() throws Exception
	{
		Iso8583Post msgFromTm = new Iso8583Post();
		Iso8583FMS msgToFms = new Iso8583FMS();
		StructuredData sd = new StructuredData();
		msgFromTm.putStructuredData(sd);
		msgFromTm.putField(Bit._042_CARD_ACCEPTOR_ID_CODE, "STORENUMBER1234");
		m.constructStoreNumberToFMS(msgFromTm, msgToFms);
		assertEquals("STORENUMBER1234", msgToFms.getField(Bit._042_CARD_ACCEPTOR_ID_CODE));
	}


	@Test
	public void testConstructPosEcrIdToFMS_fromSdWithSpaces() throws Exception
	{
		Iso8583Post msgFromTm = new Iso8583Post();
		Iso8583FMS msgToFms = new Iso8583FMS();
		StructuredData sd = new StructuredData();
		sd.put(StructDataTags.BOX_NR_FIELD_41, "TERMIN  ");
		msgFromTm.putStructuredData(sd);
		m.constructPosEcrIdToFMS(msgFromTm, msgToFms);
		assertEquals("00TERMIN", msgToFms.getField(Bit._041_CARD_ACCEPTOR_TERM_ID));
	}
	@Test
	public void testConstructPosEcrIdToFMS_fromSdGoldilocks() throws Exception
	{
		Iso8583Post msgFromTm = new Iso8583Post();
		Iso8583FMS msgToFms = new Iso8583FMS();
		StructuredData sd = new StructuredData();
		sd.put(StructDataTags.BOX_NR_FIELD_41, "TERMINAL");
		msgFromTm.putStructuredData(sd);
		m.constructPosEcrIdToFMS(msgFromTm, msgToFms);
		assertEquals("TERMINAL", msgToFms.getField(Bit._041_CARD_ACCEPTOR_TERM_ID));
	}
	@Test
	public void testConstructPosEcrIdToFMS_fromSdWithSpacesPrefix() throws Exception
	{
		Iso8583Post msgFromTm = new Iso8583Post();
		Iso8583FMS msgToFms = new Iso8583FMS();
		StructuredData sd = new StructuredData();
		sd.put(StructDataTags.BOX_NR_FIELD_41, "  TERMIN");
		msgFromTm.putStructuredData(sd);
		m.constructPosEcrIdToFMS(msgFromTm, msgToFms);
		assertEquals("00TERMIN", msgToFms.getField(Bit._041_CARD_ACCEPTOR_TERM_ID));
	}
	@Test
	public void testConstructPosEcrIdToFMS_fromP42WithSpaces() throws Exception
	{
		Iso8583Post msgFromTm = new Iso8583Post();
		Iso8583FMS msgToFms = new Iso8583FMS();
		StructuredData sd = new StructuredData();
		msgFromTm.putStructuredData(sd);
		msgFromTm.putField(Bit._041_CARD_ACCEPTOR_TERM_ID, "TERMIN  ");
		m.constructPosEcrIdToFMS(msgFromTm, msgToFms);
		assertEquals("00TERMIN", msgToFms.getField(Bit._041_CARD_ACCEPTOR_TERM_ID));
	}
	@Test
	public void testConstructPosEcrIdToFMS_fromP42WithSpacesPrefix() throws Exception
	{
		Iso8583Post msgFromTm = new Iso8583Post();
		Iso8583FMS msgToFms = new Iso8583FMS();
		StructuredData sd = new StructuredData();
		msgFromTm.putStructuredData(sd);
		msgFromTm.putField(Bit._041_CARD_ACCEPTOR_TERM_ID, "  TERMIN");
		m.constructPosEcrIdToFMS(msgFromTm, msgToFms);
		assertEquals("00TERMIN", msgToFms.getField(Bit._041_CARD_ACCEPTOR_TERM_ID));
	}
	@Test
	public void testConstructPosEcrIdToFMS_fromP42Goldilocks() throws Exception
	{
		Iso8583Post msgFromTm = new Iso8583Post();
		Iso8583FMS msgToFms = new Iso8583FMS();
		StructuredData sd = new StructuredData();
		msgFromTm.putStructuredData(sd);
		msgFromTm.putField(Bit._041_CARD_ACCEPTOR_TERM_ID, "TERMINAL");
		m.constructPosEcrIdToFMS(msgFromTm, msgToFms);
		assertEquals("TERMINAL", msgToFms.getField(Bit._041_CARD_ACCEPTOR_TERM_ID));
	}
}

