package postilion.realtime.passthroughinterface;

import postilion.realtime.sdk.message.IMessage;
import postilion.realtime.sdk.message.bitmap.Iso8583Post;
import postilion.realtime.sdk.node.*;
/**
 * Custom version of PassthroughInterface to insert fake delay in processing of
 * messages upon receipt of a command delays is valid for the next transaction
 * only
 */
public class PassThroughInterface
	extends
	AInterchangeDriver8583
{
	private long delayOut = 0;

	public PassThroughInterface()
	{
	}

	public IMessage newMsg(byte data[]) throws Exception
	{
		Iso8583Post msg = new Iso8583Post();
		msg.fromMsg(data);
		return msg;
	}

	public Action processMsgFromInterchange(AInterchangeDriverEnvironment interchange, IMessage msg) throws Exception
	{
		return new Action((Iso8583Post)msg, null, null, null);
	}

	public Action processMsgFromTranmgr(AInterchangeDriverEnvironment interchange, Iso8583Post msg_from_tran_mgr)
		throws Exception
	{
		// For messages to the sink, delay
		delayOutbound();
		return new Action(null, msg_from_tran_mgr, null, null);
	}

	public Action processMsgFromRemote(AInterchangeDriverEnvironment interchange, IMessage msg) throws Exception
	{
		return new Action((Iso8583Post)msg, null, null, null);
	}

	/**
	 * Handle {@code SET <interchange> DELAYOUT  [OFF | <milliseconds]}
	 */
	@Override
	public Action processSetCommand(AInterchangeDriverEnvironment interchange, String param, String value)
		throws XNodeParameterUnknown, XNodeParameterValueInvalid, Exception
	{
		if ("DELAYOUT".equalsIgnoreCase(param))
		{
			if ("OFF".equalsIgnoreCase(value))
			{
				delayOut = 0;
			}
			else
			{
				delayOut = Long.valueOf(value);
			}
			return null;
		}
		else
		{
			return super.processSetCommand(interchange, param, value);
		}
	}

	/**
	 * Delay processing (suspends the thread) if required, and toggle delay off
	 */
	private void delayOutbound()
	{
		if (delayOut > 0)
		{
			try
			{
				Thread.sleep(delayOut);
			}
			catch(InterruptedException e)
			{
				// Do nothing
			}
			// Reset for the next transaction
			delayOut = 0;
		}
	}
}
