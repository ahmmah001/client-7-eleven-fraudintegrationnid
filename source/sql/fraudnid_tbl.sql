--------------------------------------------------------------------------------
PRINT ''
PRINT 'Creating Table: fraudnid_swh_cat_fms_config'
PRINT ''
--------------------------------------------------------------------------------
GO

CREATE TABLE fraudnid_swh_cat_fms_config (
	id VARCHAR(255), 
	desc_tran VARCHAR(255), 
	id_fms VARCHAR(255), 
	tran_type VARCHAR(255), 
	name_interface VARCHAR(255), 
	service VARCHAR(255), 
	card_type VARCHAR(255), 
	field_account_ref_num VARCHAR(255), 
	field_amount VARCHAR(255),
	field_optional_1 VARCHAR(50) NULL,
	field_optional_2 VARCHAR(50) NULL,
	field_optional_3 VARCHAR(50) NULL
)
GO

GRANT SELECT, INSERT, DELETE, UPDATE ON fraudnid_swh_cat_fms_config TO postapp
GRANT SELECT, INSERT, DELETE, UPDATE ON fraudnid_swh_cat_fms_config TO postcfg
GRANT SELECT ON fraudnid_swh_cat_fms_config TO postmon
GO

--------------------------------------------------------------------------------
PRINT ''
PRINT 'Creating Table: fraudnid_persistent_safq'
PRINT ''
--------------------------------------------------------------------------------
GO

CREATE TABLE fraudnid_persistent_safq (
	id VARCHAR(255) NOT NULL, 
	mti VARCHAR(4) NULL,
	sink_node_name VARCHAR(50) NOT NULL, 
	queue_time DATETIME NOT NULL, 
	request_time DATETIME NULL,
	response_time DATETIME NULL,
	response_code CHAR(2) NULL,
	retries_remaining INT NOT NULL,
	sent BIT NOT NULL,
	msg VARCHAR(MAX) NOT NULL, 
	msgout VARCHAR(MAX),
	msgin VARCHAR(MAX)
)
GO

ALTER TABLE fraudnid_persistent_safq ADD CONSTRAINT pk_fraudnid_persistent_safq PRIMARY KEY (id, sink_node_name)
GO

CREATE NONCLUSTERED INDEX ix1_fraudnid_persistent_safq ON fraudnid_persistent_safq (queue_time, sent, sink_node_name) 
GO

CREATE NONCLUSTERED INDEX ix2_fraudnid_persistent_safq ON fraudnid_persistent_safq (queue_time, mti, sink_node_name)
GO

GRANT SELECT, INSERT, DELETE, UPDATE ON fraudnid_persistent_safq TO postapp
GRANT SELECT, INSERT, DELETE, UPDATE ON fraudnid_persistent_safq TO postcfg
GRANT SELECT ON fraudnid_persistent_safq TO postmon
GO

--------------------------------------------------------------------------------
PRINT ''
PRINT 'Creating Table: fraudnid_tran_filter_config'
PRINT ''
--------------------------------------------------------------------------------
GO

CREATE TABLE fraudnid_tran_filter_config (
	tran_type VARCHAR(2) NOT NULL,
	ext_tran_type VARCHAR(4), 
	enable BIT NOT NULL, 
	interface_index INT NOT NULL 
)
GO

GRANT SELECT, INSERT, DELETE, UPDATE ON fraudnid_tran_filter_config TO postapp
GRANT SELECT, INSERT, DELETE, UPDATE ON fraudnid_tran_filter_config TO postcfg
GRANT SELECT ON fraudnid_tran_filter_config TO postmon
GO