/*----------------------------------------------------------------------------*
 *                                                 
 * Copyright (c) 2018 by Montant Limited. All rights reserved.
 *                                                                             
 * This software is the confidential and proprietary property of 
 * Montant Limited and may not be disclosed, copied or distributed 
 * in any form without the express written permission of Montant Limited.
 *----------------------------------------------------------------------------*/

package postilion.realtime.fraudnid.perfmon;

import postilion.realtime.sdk.util.perfmon.ICounterFractional;
import postilion.realtime.sdk.util.perfmon.ICounterNumeric;
import postilion.realtime.sdk.util.perfmon.PerfObject;

public class PerfMonDiagInstance extends PerfMonInstance
{
	private PerfMonDiagInstance totalInstance = null;
	protected ICounterNumeric msgsSentPerSec = null;
	protected ICounterNumeric msgsRecPerSec = null;
	protected ICounterNumeric timeoutsPerSec = null;
	protected ICounterNumeric safqDepth = null;
	protected ICounterFractional avgRspTime = null;
	protected ICounterFractional percentApproved = null;
	protected ICounterFractional percentDeclined = null;
		
	/*-------------------------------------------------------------------------*/
	/**
	 * Constructor for creating the _Total instance.
	 */
	PerfMonDiagInstance(PerfObject perfDiagObject, String name)
	{
		this(perfDiagObject, null, name);
	}
	/*-------------------------------------------------------------------------*/
	/**
	 * Constructor for creating the instances.
	 */
	PerfMonDiagInstance(
			PerfObject perfDiagObject, 
			PerfMonDiagInstance totalInstance, 
			String name)
	{
		super(perfDiagObject, name);
		
		if (PerfMonManager.useDummyInstances())
		{
			return;
		}
		
		this.totalInstance = totalInstance;
	
		msgsSentPerSec = 
			(ICounterNumeric) getNumericCounter(
					PerfDefns.MSGS_SENT_PER_SEC);
		msgsRecPerSec = 
			(ICounterNumeric) getNumericCounter(
					PerfDefns.MSGS_RECEIVED_PER_SEC);
		timeoutsPerSec = 
			(ICounterNumeric) getNumericCounter(
					PerfDefns.TIMEOUTS_PER_SEC);
		safqDepth = 
			(ICounterNumeric) getNumericCounter(
					PerfDefns.STORE_AND_FORWARD_QUEUE_DEPTH);
		avgRspTime = 
			(ICounterFractional) getFractionalCounter(
					PerfDefns.AVERAGE_RSP_TIME);
		percentApproved = 
			(ICounterFractional) getFractionalCounter(
					PerfDefns.PERCENT_APPROVED_TRANS);
		percentDeclined = 
			(ICounterFractional) getFractionalCounter(
					PerfDefns.PERCENT_DECLINED_TRANS);
		
		super.open();
	}
	
	/*-------------------------------------------------------------------------*/
	public void initializeCounterValues()
	{
		// Note: Not all counters are initialized here. Counters
		// are set to 0 at instantiation time. Some counters, typically
		// rate indicators or counters sampled periodically can be left alone. 
		// Counters that represent things like sizes of a container 
		// which is *emptied* during a RESYNC should be reset.
		if (isClosed())
		{
			return;
		}
	}
		
	/*------------------------------------------------------------------------*/
	public synchronized void incrementMessagesSentPerSecond()
	{	
		if (isClosed())
		{
			return;
		}
		
		if (totalInstance != null)
		{
			totalInstance.incrementMessagesSentPerSecond();
		}
		
		this.msgsSentPerSec.increment(1);
	}
	
	/*------------------------------------------------------------------------*/
	public synchronized void incrementMessagesReceivedPerSecond()
	{
		if (isClosed())
		{
			return;
		}
		
		if (totalInstance != null)
		{
			totalInstance.incrementMessagesReceivedPerSecond();
		}
		
		this.msgsRecPerSec.increment(1);
	}
	
	/*------------------------------------------------------------------------*/
	public synchronized void incrementMsgTimeoutsPerSecond()
	{
		if (isClosed())
		{
			return;
		}
		
		if (totalInstance != null)
		{
			totalInstance.incrementMsgTimeoutsPerSecond();
		}
		
		this.timeoutsPerSec.increment(1);
	}
	
	/*------------------------------------------------------------------------*/
	public synchronized void setStoreAndForwardQueueDepth(long value)
	{
		if (isClosed())
		{
			return;
		}
		
		if (totalInstance != null)
		{
			totalInstance.setStoreAndForwardQueueDepth(value);
		}
		
		this.safqDepth.setValue(value);
	}
	
	/*------------------------------------------------------------------------*/
	public synchronized void incrementAverageResponseTime(long time)
	{
		if (isClosed())
		{
			return;
		}
		
		// Average time per operation counters are tracked as percentage
		// counters (don't ask). The denominator increment of 100
		// negates the 100 multiplier.
		
		if (totalInstance != null)
		{
			totalInstance.incrementAverageResponseTime(time);
		}
		
		this.avgRspTime.increment(time, 100);
	}
	
	/*------------------------------------------------------------------------*/
	public synchronized void incrementPercentageApprovedAndDeclined(
			long approvedNumberDelta, 
			long approvedBaseDelta, 
			long declinedNumberDelta, 
			long declinedBaseDelta)
	{
		if (isClosed())
		{
			return;
		}
		
		// Related ratio'd counters (for example a pair of counters tracking
		// % approved vs. % declined transactions) must be updated together
		// so PerfMon can track the ratio correctly.
		
		if (totalInstance != null)
		{
			totalInstance.incrementPercentageApprovedAndDeclined(
					approvedNumberDelta, 
					approvedBaseDelta, 
					declinedNumberDelta, 
					declinedBaseDelta);
		}
		
		this.percentApproved.increment(approvedNumberDelta, approvedBaseDelta);
		this.percentDeclined.increment(declinedNumberDelta, declinedBaseDelta);
	}
	
	/*------------------------------------------------------------------------*/
}