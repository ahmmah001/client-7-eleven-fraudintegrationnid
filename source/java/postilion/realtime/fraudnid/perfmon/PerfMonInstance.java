/*----------------------------------------------------------------------------*
 *                                                 
 * Copyright (c) 2018 by Montant Limited. All rights reserved.
 *                                                                             
 * This software is the confidential and proprietary property of 
 * Montant Limited and may not be disclosed, copied or distributed 
 * in any form without the express written permission of Montant Limited.
 *----------------------------------------------------------------------------*/

package postilion.realtime.fraudnid.perfmon;

import postilion.realtime.sdk.eventrecorder.EventRecorder;
import postilion.realtime.sdk.util.perfmon.CounterType;
import postilion.realtime.sdk.util.perfmon.ICounterFractional;
import postilion.realtime.sdk.util.perfmon.ICounterNumeric;
import postilion.realtime.sdk.util.perfmon.ObjectID;
import postilion.realtime.sdk.util.perfmon.PerfCounter;
import postilion.realtime.sdk.util.perfmon.PerfInstance;
import postilion.realtime.sdk.util.perfmon.PerfObject;

/**
 * This light-weight wrapper class encapsulates an instance of
 * the postilion.realtime.sdk.util.perfmon.PerfInstance class to ensure things
 * like dummy counters are available if the PerfMon subsystem
 * could not be initialized.
 */
public abstract class PerfMonInstance
{
	private static final NullCounter NULL_COUNTER = new NullCounter();
	private PerfInstance			instance;
	private boolean				closed = true;
	
	/*------------------------------------------------------------------------*/
	/**
	 * Constructor for creating a _Total instance.
	 */
	protected PerfMonInstance(PerfObject perfObject)
	{
		if (PerfMonManager.useDummyInstances())
		{
			return;
		}

		try
		{
			instance = perfObject.getTotalInstance();
		}
		catch(Throwable e)
		{
			EventRecorder.recordEvent(e);
		}
	}

	/*------------------------------------------------------------------------*/
	/**
	 * Constructor for creating a specific instance.
	 */
	protected PerfMonInstance(PerfObject perfObject, String name)
	{
		if (PerfMonManager.useDummyInstances())
		{
			return;
		}

		try
		{
			instance = perfObject.createInstance(name);
		}
		catch(Throwable e)
		{
			EventRecorder.recordEvent(e);
		}	
	}
	
	/*------------------------------------------------------------------------*/
	protected synchronized void open()
	{
		this.closed = false;
	}
		
	/*------------------------------------------------------------------------*/
	public synchronized void close()
	{
		if (this.closed)
		{
			return;
		}
		
		if (instance != null)
		{
			instance.close();
			instance = null;
		}
		
		this.closed = true;
	}
	
	/*------------------------------------------------------------------------*/
	protected synchronized boolean isClosed()
	{
		return closed;
	}

	/*------------------------------------------------------------------------*/
	protected synchronized ICounterNumeric getNumericCounter(ObjectID id)
	{
		if (instance == null)
		{
			return NULL_COUNTER;
		}
		
		return (ICounterNumeric)instance.getCounter(id);
	}
	
	/*------------------------------------------------------------------------*/
	protected synchronized ICounterFractional getFractionalCounter(ObjectID id)
	{
		if (instance == null)
		{
			return NULL_COUNTER;
		}
		
		return (ICounterFractional)instance.getCounter(id);
	}
	
	/*------------------------------------------------------------------------*/
	public abstract void initializeCounterValues();
	
	/*------------------------------------------------------------------------*/
	/**
	 * This class is a stub class for dealing with the case
	 * where a counter can not be created because an instance
	 * could not be created for some reason.
	 */
	private static class NullCounter
	extends PerfCounter
	implements ICounterFractional, ICounterNumeric
	{
		public NullCounter()
		{
			super();
		}
		
		public void increment(long delta)
		{
		}
		
		public void decrement(long delta)
		{
		}
		
		public void setValue(long delta)
		{
		}
		
		public void increment(long delta, long base)
		{
		}
		
		public void decrement(long delta, long base)
		{
		}
		
		public void setValue(long delta, long base)
		{
		}
		
		public long getValue()
		{
			return 0;
		}

		public long getNumeratorValue()
		{
			return 0;
		}

		public long getDenominatorValue()
		{
			return 0;
		}
		
		public void refresh()
		{
		}
		
		@SuppressWarnings("unused")
		public CounterType getCounterType()
		{
			return CounterType.PERF_COUNTER_RAWCOUNT;
		}
		
		public CounterType getDenominatorCounterType()
		{
			return CounterType.PERF_COUNTER_RAWCOUNT;
		}
	}
	
	/*------------------------------------------------------------------------*/
}