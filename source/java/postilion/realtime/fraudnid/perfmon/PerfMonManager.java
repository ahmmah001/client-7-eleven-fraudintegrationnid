/*----------------------------------------------------------------------------*
 *                                                 
 * Copyright (c) 2018 by Montant Limited. All rights reserved.
 *                                                                             
 * This software is the confidential and proprietary property of 
 * Montant Limited and may not be disclosed, copied or distributed 
 * in any form without the express written permission of Montant Limited.
 *----------------------------------------------------------------------------*/

package postilion.realtime.fraudnid.perfmon;

import java.util.Enumeration;
import java.util.Hashtable;
import postilion.realtime.sdk.env.App;
import postilion.realtime.sdk.eventrecorder.EventRecorder;
import postilion.realtime.sdk.util.perfmon.PerfInstance;
import postilion.realtime.sdk.util.perfmon.PerfMapManager;
import postilion.realtime.sdk.util.perfmon.PerfObject;

public class PerfMonManager
{
	private static PerfObject diagPerfObject;
	
	// _Total instances. These are always present but are handed
	// to the relevant group of instances which are then responsible
	// for updating the totals when their individual values are
	// updated.
	private static PerfMonDiagInstance diagTotalInstance;

	// Table for tracking existing PerfMon instances
	private static Hashtable<String, PerfMonInstance> instances = 
			new Hashtable<String, PerfMonInstance>();

	// Flag to indicate whether static initialization succeeded
	private static boolean initError;
	
	/*------------------------------------------------------------------------*/
	public static synchronized void initialize(String sinkNodeName)
	{
		registerPerfMonDiagInstance(sinkNodeName);
		
		Enumeration<PerfMonInstance> e = instances.elements();
		while( e.hasMoreElements() )
		{
			PerfMonInstance instance = e.nextElement();
			instance.initializeCounterValues();
		}
		
		PerfMapManager.initCompleted();
	}
	
	/*------------------------------------------------------------------------*/
	public static boolean useDummyInstances()
	{
		return initError;
	}

	/*------------------------------------------------------------------------*/
	public static synchronized void close()
	{
		Enumeration<PerfMonInstance> e = instances.elements();
		while( e.hasMoreElements() )
		{
			e.nextElement().close();
		}
		
		instances.clear();
	}	

	/*------------------------------------------------------------------------*/
	private static PerfMonDiagInstance registerPerfMonDiagInstance(
			String sinkNodeName)
	{
		closeExistingInstance(sinkNodeName);

		PerfMonDiagInstance instance = 
			new PerfMonDiagInstance(
					diagPerfObject, 
					diagTotalInstance, 
					sinkNodeName);
		
		putInstance(sinkNodeName, instance);
		
		return instance;
	}
	
	/*------------------------------------------------------------------------*/
	private static void putInstance(String sinkNodeName, PerfMonInstance instance)
	{
		instances.put(sinkNodeName, instance);
	}
	
	/*------------------------------------------------------------------------*/
	@SuppressWarnings("unchecked")
	private static <T extends PerfMonInstance> T getInstance(String sinkNodeName)
	{
		return (T) instances.get(sinkNodeName);
	}
	
	/*------------------------------------------------------------------------*/
	private static synchronized void closeExistingInstance(String sinkNodeName)
	{
		PerfMonInstance instance = instances.remove(sinkNodeName);
		if (instance != null)
		{
			instance.close();
		}
	}
	
	/*------------------------------------------------------------------------*/
	public static void incrementMessagesSentPerSecond(String sinkNodeName)
	{	
		PerfMonDiagInstance perf = getInstance(sinkNodeName);
		perf.incrementMessagesSentPerSecond();
	}
	
	/*------------------------------------------------------------------------*/
	public static void incrementMessagesReceivedPerSecond(String sinkNodeName)
	{	
		PerfMonDiagInstance perf = getInstance(sinkNodeName);
		perf.incrementMessagesReceivedPerSecond();
	}
	
	/*------------------------------------------------------------------------*/
	public static void incrementMsgTimeoutsPerSecond(String sinkNodeName)
	{
		PerfMonDiagInstance perf = getInstance(sinkNodeName);
		perf.incrementMsgTimeoutsPerSecond();
	}
	
	/*------------------------------------------------------------------------*/
	
	public static void setStoreAndForwardQueueDepth(String sinkNodeName, long length)
	{
		PerfMonDiagInstance perf = getInstance(sinkNodeName);
		perf.setStoreAndForwardQueueDepth(length);
	}
	
	/*------------------------------------------------------------------------*/
	public static void incrementAverageResponseTime(String sinkNodeName, long duration)
	{	
		PerfMonDiagInstance perf = getInstance(sinkNodeName);
		perf.incrementAverageResponseTime(duration);
	}

	/*------------------------------------------------------------------------*/
	public static void incrementPercentageApprovedAndDeclined(
			String sinkNodeName, 
			long approvedNumberDelta, 
			long approvedBaseDelta, 
			long declinedNumberDelta, 
			long declinedBaseDelta)
	{	
		
		PerfMonDiagInstance perf = getInstance(sinkNodeName);
		perf.incrementPercentageApprovedAndDeclined(
				approvedNumberDelta, 
				approvedBaseDelta, 
				declinedNumberDelta, 
				declinedBaseDelta);
	}
	
	/*------------------------------------------------------------------------*/
	static
	{
		try
		{
			// Register our performance counters.
			PerfDefns.register();
			
			// Initialize the PerfMon framework.
			PerfMapManager.initialize();

			diagPerfObject = PerfDefns.getObject(PerfDefns.FRAUD_NID_DIAG);
			initError = false;
		}
		catch( Throwable e )
		{
			initError = true;
			EventRecorder.recordEvent(e);
		}
		
		// 
		// This is done separately to ensure that we instantiate
		// our _Total instances even if an error occurs.
		//
		try
		{
			diagTotalInstance = 
					new PerfMonDiagInstance(
							diagPerfObject, 
							PerfInstance.getTotalInstanceName());

			instances.put(
					App.getName() 
						+ "_FraudNID_DIAG" 
						+ PerfInstance.getTotalInstanceName(), 
					diagTotalInstance);
			
			// Finally, assuming everything was correctly set up, install
			// a shutdown hook to clean up
			Runtime.getRuntime().addShutdownHook(
					new Thread()
					{
						public void run()
						{
							PerfMonManager.close();
						}
					}
				);
		}
		catch(Throwable e)
		{
			initError = true;
			EventRecorder.recordEvent(e);
		}	
	}
	
	/*------------------------------------------------------------------------*/
}