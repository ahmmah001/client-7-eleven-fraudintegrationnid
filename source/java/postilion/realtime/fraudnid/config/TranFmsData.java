/*----------------------------------------------------------------------------*
 *                                                 
 * Copyright (c) 2018 by Montant Limited. All rights reserved.
 *                                                                             
 * This software is the confidential and proprietary property of 
 * Montant Limited and may not be disclosed, copied or distributed 
 * in any form without the express written permission of Montant Limited.
 *----------------------------------------------------------------------------*/

package postilion.realtime.fraudnid.config;

import java.io.Serializable;
import java.util.Hashtable;
import postilion.realtime.fraudnid.FraudNID;
import postilion.realtime.sdk.message.bitmap.Iso8583Post;
import postilion.realtime.sdk.message.bitmap.StructuredData;
import postilion.realtime.sdk.util.XPostilion;

/**
 * Wrapper/Container class for storing FMS configuration retrieved from the 
 * fraudnid_swh_cat_fms_config table in the database.
 */
public class TranFmsData implements Serializable
{
	private static final long serialVersionUID = -8590719412934847361L;
	
	protected String idFms;
	protected String tranType;
	protected String nameInterface;
	protected String service;
	protected String cardType;
	protected String fieldAccountRefNum;
	protected String fieldAmount;
	protected String optional1;
	protected String optional2;
	protected String optional3;
	
	public String getIdFms() 
	{
		return idFms;
	}
	
	public void setIdFms(String idFms) 
	{
		this.idFms = idFms;
	}
	
	public String getTranType() 
	{
		return tranType;
	}
	
	public void setTranType(String tranType) 
	{
		this.tranType = tranType;
	}
	
	public String getNameInterface() 
	{
		return nameInterface;
	}
	
	public void setNameInterface(String nameInterface) 
	{
		this.nameInterface = nameInterface;
	}
	
	public String getService() 
	{
		return service;
	}
	
	public void setService(String service) 
	{
		this.service = service;
	}
	
	public String getCardType() 
	{
		return cardType;
	}
	
	public void setCardType(String cardType) 
	{
		this.cardType = cardType;
	}
	
	public String getFieldAccountRefNum() 
	{
		return fieldAccountRefNum;
	}
	
	public void setFieldAccountRefNum(String fieldAccountRefNum) 
	{
		this.fieldAccountRefNum = fieldAccountRefNum;
	}
	
	public String getFieldAmount() 
	{
		return fieldAmount;
	}
	
	public void setFieldAmount(String fieldAmount) 
	{
		this.fieldAmount = fieldAmount;
	}
	
	/**
	 * Retrieves the {@link TranFmsData} specific to the given message from the 
	 * given {@link Hashtable}.
	 * 
	 * @param msg
	 * 			The {@link Iso8583Post} to retrieve the FMS ID from, from 
	 * 			{@link StructuredData StructuredData.FIELD_9998}.
	 * 
	 * @param htTranFmsData
	 * 			The {@link Hashtable} containing the FMS data loaded from the
	 * 			database.
	 * 
	 * @return
	 * 			The {@link TranFmsData} specific to the given message.
	 * 
	 * @throws XPostilion
	 */
	public static TranFmsData getDataForMsg(
			Iso8583Post msg, 
			Hashtable<?, ?> htTranFmsData) 
	throws XPostilion
	{
		if (msg == null || htTranFmsData == null)
		{
			return null;
		}
		
		StructuredData sd = msg.getStructuredData();
		if (sd == null)
		{
			return null;
		}
		
		String idFms = sd.get(FMS_ID_FIELD_9998);
		if (!FraudNID.isStrNullOrEmpty(idFms))
		{
			return (TranFmsData) htTranFmsData.get(idFms);
		}
		
		return null;
	}
	
	public String getOptional1()
	{
		return optional1;
	}

	public void setOptional1(String optional1)
	{
		this.optional1 = optional1;
	}

	public String getOptional2()
	{
		return optional2;
	}

	public void setOptional2(String optional2)
	{
		this.optional2 = optional2;
	}

	public String getOptional3()
	{
		return optional3;
	}

	public void setOptional3(String optional3)
	{
		this.optional3 = optional3;
	}

	
	public static final String FMS_ID_FIELD_9998 = "FIELD_9998";
	
	
	public static class IdFms
	{
		public static final int _3_BBVA_PAYMENT_SERVICES		 = 3;
		public static final int _6_BANORTE_PAYMENT_SERVICES	 	 = 6;
		public static final int _13_TDN_CASH				 	 = 13;
		public static final int _14_DATALOGIC_CASH		 		 = 14;
		public static final int _25_BBCA_BUY_MG_WITHOUT_POINTS 	 = 25;
		public static final int _27_TDN_TCD_WITHOUT_POINTS		 = 27;
		public static final int _28_TDN_TCD_POINTS			 	 = 28;
		public static final int _29_DATALOGIC_TCD_WITHOUT_POINTS = 29;
		public static final int _30_DATALOGIC_TCD_POINTS		 = 30;
		public static final String _0_DEFAULT_SERVICE_NR 	 	 = "0";
	}
}