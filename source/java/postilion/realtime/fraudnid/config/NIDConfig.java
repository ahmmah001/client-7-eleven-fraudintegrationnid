/*----------------------------------------------------------------------------*
 *                                                 
 * Copyright (c) 2018 by Montant Limited. All rights reserved.
 *                                                                             
 * This software is the confidential and proprietary property of 
 * Montant Limited and may not be disclosed, copied or distributed 
 * in any form without the express written permission of Montant Limited.
 *----------------------------------------------------------------------------*/
package postilion.realtime.fraudnid.config;

import java.util.Hashtable;

import postilion.realtime.fraudnid.FraudNID;
import postilion.realtime.fraudnid.dao.RespCodeTxtDao;
import postilion.realtime.fraudnid.dao.SwhConfigDao;
import postilion.realtime.fraudnid.dao.TdnIssuerDataDao;
import postilion.realtime.fraudnid.dao.TranFilterConfigDao;
import postilion.realtime.fraudnid.dao.TranFmsDataDao;
import postilion.realtime.fraudnid.eventrecorder.events.ApplicationError;
import postilion.realtime.sdk.env.SystemProperties;
import postilion.realtime.sdk.env.customclasses.CustomClassParameters;
import postilion.realtime.sdk.installfw.typedefs.Mapper;
import postilion.realtime.sdk.util.XPostilion;

/**
 * User and database parameter configuration container.
 * <p>
 * Provides a single place for all configuration that can be passed around,
 * especially to the {@link Mapper} implementations
 */
public class NIDConfig
{
	public String									tranFilterMode;
	public String									remoteIP;
	public int										remotePort;
	public long										fraudSystemTimeout;
	public long										origTranCacheTimeout;
	public long										fraudReqRspCacheTimeout;
	public long										safqPollInterval;
	public int										retransmissionLimit;
	public String									accountHashAlgorithm;
	public String									mapperClassName;
	public int										safqCleanerPeriod;
	public int										safqRetentionPeriod;
	// Maximum number of store-and-forward transactions in flight
	public int										safqMaxInFlight;
	// Maximum milliseconds to wait for a response to a store-and-forward message
	public int										safqTimeout;
	// Number of saf records to load from the database to limit number of
	// database hits
	public int										safqCacheSize;
	// This will be used when doing lookups against swh_postilionconfiguracion
	// It represents the sink node name that the original ControlFraudes
	// interface
	// would have used.
	// Period to keep history for dup checking. Default = 5 minutes (300000ms)
	public long										duplicateRetentionPeriod	= 300000;
	public String									simulatedNodeName;
	public Hashtable<String, String>			htRspCodeMap					= new Hashtable<String, String>();
	public Hashtable<String, String>			htTdnIssuerData				= new Hashtable<String, String>();
	public Hashtable<String, TranFmsData>	htTranFmsData					= new Hashtable<String, TranFmsData>();
	public Hashtable<String, Integer>		htTranFilterConfig			= new Hashtable<String, Integer>();
	public Hashtable<String, String>			htSwhConfig						= new Hashtable<String, String>();

	/*------------------------------------------------------------------------*/
	/**
	 * Default constructor. Retrieves, parses and validates the necessary
	 * configuration from the given user parameters and custom class parameters.
	 * 
	 * @param integrationDriverParameters
	 *           Any user parameters configured for this integration driver.
	 * 
	 * @param customClassParameters
	 *           Any custom class parameters configured for this class.
	 * 
	 * @throws Exception
	 */
	public NIDConfig(String integrationDriverParameters, String customClassParameters) throws Exception
	{
		// Split the given user parameter string.
		String[] paramArray = integrationDriverParameters.split(SPACE_DELIM_STR);
		// All parameters mandatory as they are positional.
		if (paramArray != null) 
		{
			// 14 = mandatory parameters, and there's 1 optional paramater
			if (paramArray.length < 12 || paramArray.length > 13)
			{
				throw new XPostilion(
					new ApplicationError(new String[]{"Invalid/incorrect number of user parameters " + "configured."}));
			}
		}
		// Retrieve/Parse/Validate the user parameters and set defaults if
		// necessary.
		this.tranFilterMode = paramArray[0].toUpperCase();
		this.remoteIP = paramArray[1];
		this.remotePort = Integer.valueOf(paramArray[2]);
		this.fraudSystemTimeout = Integer.valueOf(paramArray[3]) * 1000;
		this.origTranCacheTimeout = Integer.valueOf(paramArray[4]) * 1000;
		this.fraudReqRspCacheTimeout = Integer.valueOf(paramArray[5]) * 1000;
		this.safqPollInterval = Integer.valueOf(paramArray[6]) * 1000;
		this.retransmissionLimit = Integer.valueOf(paramArray[7]);
		this.accountHashAlgorithm = paramArray[8].toLowerCase();
		this.simulatedNodeName = paramArray[9];
		this.safqCleanerPeriod = Integer.valueOf(paramArray[10]);
		this.safqRetentionPeriod = Integer.valueOf(paramArray[11]);
		if (paramArray.length > 12)
		{
			// Optional - maximum history for duplicate checks
			// Convert to milliseconds from minutes
			this.duplicateRetentionPeriod = Integer.valueOf(paramArray[12]) * 60 * 1000;
		}
		// Retrieve the mapper class name.
		CustomClassParameters ccp = new CustomClassParameters(customClassParameters);
		String tempMapperClassName = ccp.getParameterValue(FraudNID.MAPPER_CLASS_NAME);
		if (!FraudNID.isStrNullOrEmpty(tempMapperClassName))
		{
			this.mapperClassName = tempMapperClassName;
		}
		else
		{
			throw new XPostilion(
				new ApplicationError(new String[]{"Invalid mapper class name configured: '" + this.mapperClassName + "'"}));
		}
	}

	/**
	 * Loads the database configuration
	 * 
	 * @throws Exception
	 */
	public void load() throws Exception
	{
		// Load the database information/configuration.
		RespCodeTxtDao.getRspCodeMap(FraudNID.CONTROL_FRAUDES_NODE_NAME, htRspCodeMap);
		TranFmsDataDao.getTranFmsData(htTranFmsData);
		TdnIssuerDataDao.getTdnIssuerData(htTdnIssuerData);
		TranFilterConfigDao.getTranFilterConfig(htTranFilterConfig);
		SwhConfigDao.getConfiguration(htSwhConfig, simulatedNodeName, 1);
		safqCacheSize = SystemProperties.getAsInt(SAFQ_SAF_CACHE_SIZE, DEFAULT_SAF_CACHE_SIZE);
		safqMaxInFlight = SystemProperties.getAsInt(SAFQ_MAX_IN_FLIGHT, DEFAULT_SAF_MAX_IN_FLIGHT);
		safqTimeout = SystemProperties.getAsInt(SAFQ_TIMEOUT, DEFAULT_SAF_TIMEOUT);
	}

	public String toString()
	{
		return "Transaction Identification/Filter Mode: '" + tranFilterMode + "', " + "Remote IP: '" + remoteIP + "', "
			+ "Remote Port: '" + remotePort + "', " + "Fraud System Timeout: '"
			+ fraudSystemTimeout / 1000 + "s', " + "Original Transaction Cache Timeout: '" + origTranCacheTimeout / 1000
			+ "s', " + "Fraud ReqRsp Cache Timeout: '" + fraudReqRspCacheTimeout / 1000 + "s', "
			+ "Store-and-Forward Poll Interval: '" + safqPollInterval / 1000 + "s', " + "Advice Retransmission Limit: '"
			+ retransmissionLimit + "', " 
			+ "Hashing algorithm for Account References: '" + accountHashAlgorithm + "', "
			+ "Mapper Class Name: '" + mapperClassName + "', " + "swh_postilionconfiguracion Sink Node Name: '"
			+ simulatedNodeName + "'" + ", SAFQ cleaner period: '" + safqCleanerPeriod + "', safqRetentionPeriod: '"
			+ safqRetentionPeriod + "'";
	}
	// Maximum number of store-and-forward transactions to preload
	public static final String	SAFQ_SAF_CACHE_SIZE			= "postilion.realtime.fraudnid.config.saf_cache_size";
	public static final String	SAFQ_MAX_IN_FLIGHT			= "postilion.realtime.fraudnid.config.saf_max_in_flight";
	public static final String	SAFQ_TIMEOUT					= "postilion.realtime.fraudnid.config.saf_timeout";
	public static final int		DEFAULT_SAF_CACHE_SIZE		= 100;
	public static final int		DEFAULT_SAF_MAX_IN_FLIGHT	= 1;
	public static final int		DEFAULT_SAF_TIMEOUT			= 5000;
	public static final String	SPACE_DELIM_STR				= " ";
	
	public static final class SupportedHashFunction
	{
		public static final String FALSE_NONE 					= "false";
		public static final String TRUE_MD5 					= "true";
		public static final String SHA_256 						= "sha-256";
	}
}