/*----------------------------------------------------------------------------*
 *                                                 
 * Copyright (c) 2018 by Montant Limited. All rights reserved.
 *                                                                             
 * This software is the confidential and proprietary property of 
 * Montant Limited and may not be disclosed, copied or distributed 
 * in any form without the express written permission of Montant Limited.
 *----------------------------------------------------------------------------*/

package postilion.realtime.fraudnid;

import postilion.realtime.fraudnid.eventrecorder.ILogger;
import postilion.realtime.fraudnid.ipc.ConnectionManager;
import postilion.realtime.fraudnid.msg.Iso8583FMS;
import postilion.realtime.sdk.node.Interchange;
import postilion.realtime.sdk.util.Processor;
import postilion.realtime.sdk.util.Queue;

/**
 * This {@link Processor} is responsible for sending {@link Iso8583FMS} messages
 * to the FMS using the {@link ConnectionManager}. It exists to free up the 
 * threads belonging to the {@link Interchange} and the {@link 
 * ConnectionManager} so they may perform their tasks without being blocked by 
 * an attempt to send a message.
 */
public class FmsMsgQueue extends Processor
{
	protected Queue queue = null;
	protected ILogger logger = null;
	protected ConnectionManager connManager = null;
	
	/*------------------------------------------------------------------------*/
	public FmsMsgQueue(ConnectionManager connManager, ILogger logger)
	{
		queue = getNewProcessorQueue();
		putQueue(queue);
		this.connManager = connManager;
		this.logger = logger;
	}
	
	/*------------------------------------------------------------------------*/
	public void init()
	{
		this.start();
	}
	
	/*------------------------------------------------------------------------*/
	@Override
	protected boolean processEvent(Object event)
	{
		if (event instanceof Iso8583FMS)
		{
			try
			{
				connManager.send((Iso8583FMS) event);
			}
			catch (Exception e)
			{
				logger.recordEvent(
					FraudNID.getApplicationException(
						"An error occurred while attempting to send a message to the FMS. Exception: ", 
						e)); 
			}
			
			return true;
		}
		
		return false;
	}
	
	/*------------------------------------------------------------------------*/
	/**
	 * Append a new fraud check request event to the queue. See {@link #processEvent(Object)}
	 * @param msgToFMS
	 */
	public void enqueueMsg(Iso8583FMS msgToFMS)
	{
		queue.append(msgToFMS);
	}

	/*------------------------------------------------------------------------*/
}