/*----------------------------------------------------------------------------*
 *                                                 
 * Copyright (c) 2018 by Montant Limited. All rights reserved.
 *                                                                             
 * This software is the confidential and proprietary property of 
 * Montant Limited and may not be disclosed, copied or distributed 
 * in any form without the express written permission of Montant Limited.
 *----------------------------------------------------------------------------*/

package postilion.realtime.fraudnid.msg;

import postilion.realtime.fraudnid.config.NIDConfig;
import postilion.realtime.fraudnid.config.TranFmsData;
import postilion.realtime.sdk.message.bitmap.Iso8583Post;

/**
 * Interface to be implemented for all Mapper_vX classes.
 * <br><br>
 * <b>Developer Notes</b>: Mapper classes use a version system and the version 
 * number should be incremented by 1 for each new mapper class. This applies to 
 * changes as well, allowing the customer to activate a changed mapper and if 
 * they want to rollback to the previous mapper class, it is a simple 
 * configuration change. It is not advised to change this interface unless 
 * absolutely necessary.
 */
public interface IMapper
{
	/*------------------------------------------------------------------------*/
	/**
	 * Initializes the mapper by re-loading the given configuration into 
	 * the mapper class for use during processing. This method should be called 
	 * whenever a new mapper class is loaded.
	 * 
	 * @param config
	 * 			The {@link NIDConfig} to load and reference during message 
	 * 			construction/processing.
	 * 
	 * @throws Exception
	 */
	public void init(NIDConfig config)
	throws Exception;
	
	/*------------------------------------------------------------------------*/
	/**
	 * Constructs an 0200 {@link Iso8583FMS} Transaction Request message to 
	 * request a fraud check from the Fraud Management System.
	 * 
	 * @param msgFromTM
	 * 			The {@link Iso8583Post} message to reference during 
	 * 			construction.
	 * 
	 * @param interfaceIndex
	 * 			The interface index, retrieved from the transaction filter 
	 * 			configuration, used to determine which fields/values, 
	 * 			configured in the FMS database configuration, to reference
	 * 			during construction.
	 * 
	 * @param tranFmsData
	 * 			The {@link TranFmsData}, loaded from the database, to reference
	 * 			during construction.
	 * 
	 * @return
	 * 			A new {@link Iso8583FMS} message.
	 * 
	 * @throws Exception
	 */
	public Iso8583FMS constructTranReqToFMS(
			Iso8583Post msgFromTM, 
			int interfaceIndex, 
			TranFmsData tranFmsData)
	throws Exception;
	
	/*------------------------------------------------------------------------*/
	/**
	 * Constructs an 0420 {@link Iso8583FMS} Acquirer Reversal Advice message 
	 * to request a fraud check reversal from the Fraud Management System.
	 * 
	 * @param fraudReq
	 * 			The original {@link Iso8583FMS} fraud request message to 
	 * 			reference during construction.
	 * 
	 * @return
	 * 			A new {@link Iso8583FMS} message.
	 * 
	 * @throws Exception
	 */
	public Iso8583FMS constructAcquirerRevAdvToFMS(Iso8583FMS fraudReq)
	throws Exception;
	
	/*------------------------------------------------------------------------*/
	/**
	 * Constructs an 0210 {@link Iso8583FMS} Transaction Request Response 
	 * message to decline a transaction from Transaction Manager for which a 
	 * fraud check failed.
	 * 
	 * @param msgFromTM
	 * 			The {@link Iso8583Post} message to reference during 
	 * 			construction.
	 * 
	 * @param msgToFMS
	 * 			The {@link Iso8583FMS} linked fraud request to reference 
	 * 			during construction.
	 * 
	 * @param msgFromFMS
	 * 			The {@link Iso8583FMS} linked (declined) fraud request response 
	 * 			to reference during construction.
	 * 
	 * @return
	 * 			A new {@link Iso8583FMS} message.
	 * 
	 * @throws Exception
	 */
	public Iso8583Post constructDeclineToTM(
			Iso8583Post msgFromTM, 
			Iso8583FMS msgToFMS, 
			Iso8583FMS msgFromFMS)
	throws Exception;

	/*------------------------------------------------------------------------*/
	/**
	 * Processes a message received from the remote sink entity, updating it 
	 * with all the necessary fraud-check-related information.
	 * 
	 * @param msgToTM
	 * 			The {@link Iso8583Post} message to update.
	 * 
	 * @param fraudReq
	 * 			The {@link Iso8583FMS} linked fraud request to reference 
	 * 			during construction.
	 * 
	 * @param fraudRsp
	 * 			The {@link Iso8583FMS} linked (declined) fraud request response 
	 * 			to reference during construction.
	 * 
	 * @throws Exception
	 */
	public void processMsgFromRemoteSink(
			Iso8583Post msgToTM, 
			Iso8583FMS fraudReq, 
			Iso8583FMS fraudRsp)
	throws Exception;
	
	/*------------------------------------------------------------------------*/
	/**
	 * Processes a message received from Transaction Manager at the sink node, 
	 * updating it with all the necessary fraud-check-related information.
	 * 
	 * @param msgFromTM
	 * 			The {@link Iso8583Post} message to update.
	 * 
	 * @param fraudReq
	 * 			The {@link Iso8583FMS} linked fraud request to reference 
	 * 			during construction.
	 * 
	 * @throws Exception
	 */
	public void processMsgFromTranmgrSinkNode(
			Iso8583Post msgFromTM, 
			Iso8583FMS fraudReq)
	throws Exception;
	
	/*------------------------------------------------------------------------*/
}