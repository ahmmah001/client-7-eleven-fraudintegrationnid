/*----------------------------------------------------------------------------*
 *                                                 
 * Copyright (c) 2018 by Montant Limited. All rights reserved.
 *                                                                             
 * This software is the confidential and proprietary property of 
 * Montant Limited and may not be disclosed, copied or distributed 
 * in any form without the express written permission of Montant Limited.
 *----------------------------------------------------------------------------*/

package postilion.realtime.fraudnid.msg;

import java.security.MessageDigest;
import java.util.Enumeration;
import postilion.realtime.fraudnid.FraudNID;
import postilion.realtime.fraudnid.config.NIDConfig;
import postilion.realtime.sdk.message.bitmap.Iso8583;
import postilion.realtime.sdk.util.XPostilion;

/**
 * This class is a basic extension of the {@link Iso8583} class. It provides 
 * additional information storage, provides utility methods related to the 
 * object type and defines FMS-specific field names.
 */
public class Iso8583FMS extends Iso8583
{
	protected long		reqSendTime		= 0;
	protected long		rspReceiveTime	= 0;
	// Sometimes we need to track the identity of the message too.
	protected String	id					= null;
	
	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	/*------------------------------------------------------------------------*/
	public void putReqSendTime(long timeInMs)
	{
		reqSendTime = timeInMs;
	}
	
	/*------------------------------------------------------------------------*/
	public long getReqSendTime()
	{
		return reqSendTime;
	}
	
	/*------------------------------------------------------------------------*/
	public void putRspReceiveTime(long timeInMs)
	{
		rspReceiveTime = timeInMs;
	}
	
	/*------------------------------------------------------------------------*/
	public long getRspReceiveTime()
	{
		return rspReceiveTime;
	}
	
	/*------------------------------------------------------------------------*/
	public static class MsgType extends Iso8583.MsgType
	{
		/**
		 * Returns the request message type for the given response message type.
		 * 
		 * @param rspMsgType
		 * 			The response message type to convert.
		 * 
		 * @return
		 * 			The request message type.
		 */
		public static int getRequest(int rspMsgType)
		{
			return rspMsgType - (rspMsgType % 2) - 0x10;
		}
	}
	
	/*------------------------------------------------------------------------*/
	/**
	 * Generates the MD5 hash for the given value.
	 * 
	 * @param value
	 *           The value to generate the hash from.
	 * @param digestStr
	 *           The specified digest algorithm to use during hashing.
	 * @return The hash for the given value.
	 * 
	 * @throws Exception
	 */
	public static String getHashValue(String value, String digestStr) throws Exception
	{
		if(FraudNID.isStrNullOrEmpty(value))
		{
			return null;
		}
		
		MessageDigest md = MessageDigest.getInstance(digestStr);
		md.update(value.getBytes());
		byte[] digest = md.digest();
		
		StringBuffer sb = new StringBuffer();
		for (byte b : digest) 
		{
			sb.append(String.format("%02x", b & 0xff));
		}

		return sb.toString();
	}
	
	/*------------------------------------------------------------------------*/
	/**
	 * Iterates through an {@link Iso8583FMS} message and produces a 
	 * user-friendly {@link String} representation of the message suitable for 
	 * storing in StructuredData.
	 * 
	 * @return
	 * 			A {@link String} representation of an {@link Iso8583FMS} message.
	 * 
	 * @throws Exception
	 */
	public String toStructDataString(String accountHashAlgorithm) throws Exception
	{
		StringBuilder strBuilder = new StringBuilder();
		strBuilder.append("[MTI = '" + this.getMessageType() + "']");
		
		Enumeration<?> enu = this.enumerateSetFields();
		while (enu.hasMoreElements())
		{
			String fieldNr = (String) enu.nextElement();
			
			// Mask Field 106 - Account Number/Reference if it's not hashed.
			if (fieldNr.equals(String.valueOf(Bit._106_ACCOUNT_NR_REF)) 
				&& NIDConfig.SupportedHashFunction.FALSE_NONE.equals(accountHashAlgorithm))
			{
				strBuilder.append("[Field " + fieldNr + " = '*']");
			}
			else
			{
				strBuilder.append(
					"[Field " + fieldNr + " = '" + this.getField(fieldNr) + "']");
			}
		}
		
		return strBuilder.toString();
	}
	
	/*------------------------------------------------------------------------*/
	@Override
	public Object clone() throws CloneNotSupportedException
	{
		try
		{
			byte[] byteImage = this.toMsg();
			Iso8583FMS newMsg = new Iso8583FMS();
			newMsg.fromMsg(byteImage);
			return newMsg;
		}
		catch (XPostilion e)
		{
			throw new CloneNotSupportedException();
		}
	}
	
	/*------------------------------------------------------------------------*/
	public static class Bit extends Iso8583.Bit
	{
		public static final int _105_COMPANY			 	 = 105;
		public static final int _106_ACCOUNT_NR_REF		 	 = 106;
		public static final int _107_TRAN_TYPE			 	 = 107;
		public static final int _108_INTERFACE_NAME		 	 = 108;
		public static final int _109_SERVICE_NAME		 	 = 109;
		public static final int _110_CARD_TYPE			 	 = 110;
		public static final int _111_SERVICE_NR			 	 = 111;
		public static final int _112_OPTIONAL_1			 	 = 112;
		public static final int _114_OPTIONAL_2			 	 = 114;
		public static final int _115_OPTIONAL_3			 	 = 115;
		public static final int _116_TRAN_DATE_TIME		 	 = 116;
		public static final int _117_SHIFT_NR			 	 = 117;
		public static final int _118_CASHIER_EMPLOYEE_NR 	 = 118;
		public static final int _119_TICKET_NR			 	 = 119;
		public static final int _120_RESERVED 			 	 = 120;
		public static final int _121_POS_ECR_IP			 	 = 121;
	}
	
	/*------------------------------------------------------------------------*/
}