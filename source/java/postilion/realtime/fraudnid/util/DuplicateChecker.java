/*----------------------------------------------------------------------------*\
 *                                                                            *
 * Copyright (c) 2019 by Montant Limited. All rights reserved.                *
 *                                                                            *
 * This software is the confidential and proprietary property of              *
 * Montant Limited and may not be disclosed, copied or distributed            *
 * in any form without the express written permission of Montant Limited.     *
 *                                                                            *
\*----------------------------------------------------------------------------*/
package postilion.realtime.fraudnid.util;

import postilion.realtime.sdk.util.TimedHashtable;

/**
 * Keeps a history of keys that can be used to determine whether there has been
 * a duplicate before
 */
public class DuplicateChecker
{
	public static final DuplicateChecker	INSTANCE					= new DuplicateChecker();
	// Default duplication period is 5 minutes
	private static final long					DEFAULT_RETENTION_MS	= 300000;
	private final TimedHashtable				history					= new TimedHashtable(DEFAULT_RETENTION_MS);

	/**
	 * A record that will be checked for duplication
	 */
	private static class Record
	{
		// The time in milliseconds that the entry was added
		private final long inserted = System.currentTimeMillis();

		public long getInserted()
		{
			return inserted;
		}
	}

	/**
	 * Hide the constructor - only the singleton can construct
	 */
	private DuplicateChecker()
	{
		// Does nothing
	}

	/**
	 * Checks whether the given key was duplicated. If so, the original is
	 * retained
	 * 
	 * @param key
	 * @param lifespan
	 * @return -1 if the record wasn't duplicated, otherwise the time in ms that
	 *         the original record was added
	 */
	public synchronized long isDuplicated(String key, long lifespan)
	{
		Record hit = (Record)history.get(key);
		if (hit == null)
		{
			// No hit - this is the first occurrence in living memory
			// Add it so the next guy can check for duplication
			history.put(key, new Record(), lifespan);
			// Indicate no hit
			return -1l;
		}
		// Return the timestamp for the original
		return hit.getInserted();
	}

	public synchronized void clear()
	{
		history.clear();
	}
}
