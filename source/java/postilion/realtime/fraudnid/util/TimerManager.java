/*----------------------------------------------------------------------------*\
 *                                                                            *
 * Copyright (c) 2019 by Montant Limited. All rights reserved.                *
 *                                                                            *
 * This software is the confidential and proprietary property of              *
 * Montant Limited and may not be disclosed, copied or distributed            *
 * in any form without the express written permission of Montant Limited.     *
 *                                                                            *
\*----------------------------------------------------------------------------*/
package postilion.realtime.fraudnid.util;

import postilion.realtime.fraudnid.eventrecorder.ILogger;

/**
 * Manages timers that have been started: <br>
 * <ul>
 * <li>Track timers so they can be stopped
 * <li>Notify the owner of the timers when they expire
 * </ul>
 * Implements a quick linked list of timers (since we're doing timers on a NID,
 * nano accuracy isn't needed, and there won't be billions of timers). New
 * timers are added into the list in expiry time order for expiry from the front
 * (since the oldest timers will be guaranteed to be at the head) <br>
 * A thread periodically checks for expired times and notifies the caller<br>
 * Things don't go well if there's more than one instance, so this is a
 * singleton.
 * <p>
 * This custom implementation is used to provide direct access to the timer
 * manager to simplify stop requests - postilion.realtime.sdk.util.Timer forces
 * you to track timers because you need the original timer to stop it. Here we
 * allow stop by name.
 */
public class TimerManager
	implements
	Runnable
{
	public static final TimerManager	INSTANCE	= new TimerManager();
	private static TimerElement		head		= null;
	private static final Object		latch		= new Object();
	private boolean						stop		= false;
	private ILogger						logger	= null;

	public interface TimerUser
	{
		/**
		 * Callback invoked when a timer expires
		 * 
		 * @param timer
		 */
		void onTimerExpired(StanchionTimer timer, long elapsedTime);
	}

	public void setLogger(ILogger logger)
	{
		INSTANCE.logger = logger;
	}

	private static class TimerElement
	{
		private StanchionTimer	timer;
		private TimerElement		next		= null;
		private TimerElement		prev		= null;
		private TimerUser			user;
		// This is when the timer is expected to expire
		private long				expiryTime;
		private boolean			stopped	= false;
		// Original timeout
		private long				timeout;
		// Time of original creation
		private long				createdTime;

		public TimerElement(StanchionTimer timer, TimerUser user, long timeout)
		{
			super();
			this.timer = timer;
			this.user = user;
			this.timeout = timeout;
			// Calculate when the timer will expire
			createdTime = System.currentTimeMillis();
			expiryTime = createdTime + timeout;
		}

		public synchronized void stop()
		{
			if (!stopped)
			{
				stopped = true;
			}
		}

		/**
		 * @return true if the timer has expired, false if not
		 */
		public boolean hasExpired(long now)
		{
			if (expiryTime <= now)
			{
				return true;
			}
			return false;
		}

		public StanchionTimer getTimer()
		{
			return timer;
		}

		public TimerUser getUser()
		{
			return user;
		}

		public long getExpiryTime()
		{
			return expiryTime;
		}

		@Override
		public String toString()
		{
			StringBuilder builder = new StringBuilder();
			builder.append("TimerElement [handle=").append(timer).append(", expiryTime=").append(expiryTime)
				.append(", timeout=").append(timeout).append("]");
			return builder.toString();
		}
	}

	// Nobody else can instantiate
	private TimerManager()
	{
		Thread thread = new Thread(this);
		// Don't let this thread keep the VM running
		thread.setDaemon(true);
		thread.setName("Gateway Timers");
		thread.start();
	}

	/**
	 * Inserts
	 * 
	 * @param timer
	 * @param user
	 */
	public synchronized void startTimer(StanchionTimer timer, TimerUser user)
	{
		TimerElement newTimer = new TimerElement(timer, user, timer.getPeriod());
		TimerElement el = head;
		TimerElement prev = null;
		if (timer.getPeriod() == 0)
		{
			throw new RuntimeException("Invalid timeout of 0 for timer " + timer);
		}
		// Need the time this will expire so we can insert it in the right place
		// in the list
		long expiryTime = newTimer.getExpiryTime();
		// Find that place... iterate the list until we hit an empty spot or we
		// find the time range
		while(el != null && el.getExpiryTime() < expiryTime)
		{
			prev = el;
			el = el.next;
		}
		// Save the new entry here - link it to the next and previous elements
		newTimer.next = el;
		newTimer.prev = prev;
		// Relink the new neighbors (if any)
		if (el != null)
		{
			el.prev = newTimer;
		}
		if (prev != null)
		{
			// Link the previous element to the new one
			prev.next = newTimer;
		}
		if (head == el)
		{
			// New element is the new head
			head = newTimer;
		}
		trace("Started timer " + timer);
	}

	/**
	 * Find the timer with the given timer ID and stop it. Timer is also removed
	 * from the list
	 */
	public synchronized void stopTimer(String id) throws Exception
	{
		TimerElement el = head;
		TimerElement stopped = null;
		while(el != null)
		{
			if (el.getTimer().getId().equals(id))
			{
				trace("Stopped timer " + id);
				el.stop();
				stopped = el;
				TimerElement prev = el.prev;
				TimerElement next = el.next;
				// Relink the previous and next elements, as applicable
				if (next != null)
				{
					next.prev = prev;
				}
				if (prev != null)
				{
					prev.next = next;
				}
				if (el == head)
				{
					// If we're the head, point it to the next element
					head = next;
				}
				// Dereference for GC cleanliness
				el.next = null;
				el.prev = null;
				return;
			}
			// Next element
			el = el.next;
		}
		if (stopped == null)
		{
			trace("Ignoring stop request on non-existent timer " + id);
		}
	}

	/**
	 * Stops all timers in the collection. Here for test purposes.
	 */
	public synchronized void stopAllTimers()
	{
		while(head != null)
		{
			TimerElement el = head;
			el.stop();
			head = head.next;
			el.next = null;
			el.prev = null;
		}
	}

	/**
	 * The manager calls this to check for timers that need to be expired
	 */
	private synchronized void expireTimers()
	{
		// We work from the head only since the expired timers should be at at
		// the head
		long now = System.currentTimeMillis();
		while(head != null && head.hasExpired(now))
		{
			trace("Expiring timer " + head.timer);
			// Notify the user of the timer
			head.getUser().onTimerExpired(head.getTimer(), now - head.createdTime);
			// Remove the current head
			TimerElement el = head;
			head = head.next;
			// Clean up references
			el.next = null;
			el.prev = null;
		}
	}

	private void trace(Object string)
	{
		if (logger != null)
		{
			logger.debug(string);
		}
	}

	/**
	 * Periodically check for expired timers
	 */
	@Override
	public void run()
	{
		// Run until stopped
		boolean done = false;
		while(!done)
		{
			synchronized(latch)
			{
				if (stop)
				{
					done = true;
					continue;
				}
			}
			// Yield
			try
			{
				Thread.sleep(10);
				expireTimers();
			}
			catch(InterruptedException e)
			{
				// Eat this
			}
		}
	}

	public void shutdown()
	{
		stopAllTimers();
		synchronized(latch)
		{
			stop = true;
		}
	}
}
