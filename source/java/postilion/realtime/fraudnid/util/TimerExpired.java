/*----------------------------------------------------------------------------*\
 *                                                                            *
 * Copyright (c) 2019 by Montant Limited. All rights reserved.                *
 *                                                                            *
 * This software is the confidential and proprietary property of              *
 * Montant Limited and may not be disclosed, copied or distributed            *
 * in any form without the express written permission of Montant Limited.     *
 *                                                                            *
\*----------------------------------------------------------------------------*/
package postilion.realtime.fraudnid.util;

/**
 * Encapsulates an expired timer... <br>
 * 
 */
public class TimerExpired
{
	private StanchionTimer ref;

	public TimerExpired(StanchionTimer ref)
	{
		this.ref = ref;
	}

	public StanchionTimer getRef()
	{
		return ref;
	}

	public void setRef(StanchionTimer ref)
	{
		this.ref = ref;
	}
}
