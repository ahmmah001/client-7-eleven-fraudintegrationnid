/*----------------------------------------------------------------------------*\
 *                                                                            *
 * Copyright (c) 2019 by Montant Limited. All rights reserved.                *
 *                                                                            *
 * This software is the confidential and proprietary property of              *
 * Montant Limited and may not be disclosed, copied or distributed            *
 * in any form without the express written permission of Montant Limited.     *
 *                                                                            *
\*----------------------------------------------------------------------------*/
package postilion.realtime.fraudnid.util;

/**
 * A timer object for timers that operate outside of the interchange SDK
 */
public class StanchionTimer
{
	private long	period;
	private String	id;
	private Object	echoData;

	/**
	 * Create a timer
	 * 
	 * @param id
	 *           Unique identity for the timer
	 * @param period
	 *           Timeout period for the timer
	 * @param echoData
	 *           Anything the user wants to associate with the timer
	 */
	public StanchionTimer(String id, long period, Object echoData)
	{
		this.setId(id);
		this.setPeriod(period);
		this.setEchoData(echoData);
	}

	public Object getEchoData()
	{
		return echoData;
	}

	public void setEchoData(Object echoData)
	{
		this.echoData = echoData;
	}

	public long getPeriod()
	{
		return period;
	}

	public void setPeriod(long period)
	{
		this.period = period;
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	/**
	 * Describe en Anglais
	 */
	@Override
	public String toString()
	{
		StringBuilder builder = new StringBuilder();
		builder.append("StanchionTimer [id=").append(id).append(", period=").append(period).append(", echoData=")
			.append((echoData != null) ? echoData.getClass().getSimpleName() : "<null>").append(']');
		return builder.toString();
	}

	/**
	 * This object's hashcode is based upon the ID
	 */
	@Override
	public int hashCode()
	{
		return id.hashCode();
	}

	/**
	 * Compare this instance with another one using the ID
	 */
	@Override
	public boolean equals(Object obj)
	{
		if (obj instanceof StanchionTimer)
		{
			return id.equals(((StanchionTimer)obj).getId());
		}
		return false;
	}
}
