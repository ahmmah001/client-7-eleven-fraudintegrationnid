/*----------------------------------------------------------------------------*
 *                                                 
 * Copyright (c) 2018 by Montant Limited. All rights reserved.
 *                                                                             
 * This software is the confidential and proprietary property of 
 * Montant Limited and may not be disclosed, copied or distributed 
 * in any form without the express written permission of Montant Limited.
 *----------------------------------------------------------------------------*/

package postilion.realtime.fraudnid.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.Hashtable;

import postilion.realtime.fraudnid.FraudNID;
import postilion.realtime.sdk.jdbc.JdbcManager;

/**
 * The data access object responsible for all interaction with the 
 * TdnIssuerData table from the {@link FraudNID}.
 */
public class TdnIssuerDataDao
{
	/*------------------------------------------------------------------------*/
	public static void getTdnIssuerData(
			Hashtable<String, String> htTdnIssuerData) 
	throws Exception
	{
		Connection cn = null;
		CallableStatement stmt = null;
		ResultSet rs  = null;

		try
		{
			htTdnIssuerData.clear();
			cn = JdbcManager.getDefaultConnection();
			stmt = cn.prepareCall(TdnIssuerDataDao.SP_GET_ISSUERS_TDN);
			
			rs = stmt.executeQuery();
			while (rs.next()) 
			{
				htTdnIssuerData.put(
						rs.getString(TdnIssuerDataDao.Columns.SKU), 
						rs.getString(TdnIssuerDataDao.Columns.RESP_TEXT));
			}

			JdbcManager.commit(cn, stmt, rs);
		}
		finally
		{
			JdbcManager.cleanup(cn, stmt, rs);
		}
	}
	
	/*------------------------------------------------------------------------*/
	
	public static final String SP_GET_ISSUERS_TDN = 
			"{call sp_get_issuers_tdn}";
	
	public static final class Columns
	{
		public static final String SKU = "sku";
		public static final String RESP_TEXT = "resp_text";
	}
	
	/*------------------------------------------------------------------------*/
}