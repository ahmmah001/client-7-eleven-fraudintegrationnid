/*----------------------------------------------------------------------------*\
 *                                                                            *
 * Copyright (c) 2018 by Montant Limited. All rights reserved.                *
 *                                                                            *
 * This software is the confidential and proprietary property of              *
 * Montant Limited and may not be disclosed, copied or distributed            *
 * in any form without the express written permission of Montant Limited.     *
 *                                                                            *
\*----------------------------------------------------------------------------*/
package postilion.realtime.fraudnid.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import postilion.realtime.sdk.jdbc.JdbcManager;

/**
 * Data access into Realtime's database
 */
public class RealtimeDao
{
	/**
	 * @return The overall retention period obtained from Realtime's node
	 *         configuration, which is the maximum of all nodes
	 * @throws SQLException
	 */
	public static int getRealtimeNodeRetentionPeriod() throws SQLException
	{
		final String sql = "SELECT MAX(retention_period) FROM tm_nodes";
		Connection cn = null;
		ResultSet rs = null;
		PreparedStatement stmt = null;
		int retentionPeriod = 0;
		try
		{
			cn = JdbcManager.getRealtimeConnection();
			stmt = cn.prepareStatement(sql);
			rs = stmt.executeQuery();
			if (rs.next())
			{
				retentionPeriod = rs.getInt(1);
			}
			else
			{
				throw new SQLException("No retention period retrieved from the database");
			}
			JdbcManager.commit(cn, stmt, rs);
			return retentionPeriod;
		}
		finally
		{
			JdbcManager.cleanup(cn, stmt, rs);
		}
	}
}
