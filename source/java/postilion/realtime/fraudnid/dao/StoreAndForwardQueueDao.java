/*----------------------------------------------------------------------------*
 *                                                 
 * Copyright (c) 2018 by Montant Limited. All rights reserved.
 *                                                                             
 * This software is the confidential and proprietary property of 
 * Montant Limited and may not be disclosed, copied or distributed 
 * in any form without the express written permission of Montant Limited.
 *----------------------------------------------------------------------------*/

package postilion.realtime.fraudnid.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import postilion.realtime.fraudnid.FraudNID;
import postilion.realtime.fraudnid.cache.OriginalReqCache;
import postilion.realtime.fraudnid.eventrecorder.ILogger;
import postilion.realtime.fraudnid.eventrecorder.events.SafRecordReplaced;
import postilion.realtime.fraudnid.msg.Iso8583FMS;
import postilion.realtime.fraudnid.safq.SAFQTran;
import postilion.realtime.sdk.jdbc.JdbcManager;
import postilion.realtime.sdk.message.IMessage;

/**
 * The data access object responsible for all interaction with the
 * fraudnid_persistent_safq table from the {@link FraudNID}.<br>
 * <b>Important to note that the system assumes that the ID is unique</b>
 */
public class StoreAndForwardQueueDao
{
	/**
	 * Insert the given SAF record
	 * 
	 * @param record
	 * @param logger
	 * @return
	 * @throws SQLException
	 */
	public static int insert(SAFQTran record, ILogger logger) throws SQLException
	{
		final String insertSql = "IF NOT EXISTS (SELECT 1 FROM fraudnid_persistent_safq WHERE id = ? AND sink_node_name = ?) "
			+ "BEGIN; INSERT into fraudnid_persistent_safq(id, msg, mti, sink_node_name, queue_time, retries_remaining, sent) VALUES(?,?,?,?,?,?,?); END;";
		Connection cn = null;
		PreparedStatement stmt = null;
		int recordsUpdated = 0;
		try
		{
			cn = JdbcManager.getRealtimeConnection();
			stmt = cn.prepareCall(insertSql);
			stmt.setString(1, record.getId());
			stmt.setString(2, record.getSinkNodeName());
			stmt.setString(3, record.getId());
			stmt.setString(4, record.getMsgData());
			stmt.setString(5, record.getMti());
			stmt.setString(6, record.getSinkNodeName());
			stmt.setTimestamp(7, record.getQueueTime());
			stmt.setInt(8, record.getRetriesRemaining());
			stmt.setBoolean(9, false);
			recordsUpdated = stmt.executeUpdate();
			JdbcManager.commit(cn, stmt);
			return recordsUpdated;
		}
		finally
		{
			JdbcManager.cleanup(cn, stmt);
		}		
	}
	/**
	 * Updates or inserts the given record into the SAF table.
	 * 
	 * @param record
	 *           The data to insert or update
	 * @param insert
	 *           If true, attempts to insert the record (but only if it doesn't
	 *           already exist)
	 * @param logger
	 *           Use this for logging/tracing
	 * @param msgIn
	 * 			 incoming message
	 * @param msgOut
	 * 			 outgoing message
	 * @param accountHashAlgorithm
	 * 			 Account details are hashed
	 * @throws Exception in case of error
	 */
	public static void insertOrUpdateTranInSAFQ(
		SAFQTran record, 
		boolean insert, 
		ILogger logger, 
		Iso8583FMS msgIn, 
		Iso8583FMS msgOut,
		String accountHashAlgorithm)
	throws Exception
	{
		Connection cn = null;
		PreparedStatement stmt = null;
		int recordsUpdated = 0;
		try
		{

			if (insert)
			{
				recordsUpdated = insert(record, logger);
			}
			else
			{
				StringBuilder sb = new StringBuilder("UPDATE fraudnid_persistent_safq SET sent = ?");
				// Only update retries remaining if it is a real value
				if (record.getRetriesRemaining() != -1)
				{
					sb.append(", retries_remaining = ?");
				}
				// Only update response data if there is any
				if (record.getResponseCode() != null)
				{
					sb.append(", response_code = ?");
				}
				if (record.getResponseTime() != null)
				{
					sb.append(", response_time = ?");
				}
				if(msgIn != null) {
					sb.append(", msgin = ?");
				}
				if(msgOut != null) {
					sb.append(", msgout = ?");
				}
				sb.append(" WHERE id = ? AND sink_node_name = ?");		
				cn = JdbcManager.getDefaultConnection();
				stmt = cn.prepareCall(sb.toString());
				int i = 1;
				stmt.setBoolean(i++, record.getSent());
				if(record.getRetriesRemaining() != -1) 
				{
					stmt.setInt(i++, record.getRetriesRemaining());
				}
				if(record.getResponseCode() != null) 
				{
					stmt.setString(i++, record.getResponseCode());
				}
				if(record.getResponseTime() != null) 
				{
					stmt.setString(i++,record.getResponseTime());
				}
				if(msgIn != null) {
					stmt.setString(i++, msgIn.toStructDataString(accountHashAlgorithm));
				}
				if(msgOut != null) {
					stmt.setString(i++, msgOut.toStructDataString(accountHashAlgorithm));
				}
				stmt.setString(i++, record.getId());
				stmt.setString(i++, record.getSinkNodeName());
				recordsUpdated = stmt.executeUpdate();
			}
			
			if (recordsUpdated == 0)
			{
				if (logger.isTraceOn())
				{
					logger.debug("No records were " + (insert ? "inserted" : "updated") + " for key " + record.getId());
				}
				if (insert)
				{
					// If zero records were updated on insert then it means there was
					// already a record with the same key
					// Delete it and try again
					logger.debug("Replacing a record with ID " + record.getId());
					deleteSafRecord(record);
					logger.recordEvent(new SafRecordReplaced(new String[]{record.getId(), record.getSinkNodeName()}));
					recordsUpdated = insert(record, logger);
					if(recordsUpdated != 1) 
					{
						throw new Exception("Unable to insert a new SAF record with ID " + record.getId());
					}
				}
				else
				{
					throw new Exception("The number of records updated in the store-and-forward queue (" + recordsUpdated
						+ ") didn't match the expected number (1) for transaction " + record.getId());
				}
			}
			else if (recordsUpdated > 1)
			{
				throw new Exception("The number of records updated in the store-and-forward queue (" + recordsUpdated
					+ ") didn't match the expected number (1) for transaction " + record.getId());
			}
			JdbcManager.commit(cn, stmt);
		}
		finally
		{
			JdbcManager.cleanup(cn, stmt);
		}
	}

	/**
	 * Deletes the given record (using the key and sink node name)
	 * 
	 * @throws SQLException
	 */
	public static void deleteSafRecord(SAFQTran record) throws SQLException
	{
		Connection cn = null;
		PreparedStatement stmt = null;
		final String sql = "DELETE FROM fraudnid_persistent_safq WHERE id = ? AND sink_node_name = ?";
		try
		{
			cn = JdbcManager.getRealtimeConnection();
			stmt = cn.prepareStatement(sql);
			stmt.setString(1, record.getId());
			stmt.setString(2, record.getSinkNodeName());
			stmt.executeUpdate();
			JdbcManager.commit(cn, stmt);
		}
		finally
		{
			JdbcManager.cleanup(cn, stmt);
		}
	}

	/**
	 * Retrieve the entry from the SAFQ with the given key and sink node name
	 */
	public static SAFQTran retrieve(String key, String sinkNodeName) throws SQLException 
	{
		Connection cn = null;
		CallableStatement stmt = null;
		ResultSet rs  = null;
		SAFQTran record = null;
		final String sql = "SELECT id, mti, sink_node_name, queue_time, request_time, response_time, response_code, retries_remaining, sent, msg "
			+ "FROM fraudnid_persistent_safq WHERE id = ? AND sink_node_name = ?";
		try
		{
			cn = JdbcManager.getDefaultConnection();
			stmt = cn.prepareCall(sql);
			stmt.setString(1, key);
			stmt.setString(2, sinkNodeName);
			rs = stmt.executeQuery();
			if (rs.next()) 
			{
				record = new SAFQTran();
				record.setId(
					rs.getString(ColumnsStr.ID));
				record.setMsgData(
					rs.getString(ColumnsStr.MSG));
				record.setSinkNodeName(
					rs.getString(ColumnsStr.SINK_NODE_NAME));
				record.setQueueTime(
					rs.getString(ColumnsStr.QUEUE_TIME));
				record.setRetriesRemaining(
					rs.getInt(ColumnsStr.RETRIES_REMAINING));
				record.setSent(
					rs.getBoolean(ColumnsStr.SENT));
				record.setMti(rs.getString(ColumnsStr.MTI));
				record.setRequestTime(rs.getString(ColumnsStr.REQUEST_TIME));
				record.setResponseTime(rs.getString(ColumnsStr.RESPONSE_TIME));
				record.setResponseCode(rs.getString(ColumnsStr.RESPONSE_CODE));
			}
			
			JdbcManager.commit(cn, stmt, rs);
		}
		finally
		{
			JdbcManager.cleanup(cn, stmt, rs);
		}
		
		return record;
	}
	/**
	 * Attempt to retrieve the requested number of records from the SAF table.
	 * Oldest records are fetched first
	 * 
	 * @param sinkNodeName
	 *           The name of the node for which entries should be fetched
	 * @param maxTransToFetch
	 *           Maximum number of transactions to fetch
	 * @param bookmarkId 
	 * @return A list, which may be empty, that will have at most maxTransToFetch
	 *         number of records in it
	 * @throws Exception
	 */
	public static List<SAFQTran> getNextTransFromStoreAndForwardQueue(String sinkNodeName, int maxTransToFetch, Timestamp lastQueueTime, String bookmarkId) 
	throws Exception
	{
		Connection cn = null;
		CallableStatement stmt = null;
		ResultSet rs  = null;
		SAFQTran record = null;
		// Fetch the next transaction
		final String sql = "SELECT TOP " + maxTransToFetch + " id, mti, sink_node_name, queue_time, request_time, response_time, response_code, retries_remaining, sent, msg FROM fraudnid_persistent_safq "
			+ "WHERE sent = 0 AND queue_time >= ISNULL(?, 0) AND id <> ISNULL(?, '') AND retries_remaining > 0 AND sink_node_name = ? ORDER BY queue_time ASC";	
		final List<SAFQTran> result = new ArrayList<SAFQTran>();
		try
		{
			cn = JdbcManager.getDefaultConnection();
			stmt = cn.prepareCall(sql);
			stmt.setTimestamp(1, lastQueueTime);
			stmt.setString(2, bookmarkId);
			stmt.setString(3, sinkNodeName);
			
			rs = stmt.executeQuery();
			while (rs.next()) 
			{
				record = new SAFQTran();
				record.setId(
					rs.getString(ColumnsStr.ID));
				record.setMsgData(
					rs.getString(ColumnsStr.MSG));
				record.setSinkNodeName(
					rs.getString(ColumnsStr.SINK_NODE_NAME));
				record.setQueueTime(
					rs.getTimestamp(ColumnsStr.QUEUE_TIME));
				record.setRetriesRemaining(
					rs.getInt(ColumnsStr.RETRIES_REMAINING));
				record.setSent(
					rs.getBoolean(ColumnsStr.SENT));
				record.setMti(rs.getString(ColumnsStr.MTI));
				record.setRequestTime(rs.getString(ColumnsStr.REQUEST_TIME));
				record.setResponseTime(rs.getString(ColumnsStr.RESPONSE_TIME));
				record.setResponseCode(rs.getString(ColumnsStr.RESPONSE_CODE));
				result.add(record);
			}
			
			JdbcManager.commit(cn, stmt, rs);
		}
		finally
		{
			JdbcManager.cleanup(cn, stmt, rs);
		}
		
		return result;
	}
	
	/*------------------------------------------------------------------------*/
	public static int getSAFQRecordCount(String sinkNodeName) throws Exception
	{
		Connection cn = null;
		CallableStatement stmt = null;
		ResultSet rs  = null;
		int count = -1;
		final String sql = "SELECT COUNT(1) FROM fraudnid_persistent_safq WHERE sent = 0 AND sink_node_name = ? AND retries_remaining > 0";		
		
		try
		{
			cn = JdbcManager.getDefaultConnection();
			stmt = cn.prepareCall(sql);
			stmt.setString(1, sinkNodeName);
			
			rs = stmt.executeQuery();
			if (rs.next()) 
			{
				count = rs.getInt(1);
			}
			
			JdbcManager.commit(cn, stmt, rs);
		}
		finally
		{
			JdbcManager.cleanup(cn, stmt, rs);
		}
		
		return count;
	}
	
	/**
	 * Clean the store and forward table for the given sink node name
	 * 
	 * @param cleanDate
	 *           Deletes records older than this
	 * @return Number of records cleaned
	 * @throws SQLException
	 */
	public static long clean(Date cleanDate, String sinkNode) throws SQLException
	{
		long cleaned = 0;
		
		/* Delete old records that are either: 
		 * 		i) completed (i.e. have a response time and response code).
		 * 							OR
		 * 		ii) have exceeded their retry attempts. 
		 */
		final String sql = 
				"DELETE FROM fraudnid_persistent_safq WHERE sink_node_name = ? "
				+ "AND queue_time < ? AND "
				+ "((response_time IS NOT NULL AND response_code IS NOT NULL) "
				+ "OR retries_remaining = 0)";
		
		Connection cn = null;
		PreparedStatement stmt = null;
		try
		{
			cn = JdbcManager.getRealtimeConnection();
			stmt = cn.prepareStatement(sql);
			stmt.setString(1, sinkNode);
			stmt.setTimestamp(2, new Timestamp(cleanDate.getTime()));
			cleaned = stmt.executeUpdate();
			JdbcManager.commit(cn, stmt);
		}
		finally
		{
			JdbcManager.cleanup(cn, stmt);
		}
		return cleaned;
	}

	/**
	 * Updates the sent time for the reversal message provided
	 */
	public static void updateSendData(Iso8583FMS msg, String sinkNodeName, IMessage msgOut) throws Exception
	{
		final String sql = "UPDATE fraudnid_persistent_safq SET request_time = ?, msgout = ? WHERE id = ? AND sink_node_name = ?";
		Connection cn = null;
		PreparedStatement stmt = null;

		try
		{
			cn = JdbcManager.getRealtimeConnection();
			stmt = cn.prepareStatement(sql);
			stmt.setTimestamp(1, new Timestamp(new java.util.Date().getTime()));
			stmt.setString(2, msgOut.toString());
			stmt.setString(3,  OriginalReqCache.constructKey(msg, 0, null));
			stmt.setString(4, sinkNodeName);
			stmt.executeUpdate();
			JdbcManager.commit(cn, stmt);
		}
		finally
		{
			JdbcManager.cleanup(cn, stmt);
		}
		
	}	
	
	/**
	 * Updates the given transaction's retry remaining count
	 * @param id
	 * @param remaining
	 * @throws SQLException 
	 */
	public static void updateRetriesRemaining(String nodeName, String id, int remaining) throws SQLException 
	{
		final String sql = "UPDATE fraudnid_persistent_safq SET retries_remaining = ? WHERE sink_node_name = ? AND id = ?";
		Connection cn = null;
		PreparedStatement stmt = null;
		
		try
		{
			cn = JdbcManager.getDefaultConnection();
			stmt = cn.prepareStatement(sql);
			stmt.setInt(1, remaining);
			stmt.setString(2, nodeName);
			stmt.setString(3, id);
			stmt.executeUpdate();			
		}
		finally
		{
			JdbcManager.cleanup(cn, stmt);
		}
	}
	
	public static final class ColumnsStr
	{
		public static final String ID = "id";
		public static final String MSG = "msg";
		public static final String SINK_NODE_NAME = "sink_node_name";
		public static final String QUEUE_TIME = "queue_time";
		public static final String RETRIES_REMAINING = "retries_remaining";
		public static final String SENT = "sent";
		public static final String MTI = "mti";
		public static final String REQUEST_TIME = "request_time";
		public static final String RESPONSE_TIME = "response_time";
		public static final String RESPONSE_CODE = "response_code";
	}
	
	public static final class ColumnsInt
	{
		public static final int ID = 1;
		public static final int MSG = 2;
		public static final int SINK_NODE_NAME = 3;
		public static final int QUEUE_TIME = 4;
		public static final int RETRIES_REMAINING = 5;
		public static final int SENT = 6;
	}
}