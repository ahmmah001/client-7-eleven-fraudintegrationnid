/*----------------------------------------------------------------------------*
 *                                                 
 * Copyright (c) 2018 by Montant Limited. All rights reserved.
 *                                                                             
 * This software is the confidential and proprietary property of 
 * Montant Limited and may not be disclosed, copied or distributed 
 * in any form without the express written permission of Montant Limited.
 *----------------------------------------------------------------------------*/

package postilion.realtime.fraudnid.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.Hashtable;

import postilion.realtime.fraudnid.FraudNID;
import postilion.realtime.fraudnid.config.TranFmsData;
import postilion.realtime.sdk.jdbc.JdbcManager;

/**
 * The data access object responsible for all interaction with the 
 * fraudnid_swh_cat_fms_config table from the {@link FraudNID}.
 */
public class TranFmsDataDao
{
	/*------------------------------------------------------------------------*/
	public static void getTranFmsData(
			Hashtable<String, 
			TranFmsData> htTranFmsData) 
	throws Exception
	{
		final String sql = "SELECT id_fms, tran_type, name_interface, card_type, field_account_ref_num, "
			+ "field_amount, service, field_optional_1, field_optional_2, field_optional_3 "
			+ "FROM fraudnid_swh_cat_fms_config";
		Connection cn = null;
		CallableStatement stmt = null;
		ResultSet rs  = null;
		
		try
		{
			cn = JdbcManager.getDefaultConnection();
			stmt = cn.prepareCall(sql);
			rs = stmt.executeQuery();
			
			while (rs.next()) 
			{
				TranFmsData fmsData = new TranFmsData();
				
				fmsData.setIdFms(
						rs.getString(TranFmsDataDao.Columns.ID_FMS));
				fmsData.setTranType(
						rs.getString(TranFmsDataDao.Columns.TRAN_TYPE));
				fmsData.setNameInterface(
						rs.getString(TranFmsDataDao.Columns.NAME_INTERFACE));
				fmsData.setCardType(
						rs.getString(TranFmsDataDao.Columns.CARD_TYPE));
				fmsData.setFieldAccountRefNum(
						rs.getString(TranFmsDataDao.Columns.FIELD_ACC_REF_NO));
				fmsData.setFieldAmount(
						rs.getString(TranFmsDataDao.Columns.FIELD_AMOUNT));
				
				String service = rs.getString(TranFmsDataDao.Columns.SERVICE);
				if (service != null)
				{
					fmsData.setService(service);
				}
				else
				{
					fmsData.setService(FraudNID.EMPTY_STRING);
				}
				
				String optional1 = rs.getString(Columns.FIELD_OPTIONAL_1);
				String optional2 = rs.getString(Columns.FIELD_OPTIONAL_2);
				String optional3 = rs.getString(Columns.FIELD_OPTIONAL_3);
				
				// Optional fields - null means they weren't there so don't set them
				// to empty fields
				fmsData.setOptional1(optional1);
				fmsData.setOptional2(optional2);
				fmsData.setOptional3(optional3);				
				htTranFmsData.put(fmsData.getIdFms(), fmsData);
			}
			
			JdbcManager.commit(cn, stmt, rs);
			
		}
		finally
		{
			JdbcManager.cleanup(cn, stmt, rs);
		}
	}
	
	/*------------------------------------------------------------------------*/
	
	public static final class Columns
	{
		public static final String ID = "id";
		public static final String DESC_TRAN = "desc_tran";
		public static final String ID_FMS = "id_fms";
		public static final String TRAN_TYPE = "tran_type";
		public static final String NAME_INTERFACE = "name_interface";
		public static final String SERVICE = "service";
		public static final String CARD_TYPE = "card_type";
		public static final String FIELD_ACC_REF_NO = "field_account_ref_num";
		public static final String FIELD_AMOUNT = "field_amount";
		public static final String FIELD_OPTIONAL_1 = "field_optional_1";
		public static final String FIELD_OPTIONAL_2 = "field_optional_2";
		public static final String FIELD_OPTIONAL_3 = "field_optional_3";
	}
	
	/*------------------------------------------------------------------------*/
}