/*----------------------------------------------------------------------------*
 *                                                 
 * Copyright (c) 2018 by Montant Limited. All rights reserved.
 *                                                                             
 * This software is the confidential and proprietary property of 
 * Montant Limited and may not be disclosed, copied or distributed 
 * in any form without the express written permission of Montant Limited.
 *----------------------------------------------------------------------------*/

package postilion.realtime.fraudnid.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.Hashtable;

import postilion.realtime.fraudnid.FraudNID;
import postilion.realtime.sdk.jdbc.JdbcManager;

/**
 * The data access object responsible for all interaction with the 
 * fraudnid_tran_filter_config table from the {@link FraudNID}.
 */
public class TranFilterConfigDao
{
	/*------------------------------------------------------------------------*/
	public static void getTranFilterConfig(
			Hashtable<String, Integer> htTranFilterConfig) 
	throws Exception
	{
		Connection cn = null;
		CallableStatement stmt = null;
		ResultSet rs  = null;

		try
		{
			cn = JdbcManager.getDefaultConnection();
			stmt = cn.prepareCall(TranFilterConfigDao.GET_TRAN_FILTER_CONFIG);
			rs = stmt.executeQuery();
			
			while (rs.next()) 
			{
				String tranType = 
						rs.getString(TranFilterConfigDao.Columns.TRAN_TYPE);
				
				String extTranType = FraudNID.EMPTY_STRING;
				String tempExtTranType = 
						rs.getString(TranFilterConfigDao.Columns.EXT_TRAN_TYPE);
				if (!FraudNID.isStrNullOrEmpty(tempExtTranType))
				{
					extTranType = tempExtTranType;
				}

				int interfaceIndex = 
						rs.getInt(TranFilterConfigDao.Columns.INTERFACE_INDEX);

				htTranFilterConfig.put(
						tranType + extTranType, 
						interfaceIndex);
			}

			JdbcManager.commit(cn, stmt, rs);
		}
		finally
		{
			JdbcManager.cleanup(cn, stmt, rs);
		}
	}

	/*------------------------------------------------------------------------*/
	
	public static final String GET_TRAN_FILTER_CONFIG = 
			"SELECT tran_type, ext_tran_type, interface_index " + 
			"FROM fraudnid_tran_filter_config " + 
			"WHERE enable = 1";
	
	public static final class Columns
	{
		public static final String TRAN_TYPE = "tran_type";
		public static final String EXT_TRAN_TYPE = "ext_tran_type";
		public static final String ENABLE = "enable";
		public static final String INTERFACE_INDEX = "interface_index";
	}
	
	/*------------------------------------------------------------------------*/
}