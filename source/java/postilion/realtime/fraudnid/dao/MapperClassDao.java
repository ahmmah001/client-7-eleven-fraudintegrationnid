/*----------------------------------------------------------------------------*
 *                                                 
 * Copyright (c) 2018 by Montant Limited. All rights reserved.
 *                                                                             
 * This software is the confidential and proprietary property of 
 * Montant Limited and may not be disclosed, copied or distributed 
 * in any form without the express written permission of Montant Limited.
 *----------------------------------------------------------------------------*/

package postilion.realtime.fraudnid.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;

import postilion.realtime.fraudnid.FraudNID;
import postilion.realtime.sdk.env.customclasses.CustomClassParameters;
import postilion.realtime.sdk.jdbc.JdbcManager;

/**
 * The data access object responsible for all interaction with the 
 * cfg_custom_classes table from the {@link FraudNID} for retrieving mapper 
 * class information.
 */
public class MapperClassDao
{
	/*------------------------------------------------------------------------*/
	public static String getMapperClassName() throws Exception
	{
		String name = null;
		
		Connection cn = null;
		CallableStatement stmt = null;
		ResultSet rs = null;
		
		try
		{
			cn = JdbcManager.getDefaultConnection();
			stmt = cn.prepareCall(GET_CUSTOM_CLASS_PARAMS);
			rs = stmt.executeQuery();
			
	        if (rs.next())
	        {
	        	CustomClassParameters ccp = 
	        			new CustomClassParameters(rs.getString(1));
		        name = ccp.getParameterValue(FraudNID.MAPPER_CLASS_NAME);
	        }
	        
			JdbcManager.commit(cn, stmt, rs);
		}
		finally
		{
			JdbcManager.cleanup(cn, stmt, rs);
		}

		return name;
	}
	
	/*------------------------------------------------------------------------*/
	
	public static final String GET_CUSTOM_CLASS_PARAMS = 
			"SELECT parameters " 
			+ "FROM cfg_custom_classes WITH (NOLOCK) "
			+ "WHERE unique_name = 'NODE INTEGRATION:FraudNID'";
	
	/*------------------------------------------------------------------------*/
}