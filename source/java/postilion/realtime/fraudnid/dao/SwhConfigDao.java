/*----------------------------------------------------------------------------*\
 *                                                                            *
 * Copyright (c) 2018 by Montant Limited. All rights reserved.                *
 *                                                                            *
 * This software is the confidential and proprietary property of              *
 * Montant Limited and may not be disclosed, copied or distributed            *
 * in any form without the express written permission of Montant Limited.     *
 *                                                                            *
\*----------------------------------------------------------------------------*/
package postilion.realtime.fraudnid.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Hashtable;

import postilion.realtime.sdk.jdbc.JdbcManager;
import postilion.realtime.sdk.util.XPostilion;

/**
 * Retrieves the configuration from global configuration table
 * <code>swh_postilionconfiguracion</code>
 */
public class SwhConfigDao
{
	/**
	 * Returns a table containing the key/value pairs retrieved from the
	 * configuration table
	 * 
	 * @throws XPostilion
	 * 
	 * @throws SQLException
	 */
	public static void getConfiguration(Hashtable<String, String> table, String sinkNode, int company) throws Exception
	{
		Connection cn = null;
		CallableStatement stmt = null;
		ResultSet rs = null;
		try
		{
			table.clear();
			cn = JdbcManager.getDefaultConnection();
			stmt = cn.prepareCall("{call swh_postilionconfiguracion_sel ?,?}");
			stmt.setString(1, sinkNode);
			stmt.setInt(2, company);
			rs = stmt.executeQuery();
			if (rs != null && rs.next())
			{
				table.put(rs.getString(Columns.KEY), rs.getString(Columns.VALUE));
			}
			JdbcManager.commit(cn, stmt, rs);
		}
		finally
		{
			JdbcManager.cleanup(cn, stmt, rs);
		}
	}
	
	protected static class Columns 
	{
		public static final String	KEY	= "nomClave";
		public static final String	VALUE	= "strValor";
	}
}
