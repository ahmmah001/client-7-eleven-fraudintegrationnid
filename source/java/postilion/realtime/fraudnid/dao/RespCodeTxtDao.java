/*----------------------------------------------------------------------------*
 *                                                 
 * Copyright (c) 2018 by Montant Limited. All rights reserved.
 *                                                                             
 * This software is the confidential and proprietary property of 
 * Montant Limited and may not be disclosed, copied or distributed 
 * in any form without the express written permission of Montant Limited.
 *----------------------------------------------------------------------------*/

package postilion.realtime.fraudnid.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.Hashtable;

import postilion.realtime.fraudnid.FraudNID;
import postilion.realtime.sdk.jdbc.JdbcManager;

/**
 * The data access object responsible for all interaction with the 
 * resp_codes_txt table from the {@link FraudNID}.
 */
public class RespCodeTxtDao
{
	/*------------------------------------------------------------------------*/
	public static void getRspCodeMap(
			String sinkNodeName, 
			Hashtable<String, String> htRspCodeMap) 
	throws Exception
	{
		Connection cn = null;
		CallableStatement stmt = null;
		ResultSet rs  = null;
		
		try
		{
			htRspCodeMap.clear();
			cn = JdbcManager.getDefaultConnection();
			
			stmt = cn.prepareCall(RespCodeTxtDao.SP_GET_RESPONSE_CODES);
			stmt.setString(1, sinkNodeName);
			
			rs = stmt.executeQuery();
			while (rs.next()) 
			{
				htRspCodeMap.put(
						rs.getString(RespCodeTxtDao.Columns.RESP_CODE), 
						rs.getString(RespCodeTxtDao.Columns.RESP_TEXT));
			}
			
			JdbcManager.commit(cn, stmt, rs);
			
		}
		finally
		{
			JdbcManager.cleanup(cn, stmt, rs);
		}
	}
	
	/*------------------------------------------------------------------------*/
	
	public static final String SP_GET_RESPONSE_CODES = 
			"{call sp_get_response_codes(?)}";
	
	public static final class Columns
	{
		public static final String NODE_NAME = "node_name";
		public static final String RESP_CODE = "resp_code";
		public static final String RESP_TEXT = "resp_text";
	}
	
	/*------------------------------------------------------------------------*/
}