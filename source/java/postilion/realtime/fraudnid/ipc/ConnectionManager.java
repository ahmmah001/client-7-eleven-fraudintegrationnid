/*----------------------------------------------------------------------------*
 *                                                 
 * Copyright (c) 2018 by Montant Limited. All rights reserved.
 *                                                                             
 * This software is the confidential and proprietary property of 
 * Montant Limited and may not be disclosed, copied or distributed 
 * in any form without the express written permission of Montant Limited.
 *----------------------------------------------------------------------------*/

package postilion.realtime.fraudnid.ipc;

import postilion.realtime.fraudnid.FraudNID;
import postilion.realtime.fraudnid.config.NIDConfig;
import postilion.realtime.fraudnid.dao.StoreAndForwardQueueDao;
import postilion.realtime.fraudnid.eventrecorder.ILogger;
import postilion.realtime.fraudnid.eventrecorder.Logger;
import postilion.realtime.fraudnid.eventrecorder.events.ConnectedToFms;
import postilion.realtime.fraudnid.eventrecorder.events.Disconnected;
import postilion.realtime.fraudnid.msg.Iso8583FMS;
import postilion.realtime.fraudnid.perfmon.PerfMonManager;
import postilion.realtime.sdk.env.App;
import postilion.realtime.sdk.env.SystemProperties;
import postilion.realtime.sdk.eventrecorder.AContext;
import postilion.realtime.sdk.eventrecorder.contexts.EndpointContext;
import postilion.realtime.sdk.eventrecorder.contexts.ExceptionContext;
import postilion.realtime.sdk.ipc.EndpointFilterHeaderStreamPostilion;
import postilion.realtime.sdk.ipc.IClientEndpoint;
import postilion.realtime.sdk.ipc.IEndpoint;
import postilion.realtime.sdk.ipc.Sap;
import postilion.realtime.sdk.ipc.TcpRawClientSap;
import postilion.realtime.sdk.message.bitmap.Iso8583.MsgType;
import postilion.realtime.sdk.node.AIntegrationDriverEnvironment;
import postilion.realtime.sdk.node.IntegrationEvent;
import postilion.realtime.sdk.node.Interchange;
import postilion.realtime.sdk.util.Processor;
import postilion.realtime.sdk.util.Queue;
import postilion.realtime.sdk.util.Timer;

/**
 * The {@link ConnectionManager} class is responsible for interacting with the
 * Fraud Management System on a connectivity and a data transmission level. It
 * manages establishing connections and sending/receiving data and returning
 * responses to the {@link FraudNID}.<br>
 * Connections are permanent - if one is not available, then we keep trying
 * (using a back-off algorithm) until we get connected. <br>
 * Nothing can be sent until a connection is established
 */
public class ConnectionManager extends Processor
{
	private static final String PROP_INITIAL_BACKOFF_MS = "postilion.realtime.fraudnid.ipc.initial_backoff_ms";
	private static final String PROP_MAX_BACKOFF_MS = "postilion.realtime.fraudnid.ipc.max_backoff_ms";
	private static final String PROP_TCP_KEEPALIVE = "postilion.realtime.fraudnid.ipc.tcp_keepalive";
	
	private static final long DEFAULT_INITIAL_BACKOFF_MS = 100;
	private static final long DEFAULT_MAX_BACKOFF_MS = 10000;
	
	/**
	 * Event to ask for a disconnect
	 */
	protected static class DisconnectRequest
	{
		// Empty
	}
	
	/**
	 * A timer object to trigger connecting (used as part of the backoff mechanism)
	 */
	protected static class ConnectTimer
	{
		// Empty
	}
	protected AIntegrationDriverEnvironment	env						= null;
	protected FraudNID								nidInstance				= null;
	protected NIDConfig								config					= null;
	protected ILogger									logger					= null;
	// This is the queue that we'll use to send replies back to the NID
	protected Queue									appQueue					= null;
	protected Sap										sap						= null;
	protected IClientEndpoint						endpoint					= null;
	protected boolean									isConnected				= false;
	protected String									sinkNodeName			= null;
	protected final Object							connectionLock 		= new Object();
	protected BackoffPolicy							backOffPolicy			= null;	
	protected Timer 									connectionTimer		= null;
	
	/*------------------------------------------------------------------------*/
	/**
	 * Sets up the manager with all the information/access it requires, creates 
	 * the required {@link TcpRawClientSap} and starts the SAP and itself as it 
	 * is an extension of a {@link Processor}.
	 * 
	 * @param env
	 * 			The {@link AIntegrationDriverEnvironment} to associate to the 
	 * 			manager.
	 * 
	 * @param nidInstance
	 * 			The {@link FraudNID} instance to associate to the manager. This 
	 * 			is required for returning {@link IntegrationEvent 
	 * 			IntegrationEvents}, containing responses from the FMS, to the 
	 * 			App/Interchange's queue and to the {@link FraudNID}.
	 * 
	 * @param config
	 * 			The {@link NIDConfig} to reference when setting up the manager.
	 * 
	 * @param appQueue
	 * 			THe {@link App}/{@link Interchange} queue used for interacting 
	 * 			with the {@link FraudNID}.
	 * 
	 * @param logger
	 * 			The {@link Logger} to use for interacting with the Realtime 
	 * 			Support Event Framework.
	 * 
	 * @throws Exception
	 */
	public ConnectionManager(
			AIntegrationDriverEnvironment env, 
			FraudNID nidInstance, 
			NIDConfig config, 
			Queue appQueue,
			String sinkNodeName, 
			ILogger logger)
	throws Exception
	{
		this.env = env;
		this.nidInstance =  nidInstance;
		this.config = config;
		this.appQueue = appQueue;
		this.logger = logger;
		this.sinkNodeName = sinkNodeName;
		putQueue(getNewProcessorQueue());
		// Default backoff policy - backoff between .5 and 15 seconds when trying to connect
		backOffPolicy = getBackoffPolicy();
	}
	
	/**
	 * Starts the connection timer (when this timer fires, we'll create a
	 * connection) using the backoff manager to manage cadence
	 */
	protected void startConnectionTimer()
	{
		connectionTimer = new Timer(
			getQueue(), 
			backOffPolicy.nextBackoffMillis(), 
			new ConnectTimer());		
	}

	/**
	 * Stop the connection timer (if there is one)
	 */
	protected void stopConnectionTimer()
	{
		if(connectionTimer != null)
		{
			connectionTimer.stop();
		}
	}

	/**
	 * Creates a new back off policy using configured system properties. Default
	 * values are used if nothing is configured.
	 * 
	 * @return A usable backoff policy
	 */
	protected BackoffPolicy getBackoffPolicy()
	{
		return new BackoffPolicy()
			.setInitialBackoffMillis(SystemProperties.getAsLong(PROP_INITIAL_BACKOFF_MS, DEFAULT_INITIAL_BACKOFF_MS))
			.setMaxBackoffMillis(SystemProperties.getAsLong(PROP_MAX_BACKOFF_MS, DEFAULT_MAX_BACKOFF_MS));
	}
	
	/*------------------------------------------------------------------------*/
	/**
	 * Initializes the {@link ConnectionManager} by creating and starting a
	 * {@link TcpRawClientSap} and starting our processor thread. The first
	 * connection request is also created
	 */
	public void init()
	{
		// Create a new SAP.
		this.sap = new TcpRawClientSap(
				"FraudNID Client Connection on " + sinkNodeName, 
				getQueue(), 
				null,
				new Class[] {EndpointFilterHeaderStreamPostilion.class},
				// Connect from any local address
				"0.0.0.0",
				// Connect using next available local port
				0,
				true, // async
				shouldEnableTcpKeepAlive()); // enable tcp keepalives
		
		// Start the SAP.
		this.sap.start();
		
		// Start this processor.
		this.start();
		
		// We will definitely want to start connecting to the remote entity
		// Start a connection timer
		startConnectionTimer();
	}
	
	/**
	 * @return true if TCP keep alive processing should be enabled. The default
	 *         is true if the setting not configured
	 */
	protected boolean shouldEnableTcpKeepAlive()
	{
		return SystemProperties.getAsBoolean(PROP_TCP_KEEPALIVE, true);
	}

	/*------------------------------------------------------------------------*/
	/**
	 * Updates the {@link NIDConfig} associated with the {@link 
	 * ConnectionManager}. If either the Remote IP or Remote Port has changed, 
	 * the {@link IClientEndpoint} will be closed so that a new one, with the 
	 * latest configuration, can be re-established during the next 
	 * connect-on-demand attempt.
	 * 
	 * @param config
	 * 			The {@link NIDConfig} to load into the instance.
	 * 
	 * @throws Exception
	 */
	public void updateConfig(NIDConfig config) throws Exception
	{
		boolean ipChanged = false;
		if (!this.config.remoteIP.equals(config.remoteIP))
		{
			ipChanged = true;
		}
		
		boolean portChanged = false;
		if (this.config.remotePort != config.remotePort)
		{
			portChanged = true;
		}
		
		// Save the config
		this.config = config;
		
		if (ipChanged || portChanged)
		{
			// Queue a disconnect
			getQueue().append(new DisconnectRequest());
		}
	}
	
	/*------------------------------------------------------------------------*/
	@Override
	protected boolean processEvent(Object event)
	{
		if (event instanceof Sap.DataEvent)
		{
			return processDataEvent((Sap.DataEvent) event);
		}
		else if (event instanceof Sap.ConnectEvent)
		{
			return processConnectedEvent((Sap.ConnectEvent)event);
		}
		else if (event instanceof Sap.DisconnectEvent)
		{
			// Disconnected from fraud system - either because a connection was closed
			// or a connection could not be established
			return processDisconnectedEvent((Sap.DisconnectEvent) event);
		}
		else if (event instanceof Timer.Event) 
		{
			Timer.Event timerEvent = (Timer.Event) event;
			if(timerEvent.user_ref instanceof ConnectTimer)
			{
				// The time has come to connect
				connect();
			}
		}
		else if(event instanceof DisconnectRequest)
		{
			if(sap != null)
			{
				sap.disconnectAll();
			}
		}
		
		return false;
	}

	/**
	 * Processes a disconnection event received from the SAP to the FMS. Resets
	 * the internal status of the connection, and then tries to connect again
	 * 
	 * @param dcEvent
	 *           The disconnection event
	 * @return true if the event was processed (it always is)
	 */
	protected boolean processDisconnectedEvent(Sap.DisconnectEvent dcEvent)
	{
		synchronized(connectionLock)
		{
			// Always set connection state first (consistency of state is key)
			isConnected = false;
		}
		
		try
		{
			if (dcEvent.exception == null)
			{
				// Standard disconnect.
				logger.recordEvent(new Disconnected(new AContext[]{new EndpointContext(dcEvent.endpoint)},
					new String[]{"the FMS has disconnected from the NID."}));
			}
			else
			{
				// Unexpected disconnect/host unavailable.
				logger.recordEvent(new Disconnected(
					new AContext[]{new ExceptionContext(dcEvent.exception), new EndpointContext(dcEvent.endpoint)},
					new String[]{"an exception occurred"}));
			}
		}
		finally
		{
			resetEndpoint();
			// Always try to connect again
			startConnectionTimer();
		}			
		return true;
	}

	/**
	 * Processing for a connection event to FMS. Tracks the connection status
	 * internally, and then notifies the NID that the connection occurred
	 * 
	 * @param connectEvent
	 * @return
	 */
	protected boolean processConnectedEvent(Sap.ConnectEvent connectEvent)
	{
		// This means we're connected to the fraud system
		synchronized (connectionLock)
		{
			// Always do this first - consistency of state is key
			isConnected = true;
			// Reset the backoff policy so any subsequent connections start more quickly
			backOffPolicy = getBackoffPolicy();
		}
		stopConnectionTimer();
		// Let the NID know that the fraud system has connected so that it can
		// take action, such as sending any pending SAF entries
		appQueue.append(new IntegrationEvent(nidInstance, connectEvent));
		logger.recordEvent(
			new ConnectedToFms(
				new AContext[] {
						new EndpointContext(connectEvent.endpoint)}));
		return true;
	}

	/**
	 * Processing for a data event (i.e., a reply message from the FMS system).
	 * Sends the message to the NID after incrementing counters
	 * 
	 * @param dataEvent
	 * @return
	 */
	protected boolean processDataEvent(Sap.DataEvent dataEvent)
	{
		// A response message from the fraud system
		try
		{
			Iso8583FMS rsp = new Iso8583FMS();
			rsp.fromMsg(dataEvent.data);
			PerfMonManager.incrementMessagesReceivedPerSecond(env.getSinkNodeName());
			enqueueMsgToNID(rsp, dataEvent.endpoint);
		}
		catch(Exception e)
		{
			logger
				.recordEvent(FraudNID.getApplicationException("An error occurred unpacking a response from the FMS. ", e));
		}
		return true;
	}

	/*------------------------------------------------------------------------*/
	/**
	 * Creates a new connection to the FMS (unless already connected, in which case it does nothing) 
	 * <br>
	 * Given that the only way to make a connection is via the {@link ConnectRequest} event in the queue, 
	 * and nobody else has access to that event, we don't need to track whether or not a connection is in 
	 * progress
	 */
	protected void connect() 
	{
		if(isConnected())
		{
			logger.debug("Not trying to connect - already connected");
			return;
		}

		logger.debug("Attempting to connect to FMS");
		synchronized (connectionLock)
		{
			endpoint = 
				((TcpRawClientSap) this.sap).getEndpoint(
											config.remoteIP, 
											config.remotePort);
			endpoint.connect();
		}
	}
	
	/**
	 * Disconnect the current connection, if any. Also resets the endpoint object
	 * to null
	 */
	protected void resetEndpoint()
	{
		synchronized(connectionLock)
		{
			if(endpoint != null)
			{
				endpoint.close();
				endpoint = null;
			}
		}
	}
	
	/*------------------------------------------------------------------------*/
	/**
	 * Sends the given message data to the FMS. If no connection to the FMS is
	 * immediately available, then the message is not sent
	 * 
	 * @param fmsMsg
	 *           The {@link Iso8583FMS} message to send to the FMS.
	 * 
	 * @throws Exception
	 */
	public void send(Iso8583FMS fmsMsg) throws Exception
	{
		if(!isConnected())
		{
			return;
		}
		
		byte[] data = fmsMsg.toMsg();
			
		endpoint.send(data);
		
		// If this is a reversal then it emanated from the store-and-forward queue
		// - update its sent timestamp
		switch (fmsMsg.getMsgType())
		{
		case MsgType._0420_ACQUIRER_REV_ADV :
		case MsgType._0421_ACQUIRER_REV_ADV_REP :
			StoreAndForwardQueueDao.updateSendData(fmsMsg, sinkNodeName, fmsMsg);
			break;
		default :
			break;
		}
		// Only log  after updating the status to avoid any gap between the update and the send
		logger.logRequest(fmsMsg, endpoint);
		
		PerfMonManager.incrementMessagesSentPerSecond(env.getSinkNodeName());
	}
	
	/*------------------------------------------------------------------------*/
	/**
	 * Enqueues the given {@link Iso8583FMS} message, typically a response, 
	 * onto the {@link App}/{@link Interchange} queue associated to the {@link 
	 * FraudNID}.
	 * 
	 * @param msgFromFMS
	 * 			The {@link Iso8583FMS} message to direct to the {@link 
	 * 			FraudNID}.
	 * @param dataEndpoint The endpoint upon which the data was received
	 * 
	 * @throws Exception
	 */
	protected void enqueueMsgToNID(Iso8583FMS msgFromFMS, IEndpoint dataEndpoint) throws Exception
	{		
		logger.logResponse(msgFromFMS, dataEndpoint);
		
		appQueue.append(new IntegrationEvent(nidInstance, msgFromFMS));
	}

	/**
	 * @return true if connected to the fraud system
	 */
	public boolean isConnected() 
	{
		synchronized(connectionLock)
		{
			return isConnected == true;
		}
	}
}