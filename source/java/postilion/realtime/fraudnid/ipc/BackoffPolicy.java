/*----------------------------------------------------------------------------*
 *
 * Copyright (c) 2020 by Montant Limited. All rights reserved.
 *
 * This software is the confidential and proprietary property of
 * Montant Limited and may not be disclosed, copied or distributed
 * in any form without the express written permission of Montant Limited.
 *
 *----------------------------------------------------------------------------*/
package postilion.realtime.fraudnid.ipc;

import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * Implements an exponential back off policy to allow polite retries. Inspired
 * by <a href=
 * "https://github.com/grpc/grpc-java/blob/master/core/src/main/java/io/grpc/internal/ExponentialBackoffPolicy.java">Google</a>)<br>
 * This version deals in milliseconds instead of nanoseconds<br>
 * Each invocation of {@link #nextBackoffMillis()} returns a new interval to
 * wait that applies the policy for ever-increasing back-off until it is reset
 */
public final class BackoffPolicy
{
	private Random	random					= new Random();
	private long	initialBackoffMillis	= TimeUnit.SECONDS.toMillis(1);
	private long	maxBackoffMillis		= TimeUnit.SECONDS.toMillis(5);
	private double	multiplier				= 1.6;
	private double	jitter					= .2;
	private long	nextBackoffMillis		= initialBackoffMillis;
	/**
	 * @return The next millisecond period to use when backing off
	 */
	public long nextBackoffMillis()
	{
		long currentBackoffMillis = nextBackoffMillis;
		nextBackoffMillis = Math.min((long)(currentBackoffMillis * multiplier), maxBackoffMillis);
		long result = currentBackoffMillis + uniformRandom(-jitter * currentBackoffMillis, jitter * currentBackoffMillis);
		// Make sure that the next one is on a 10ms boundary to avoid clock drift in Windows
		// Rather safe than sorry
		return result += 10 - result % 10;
	}

	private long uniformRandom(double low, double high)
	{
		double mag = high - low;
		return (long)(random.nextDouble() * mag + low);
	}

	public BackoffPolicy setRandom(Random random)
	{
		this.random = random;
		return this;
	}

	public BackoffPolicy setInitialBackoffMillis(long initialBackoffMillis)
	{
		this.initialBackoffMillis = initialBackoffMillis;
		return this;
	}

	public BackoffPolicy setMaxBackoffMillis(long maxBackoffMillis)
	{
		this.maxBackoffMillis = maxBackoffMillis;
		return this;
	}

	public BackoffPolicy setMultiplier(double multiplier)
	{
		this.multiplier = multiplier;
		return this;
	}

	public BackoffPolicy setJitter(double jitter)
	{
		this.jitter = jitter;
		return this;
	}
}
