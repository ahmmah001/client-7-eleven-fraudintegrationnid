/*----------------------------------------------------------------------------*
 *                                                 
 * Copyright (c) 2018 by Montant Limited. All rights reserved.
 *                                                                             
 * This software is the confidential and proprietary property of 
 * Montant Limited and may not be disclosed, copied or distributed 
 * in any form without the express written permission of Montant Limited.
 *----------------------------------------------------------------------------*/
package postilion.realtime.fraudnid.cache;

import java.util.Arrays;
import java.util.Hashtable;

import postilion.realtime.fraudnid.FraudNID;
import postilion.realtime.fraudnid.config.TranFmsData;
import postilion.realtime.fraudnid.msg.Iso8583FMS;
import postilion.realtime.sdk.message.bitmap.Iso8583;
import postilion.realtime.sdk.message.bitmap.Iso8583Post;
import postilion.realtime.sdk.message.bitmap.StructuredData;
import postilion.realtime.sdk.util.TimedHashtable;

/**
 * Wrapper class for the cache used to store original transaction requests from
 * Transaction manager against a key which can be constructed from responses
 * from the Fraud Management System, allowing transaction linking etc..
 */
public class OriginalReqCache
	extends
	TimedHashtable
{
	/*------------------------------------------------------------------------*/
	public OriginalReqCache(long expiryTime)
	{
		super(expiryTime);
	}

	/*------------------------------------------------------------------------*/
	/**
	 * Constructs a {@link String} "key" from the given message consisting of the
	 * Retrieval Reference Number/Master Transaction ID (Field 37) and the
	 * Interface Name (FMS Configuration/Field 108).
	 * 
	 * @param msg
	 *           The {@link Iso8583Post}/{@link Iso8583FMS} message to evaluate.
	 * 
	 * @param interfaceIndex
	 *           The interface index to use when retrieving the interface name
	 *           from the FMS configuration.
	 * 
	 * @param htTranFmsData
	 *           The {@link Hashtable} containing the FMS data loaded from the
	 *           database.
	 * 
	 * @return The "key" used for transaction linking/reference/retrieval.
	 * @throws Exception 
	 */
	public static String constructKey(Iso8583 msg, int interfaceIndex, Hashtable<String, TranFmsData> htTranFmsData)
		throws Exception
	{
		if (msg == null)
		{
			return null;
		}
		String interfaceName = FraudNID.EMPTY_STRING;
		// Attempt to retrieve the interface name from the fraud response.
		if (msg instanceof Iso8583FMS)
		{
			if (msg.isFieldSet(Iso8583FMS.Bit._108_INTERFACE_NAME))
			{
				interfaceName = ((Iso8583FMS)msg).getField(Iso8583FMS.Bit._108_INTERFACE_NAME);
			}
		}
		// Else, retrieve the interface name from the transaction's TranFmsData.
		else if (msg instanceof Iso8583Post)
		{
			StructuredData sd = ((Iso8583Post)msg).getStructuredData();
			if (sd != null)
			{
				String fmsConfigID = sd.get(FMS_ID_FIELD_9998);
				if (fmsConfigID != null)
				{
					TranFmsData tranFmsData = htTranFmsData.get(fmsConfigID);
					if (tranFmsData != null)
					{
						interfaceName = tranFmsData.getNameInterface();
						if (interfaceName != null)
						{
							if (interfaceName.contains(PIPE_DELIM))
							{
								String[] interfaces = interfaceName.split(PIPE_DELIM_REGEX);
								if (interfaces.length > interfaceIndex)
								{
									interfaceName = interfaces[interfaceIndex];
								}
								else
								{
									throw new Exception("No interface name with index " + interfaceIndex
										+ " exists in FMS config " + fmsConfigID + ": " + Arrays.toString(interfaces));
								}
							}
						}
					}
				}
			}
		}
		String key = new StringBuilder(msg.getField(Iso8583Post.Bit._037_RETRIEVAL_REF_NR)).append(interfaceName).toString();
		// Construct & return the "key".
		
		return key;
	}
	/*------------------------------------------------------------------------*/
	public static final String	FMS_ID_FIELD_9998	= "FIELD_9998";
	public static final String	PIPE_DELIM_REGEX	= "\\|";
	public static final String PIPE_DELIM = "|";
	/*------------------------------------------------------------------------*/
}