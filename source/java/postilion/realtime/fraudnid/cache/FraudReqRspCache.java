/*----------------------------------------------------------------------------*
 *                                                 
 * Copyright (c) 2018 by Montant Limited. All rights reserved.
 *                                                                             
 * This software is the confidential and proprietary property of 
 * Montant Limited and may not be disclosed, copied or distributed 
 * in any form without the express written permission of Montant Limited.
 *----------------------------------------------------------------------------*/

package postilion.realtime.fraudnid.cache;

import java.sql.SQLException;
import postilion.realtime.fraudnid.msg.Iso8583FMS;
import postilion.realtime.sdk.util.TimedHashtable;

/**
 * Wrapper class, backed by a {@link TimedHashtable} for the cache used to 
 * store fraud requests and responses against the Switch Key (Field 127.2) 
 * from the original request from Transaction Manager, allowing transaction 
 * linking etc..
 */
public class FraudReqRspCache 
{
	protected TimedHashtable timedHt = null;
	
	/*------------------------------------------------------------------------*/
	/**
	 * Default Constructor.
	 * 
	 * @param expiryTime
	 * 			The time in milliseconds after which an entry in the cache will 
	 * 			expire.
	 * 
	 * @throws SQLException
	 */
	public FraudReqRspCache(long expiryTime)
	throws SQLException
	{
		timedHt = new TimedHashtable(expiryTime);
	}
	
	/*------------------------------------------------------------------------*/
	/**
	 * This will put a value in the cache. The value is referenced by the key 
	 * and will expire after the default expiry time associated with this cache 
	 * has elapsed. This method may also be used to update either the fraud 
	 * request or the fraud response individually in the backing table.
	 * 
	 * @param origTranSwitchKey
	 * 			The Switch Key (Field 127.2) from the original request to use 
	 * 			as the key in the backing {@link TimedHashtable}.
	 * 
	 * @param fraudReqMsg
	 * 			The fraud request {@Iso8583FMS} message to store.
	 * 
	 * @param fraudRspMsg
	 * 			The fraud response {@Iso8583FMS} message to store.
	 * 
	 * @return
	 * 			The value previously referenced by the key, null if the key was 
	 * 			not found.
	 */
	public Object put(
			String origTranSwitchKey, 
			Iso8583FMS fraudReqMsg, 
			Iso8583FMS fraudRspMsg)
	{
		FraudMsgContainer fraudReqRsp = null;
		
		fraudReqRsp = (FraudMsgContainer) timedHt.get(origTranSwitchKey);
		if (fraudReqRsp == null)
		{
			fraudReqRsp = new FraudMsgContainer();
		}
		
		if (fraudReqMsg != null)
		{
			fraudReqRsp.fraudReq = fraudReqMsg;
		}
		
		if (fraudRspMsg != null)
		{
			fraudReqRsp.fraudRsp = fraudRspMsg;
		}
		
		return timedHt.put(origTranSwitchKey, fraudReqRsp);
	}
	
	/*------------------------------------------------------------------------*/
	/**
	 * Retrieves the fraud request mapped to the given key, if it exists.
	 * 
	 * @param origTranSwitchKey
	 * 			The key to which the fraud request is mapped.
	 * 
	 * @return
	 * 			The fraud request {@link Iso8583FMS} message.
	 */
	public Iso8583FMS getFraudReq(String origTranSwitchKey)
	{
		FraudMsgContainer fraudReqRsp = 
				(FraudMsgContainer) timedHt.get(origTranSwitchKey);
		if (fraudReqRsp != null)
		{
			return fraudReqRsp.fraudReq;
		}

		return null;
	}
	
	/*------------------------------------------------------------------------*/
	/**
	 * Retrieves the fraud response mapped to the given key, if it exists.
	 * 
	 * @param origTranSwitchKey
	 * 			The key to which the fraud response is mapped.
	 * 
	 * @return
	 * 			The fraud response {@link Iso8583FMS} message.
	 */
	public Iso8583FMS getFraudRsp(String origTranSwitchKey)
	{
		FraudMsgContainer fraudReqRsp = 
				(FraudMsgContainer) timedHt.get(origTranSwitchKey);
		if (fraudReqRsp != null)
		{
			return fraudReqRsp.fraudRsp;
		}

		return null;
	}
	
	/*------------------------------------------------------------------------*/
	/**
	 * Checks whether this cache contains a given key.
	 * 
	 * @param key
	 * 			A possible key.
	 * 
	 * @return 
	 * 			Whether the key was found or not.														
	 */
	public boolean containsKey(Object key)
	{
		return timedHt.containsKey(key);
	}
	
	/*------------------------------------------------------------------------*/
	/**
	 * Container class for storing fraud requests-response pairs.
	 */
	public static class FraudMsgContainer
	{
		public Iso8583FMS fraudReq = null;
		public Iso8583FMS fraudRsp = null;
		
		/*--------------------------------------------------------------------*/
		public FraudMsgContainer()
		{
		}
	}
	/*------------------------------------------------------------------------*/
}