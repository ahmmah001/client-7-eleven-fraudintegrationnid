/*----------------------------------------------------------------------------*
 *                                                 
 * Copyright (c) 2018 by Montant Limited. All rights reserved.
 *                                                                             
 * This software is the confidential and proprietary property of 
 * Montant Limited and may not be disclosed, copied or distributed 
 * in any form without the express written permission of Montant Limited.
 *----------------------------------------------------------------------------*/
package postilion.realtime.fraudnid;

import java.sql.Timestamp;
import java.util.Hashtable;

import postilion.realtime.fraudnid.cache.FraudReqRspCache;
import postilion.realtime.fraudnid.cache.OriginalReqCache;
import postilion.realtime.fraudnid.config.NIDConfig;
import postilion.realtime.fraudnid.config.TranFmsData;
import postilion.realtime.fraudnid.dao.MapperClassDao;
import postilion.realtime.fraudnid.dao.StoreAndForwardQueueDao;
import postilion.realtime.fraudnid.eventrecorder.EventTranInfo;
import postilion.realtime.fraudnid.eventrecorder.ILogger;
import postilion.realtime.fraudnid.eventrecorder.Logger;
import postilion.realtime.fraudnid.eventrecorder.events.ApplicationError;
import postilion.realtime.fraudnid.eventrecorder.events.ApplicationException;
import postilion.realtime.fraudnid.eventrecorder.events.ApplicationInfo;
import postilion.realtime.fraudnid.eventrecorder.events.ApplicationWarning;
import postilion.realtime.fraudnid.eventrecorder.events.DuplicateMessageDetected;
import postilion.realtime.fraudnid.eventrecorder.events.Initialized;
import postilion.realtime.fraudnid.eventrecorder.events.RequestTimeout;
import postilion.realtime.fraudnid.ipc.ConnectionManager;
import postilion.realtime.fraudnid.msg.IMapper;
import postilion.realtime.fraudnid.msg.Iso8583FMS;
import postilion.realtime.fraudnid.perfmon.PerfMonManager;
import postilion.realtime.fraudnid.safq.Cleaner;
import postilion.realtime.fraudnid.safq.StoreAndForwardQueue;
import postilion.realtime.fraudnid.util.DuplicateChecker;
import postilion.realtime.fraudnid.util.StanchionTimer;
import postilion.realtime.fraudnid.util.TimerManager;
import postilion.realtime.fraudnid.util.TimerManager.TimerUser;
import postilion.realtime.sdk.env.App;
import postilion.realtime.sdk.eventrecorder.AContext;
import postilion.realtime.sdk.eventrecorder.AEventRecord;
import postilion.realtime.sdk.eventrecorder.contexts.ExceptionContext;
import postilion.realtime.sdk.ipc.Sap;
import postilion.realtime.sdk.message.bitmap.Iso8583;
import postilion.realtime.sdk.message.bitmap.Iso8583.RspCode;
import postilion.realtime.sdk.message.bitmap.Iso8583Post;
import postilion.realtime.sdk.message.bitmap.Iso8583Post.PrivBit;
import postilion.realtime.sdk.message.bitmap.Iso8583Rev93.Bit;
import postilion.realtime.sdk.message.bitmap.ProcessingCode;
import postilion.realtime.sdk.node.AIntegrationDriver;
import postilion.realtime.sdk.node.AIntegrationDriverEnvironment;
import postilion.realtime.sdk.node.Action;
import postilion.realtime.sdk.node.IntegrationEvent;
import postilion.realtime.sdk.util.Queue;
import postilion.realtime.sdk.util.TimedHashtable;
import postilion.realtime.sdk.util.XPostilion;

/**
 * <p>
 * The Fraud Integration Node Integration Driver is a NID designed to intercept
 * transactions from Transaction Manager destined for a remote entity. Upon
 * receipt of a qualifying transaction, a fraud request message will be
 * generated and sent to the Fraud Management System, directly from within the
 * NID, for processing. If the fraud check passes, the original transaction will
 * be forwarded to the sink, if not, the NID will decline the original
 * transaction and return it to Transaction Manager.
 * </p>
 * <p>
 * This NID is also responsible for managing reversal messages to the Fraud
 * Management System. Reversals will be generated and sent if the sink entity
 * declines a transaction for which a fraud check was performed or if the source
 * entity reverses a previous transaction. These transactions are served by a
 * Store & Forward Queue integrated into the NID.
 * </p>
 * <p>
 * The implementation includes a "versioned" class approach to manage message
 * construction/processing/mapping. Each change/update/fix will/should be placed
 * in a new Mapper_v# class, allowing the customer to load the mapper class, via
 * the RELOADMAPPER command, without restarting the service associated to the
 * NID. See {@link IMapper} for more information.
 * </p>
 */
public class FraudNID
	extends
	AIntegrationDriver
	implements
	TimerUser
{
	protected static final String					APPROVED						= "00";
	protected static final int						FRAUD_SINK_COMPANY_NAME	= 1;
	protected AIntegrationDriverEnvironment	env							= null;
	protected Queue									appQueue						= null;
	protected NIDConfig								config						= null;
	protected IMapper									mapper						= null;
	protected ConnectionManager					connManager					= null;
	protected FmsMsgQueue							fmsQueue						= null;
	protected ILogger									logger						= null;
	protected OriginalReqCache						origReqCache				= null;
	protected FraudReqRspCache						fraudReqRspCache			= null;
	protected StoreAndForwardQueue				storeAndForwardQueue		= null;
	protected TimedHashtable						timedOutTrans				= new TimedHashtable(300 * 1000);
	protected Cleaner									cleaner						= null;

	/*------------------------------------------------------------------------*/
	/**
	 * Initialize this node integration driver.
	 *
	 * @param environment
	 *           Details of the node application/environment associated with this
	 *           integration driver.
	 * 
	 * @param integrationDriverParameters
	 *           Any user parameters configured for this integration driver.
	 * 
	 * @param customClassParameters
	 *           Any custom class parameters configured for this class.
	 * 
	 * @throws Exception
	 */
	@Override
	public void init(AIntegrationDriverEnvironment environment, String integrationDriverParameters,
		String customClassParameters)
		throws Exception
	{
		// Store a reference to the NID environment.
		this.env = environment;
		// Initialize a new PerfMon instance.
		PerfMonManager.initialize(environment.getSinkNodeName());
		// Retrieve a new Logger instance.
		this.logger = new Logger(this.env);
		TimerManager.INSTANCE.setLogger(logger);
		// Retrieve the main Queue associated with this NID's Interchange.
		this.appQueue = App.getQueue();
		// Load the user and database configuration.
		getUserAndDatabaseConfig(integrationDriverParameters, customClassParameters);
		// Check if the configuration could be loaded.
		if (config == null)
		{
			throw new XPostilion(new ApplicationError(
				new String[]{"An error occurred while trying to load the " + "user/database configuration."}));
		}
		// Retrieve and initialize the mapper.
		getMapper(this.config.mapperClassName);
		// Create and initialize a new Connection Manager.
		this.connManager = new ConnectionManager(this.env, this, this.config, this.appQueue,
			environment.getSinkNodeName(), this.logger);
		this.connManager.init();
		// Create and initialize a new FMS Request Queue/Processor.
		this.fmsQueue = new FmsMsgQueue(connManager, logger);
		this.fmsQueue.init();
		// Initialize transaction caches.
		origReqCache = new OriginalReqCache(config.origTranCacheTimeout);
		fraudReqRspCache = new FraudReqRspCache(config.fraudReqRspCacheTimeout);
		// Initialize (doesn't start) the SAFQ.
		this.storeAndForwardQueue = new StoreAndForwardQueue(this.env, this.config, this.logger);
		startSafQpollTimer();
		PerfMonManager.setStoreAndForwardQueueDepth(environment.getSinkNodeName(), getSafQueueLength());
		// Create a SAFQ cleaner if one is required
		if (cleaner == null)
		{
			// Cleaner period is in seconds in parameters, but cleaner uses
			// milliseconds
			cleaner = new Cleaner(config.safqCleanerPeriod * 1000, environment.getSinkNodeName(), logger,
				config.safqRetentionPeriod);
			Thread cleanerThread = new Thread(cleaner);
			// Don't let the thread keep the VM alive
			cleanerThread.setDaemon(false);
			cleanerThread.start();
		}
		// Log an event if everything was successful.
		this.logger.recordEvent(new Initialized(new String[]{config.toString()}));
	}

	/**
	 * @return Store and forward queue timeout in milliseconds
	 */
	public long getSafQtimeout()
	{
		return config.safqTimeout;
	}

	/**
	 * Return the current store and forward queue length
	 * 
	 * @return
	 * @throws Exception
	 */
	protected long getSafQueueLength() throws Exception
	{
		return storeAndForwardQueue.getLength();
	}

	/*------------------------------------------------------------------------*/
	/**
	 * Retrieves, parses and validates the necessary configuration from the given
	 * user parameters, custom class parameters and the database.
	 * 
	 * @param integrationDriverParameters
	 *           Any user parameters configured for this integration driver.
	 * 
	 * @param customClassParameters
	 *           Any custom class parameters configured for this class.
	 * 
	 * @throws Exception
	 */
	protected void getUserAndDatabaseConfig(String integrationDriverParameters, String customClassParameters)
		throws Exception
	{
		try
		{
			// Load the user configuration.
			config = new NIDConfig(integrationDriverParameters, customClassParameters);
			config.load();
		}
		catch(Exception e)
		{
			logger.recordEvent(
				getApplicationException("An error occurred while trying to load the " + "user/database configuration.", e));
			throw e;
		}
	}

	/*------------------------------------------------------------------------*/
	/**
	 * Attempts to instantiate and initialize the given {@link IMapper} class
	 * (name).
	 * 
	 * @param mapperClassName
	 *           The mapper class name to load, configured in the Custom Class
	 *           Parameters for this component.
	 * 
	 * @return <code>True</code> if the {@link IMapper} class was loaded
	 *         successfully, else <code>false</code>.
	 * 
	 * @throws Exception
	 */
	protected boolean getMapper(String mapperClassName) throws Exception
	{
		boolean result = false;
		if (isStrNullOrEmpty(mapperClassName))
		{
			logger.recordEvent(new ApplicationError(
				new String[]{"Invalid mapper class name configured: '" + config.mapperClassName + "'"}));
			return false;
		}
		IMapper newMapper = null;
		try
		{
			Class<?> clazz = Class.forName(mapperClassName);
			newMapper = (IMapper)clazz.newInstance();
			if (newMapper != null)
			{
				config.mapperClassName = mapperClassName;
				mapper = newMapper;
				mapper.init(config);
				result = true;
			}
		}
		catch(Exception e)
		{
			throw new XPostilion(getApplicationException(
				"The given mapper class could not be loaded. Class name: '" + mapperClassName + "'", e));
		}
		return result;
	}

	/*------------------------------------------------------------------------*/
	/**
	 * Processes a <code>RESYNC</code> command. This reloads the user parameters
	 * and database configuration, and re-initializes the {@link IMapper} and
	 * {@link ConnectionManager} (if new connection parameters have been
	 * specified). A <code>RESYNC</code> does NOT load a new mapper class.
	 *
	 * @param nodeApplication
	 *           Details of the node application associated with this integration
	 *           driver.
	 * 
	 * @param integrationDriverParameters
	 *           Any user parameters configured for this integration driver.
	 * 
	 * @param customClassParameters
	 *           Any custom class parameters configured for this class.
	 * 
	 * @throws Exception
	 */
	@Override
	public void processResyncCommand(AIntegrationDriverEnvironment nodeApplication, String integrationDriverParameters,
		String customClassParameters)
		throws Exception
	{
		Exception exception = null;
		try
		{
			getUserAndDatabaseConfig(integrationDriverParameters, customClassParameters);
			connManager.updateConfig(config);
			// Update the cleaner period in case it has changed. It will pick up
			// the new period at the next wake up interval
			cleaner.setPeriod(config.safqCleanerPeriod * 1000);
			cleaner.setRetentionPeriod(config.safqRetentionPeriod);
			storeAndForwardQueue.setConfig(config);
			mapper.init(config);
		}
		catch(Exception e)
		{
			exception = e;
		}
		if (exception != null)
		{
			logger.recordEvent(getApplicationException("Unable to process RESYNC command.", exception));
		}
		else
		{
			logger.recordEvent(new ApplicationInfo(
				new String[]{"RESYNC command processed successfully. " + "New configuration: " + config.toString()}));
		}
	}

	/*------------------------------------------------------------------------*/
	/**
	 * Processes a <code>SET</code> command. This can be used to either reload
	 * the mapper class, using <code>RELOADMAPPER</code> or to activate message
	 * tracing for the NID, using <code>FRAUDTRACE ON</code>.
	 *
	 * @param nodeApplication
	 *           Details of the node application associated with this integration
	 *           driver.
	 * 
	 * @param param
	 *           The parameter to set.
	 * @param value
	 *           The new value of the parameter.
	 * 
	 * @throws Exception
	 */
	@Override
	public Action processSetCommand(AIntegrationDriverEnvironment nodeApplication, String param, String value)
		throws Exception
	{
		if (SetCommands.RELOADMAPPER.equalsIgnoreCase(param))
		{
			boolean success = getMapper(MapperClassDao.getMapperClassName());
			if (success)
			{
				this.logger.recordEvent(
					new ApplicationInfo(new String[]{"New mapper class loaded: " + mapper.getClass().getName()}));
			}
			else
			{
				throw new Exception("Mapper class was not loaded - unspecified reason");
			}
		}
		else if (SetCommands.FRAUDTRACE.equalsIgnoreCase(param))
		{
			if (SetCommands.ON_FRAUDTRACE.equalsIgnoreCase(value))
			{
				logger.enableMsgTracing();
			}
			else if (SetCommands.OFF_FRAUDTRACE.equalsIgnoreCase(value))
			{
				logger.disableMsgTracing();
			}
		}
		return null;
	}

	/*------------------------------------------------------------------------*/
	/**
	 * Processes an integration driver user event received on the application
	 * event queue. This method performs processing in any the following
	 * scenarios:
	 * <ul>
	 * <li>Transaction request responses received from the fraud system.</li>
	 * <li>Transaction request response received too late, discard it as we have
	 * already forwarded the original request to the sink.</li>
	 * <li>Reversal advice responses received from the fraud system.</li>
	 * </ul>
	 * 
	 * @param nodeApplication
	 *           Details of the node application associated with this integration
	 *           driver.
	 *
	 * @param userRef
	 *           The user reference object.
	 *
	 * @return The action to be performed by the caller on return from the
	 *         method.
	 *
	 * @throws Exception
	 */
	@Override
	public Action processUserEvent(AIntegrationDriverEnvironment nodeApplication, Object userRef) throws Exception
	{
		try
		{
			if(userRef != null)
			{
				if (userRef instanceof Iso8583FMS)
				{
					Iso8583FMS fmsMsg = (Iso8583FMS)userRef;
					int msgType = fmsMsg.getMsgType();
					// Transaction request responses received from the fraud system.
					if (msgType == Iso8583FMS.MsgType._0210_TRAN_REQ_RSP)
					{
						return processUserEventTranReqRsp(fmsMsg);
					}
					// Reversal advice responses received from the fraud system.
					else if (msgType == Iso8583FMS.MsgType._0430_ACQUIRER_REV_ADV_RSP)
					{
						return processUserEventAcquirerRevAdvRsp(fmsMsg);
					}
					else
					{
						logger.recordEvent(new ApplicationWarning(
							new String[]{"An unsupported message type was received " + "from the FMS."}));
					}
				}
				else if (userRef instanceof StanchionTimer)
				{
					StanchionTimer timer = (StanchionTimer)userRef;
					// A timeout occurred waiting for a response from FMS
					// The echo data is the original message from TM
					return processFmsTimeout((Iso8583Post)timer.getEchoData());
				}
				else if (userRef instanceof Sap.ConnectEvent)
				{
					// FMS may now be connected - check and send SAF if applicable
					pollSafq();
				}
			}
		}
		catch(Exception e)
		{
			logger.recordEvent(getApplicationException("An error occurred processing a user event. ", e));
			throw e;
		}
		return null;
	}

	/*------------------------------------------------------------------------*/
	/**
	 * Processes events containing 0210 {@link Iso8583FMS} messages received from
	 * the fraud system.
	 * 
	 * @param fmsMsg
	 *           The {@link Iso8583FMS} to process.
	 * 
	 * @return The next {@link Action} to be performed.
	 * 
	 * @throws Exception
	 */
	protected Action processUserEventTranReqRsp(Iso8583FMS fmsMsg) throws Exception
	{
		fmsMsg.putRspReceiveTime(System.currentTimeMillis());
		// Retrieve the original request.
		Iso8583Post origReq = (Iso8583Post)origReqCache
			.get(OriginalReqCache.constructKey(fmsMsg, 0, config.htTranFmsData));
		if (origReq == null)
		{
			logger.recordEvent(new ApplicationWarning(
				new String[]{"Unable to continue processing due to a missing original transaction. Fraud Response Details: "
					+ OriginalReqCache.constructKey(fmsMsg, 0, config.htTranFmsData)}));
			return null;
		}
		String origReqSwitchKey = origReq.getPrivField(Iso8583Post.PrivBit._002_SWITCH_KEY);
		// Scenario 2.1: Transaction request response received too
		// late, discard it as we have already forwarded the
		// original request to the sink.
		if (timedOutTrans.containsKey(origReqSwitchKey))
		{
			logger.recordEvent(new ApplicationWarning(new String[]{"A late response was received from the FMS and will "
				+ "not be processed. Original Transaction: " + EventTranInfo.getString(origReq)}));
			return null;
		}
		// Retrieve the linked fraud request.
		Iso8583FMS fraudReq = fraudReqRspCache.getFraudReq(origReqSwitchKey);
		if (fraudReq != null)
		{
			PerfMonManager.incrementAverageResponseTime(env.getSinkNodeName(),
				fmsMsg.getRspReceiveTime() - fraudReq.getReqSendTime());
		}
		else
		{
			logger.recordEvent(new ApplicationWarning(new String[]{"Matching fraud request expected but not "
				+ "found. Original Transaction: " + EventTranInfo.getString(origReq)}));
		}
		String timerId = TimerName.FRAUD_REQ_SEND_TIMER_PREFIX + origReqSwitchKey;
		TimerManager.INSTANCE.stopTimer(timerId);
		// Was Iso8583.RspCode.isApprove, but 7-Eleven doesn't use response codes
		// properly, so we only treat 00 as approved
		if (APPROVED.equals(fmsMsg.getResponseCode()))
		{
			PerfMonManager.incrementPercentageApprovedAndDeclined(env.getSinkNodeName(), 1, 1, 0, 1);
			// Cache the fraud response.
			fraudReqRspCache.put(origReqSwitchKey, null, fmsMsg);
			// The fraud check passed, forward the transaction to
			// the sink unaltered. 
			checkForDuplication(origReq);
			return new Action(null, origReq, null, null);
		}
		else
		{
			PerfMonManager.incrementPercentageApprovedAndDeclined(env.getSinkNodeName(), 0, 1, 1, 1);
			// Need to respond to TM by declining the original
			// request. 
			try
			{
				return new Action(mapper.constructDeclineToTM(origReq, fraudReq, fmsMsg), null, null, null);
			}
			catch(Exception e)
			{
				logger.recordEvent(getApplicationException("Error constructing decline response to TM", e));
				throw e;
			}
		}
	}

	/*------------------------------------------------------------------------*/
	/**
	 * Processes events containing 0430 {@link Iso8583FMS} messages received from
	 * the fraud system.
	 * 
	 * @param fmsMsg
	 *           The {@link Iso8583FMS} to process.
	 * 
	 * @return The next {@link Action} to be performed.
	 * 
	 * @throws Exception
	 */
	protected Action processUserEventAcquirerRevAdvRsp(Iso8583FMS fmsMsg) throws Exception
	{
		// Let the queue deal with the response
		storeAndForwardQueue.process(fmsMsg);
		String id = OriginalReqCache.constructKey(fmsMsg, 0, null);
		// Stop the reversal timer
		TimerManager.INSTANCE.stopTimer(TimerName.SAF_TIMEOUT_TIMER_PREFIX + id);
		// Log an event if a failure occurred.
		if (!fmsMsg.isApprovedResponse())
		{
			logger.recordEvent(new ApplicationWarning(new String[]{"The FMS declined/rejected a reversal. "
				+ "Response Code: '" + fmsMsg.getResponseCode() + "'. Transaction ID: " + id}));
		}
		// If there's another item in the SAF queue, send it now
		// Helps "pace" the queue - we'll send advices as fast they are responded
		// to
		pollSafq();
		return null;
	}

	/**
	 * Sends the store-and-forward message and starts a timer to track response
	 * 
	 * @param msg
	 * @throws Exception
	 */
	protected void sendSafMessage(Iso8583FMS msg) throws Exception
	{
		// Place in the outgoing queue. Any timers will be handled by the entity
		// at the other end of the queue to ensure more accurate timing
		fmsQueue.enqueueMsg(msg);
		// Start the SAF response timer
		TimerManager.INSTANCE.startTimer(
			new StanchionTimer(TimerName.SAF_TIMEOUT_TIMER_PREFIX + OriginalReqCache.constructKey(msg, 0, null),
				config.safqTimeout, null),
			this);
	}

	/**
	 * Called when a timeout occurs waiting for a response from the FMS
	 * 
	 * @param timerName
	 *           The name of the timer
	 * @param echoData
	 * @return
	 * @throws Exception
	 */
	public Action processFmsTimeout(Iso8583Post originalMsgFromTm) throws Exception
	{
		PerfMonManager.incrementMsgTimeoutsPerSecond(env.getSinkNodeName());
		String origTranSwitchKey = originalMsgFromTm.getPrivField(Iso8583Post.PrivBit._002_SWITCH_KEY);
		// Store a reference for use later.
		timedOutTrans.put(origTranSwitchKey, null);
		logger.recordEvent(new RequestTimeout(new String[]{EventTranInfo.getString(originalMsgFromTm)}));
		// Update the message.
		try
		{
			mapper.processMsgFromTranmgrSinkNode(originalMsgFromTm, fraudReqRspCache.getFraudReq(origTranSwitchKey));
		}
		catch(Exception e)
		{
			logger.recordEvent(getApplicationException("Error processing message from TM sink node", e));
			throw e;
		}
		// Forward the (updated) message from TM to the remote entity.
		// Check for duplication. If there is one, report a warning.
		checkForDuplication(originalMsgFromTm);
		return new Action(null, originalMsgFromTm, null, null);
	}

	/**
	 * Checks whether the message being sent to the remote is a duplicate. If so,
	 * a warning is logged.
	 * 
	 * @param msgFromTm
	 * @throws XPostilion 
	 */
	protected void checkForDuplication(Iso8583Post msgFromTm) throws XPostilion
	{
		if (config.duplicateRetentionPeriod == 0)
		{
			return;
		}
		String key = msgFromTm.getPrivField(PrivBit._002_SWITCH_KEY);
		long timestamp = DuplicateChecker.INSTANCE.isDuplicated(key, config.duplicateRetentionPeriod);
		if (timestamp != -1)
		{
			// This key has been seen before - log the warning
			logger.recordEvent(new DuplicateMessageDetected(new Object[]{key, msgFromTm.getMessageType(),
					new Timestamp(timestamp).toString(), msgFromTm.getField(Bit._004_AMOUNT_TRANSACTION),
					msgFromTm.getPrivField(PrivBit._003_ROUTING_INFO), msgFromTm.getField(Bit._003_PROCESSING_CODE)}));
		}
	}

	/**
	 * Starts the timer that controls SAFQ check frequency
	 */
	protected void startSafQpollTimer()
	{
		TimerManager.INSTANCE.startTimer(new StanchionTimer(TimerName.POLL_FRAUD_SAFQ, config.safqPollInterval, null),
			this);
	}

	/**
	 * Polls the store and forward queue for an entry - sends if found
	 * 
	 * @throws Exception
	 */
	protected void pollSafq() throws Exception
	{
		if (connManager.isConnected())
		{
			Iso8583FMS reversal = storeAndForwardQueue.poll();
			if (reversal != null)
			{
				sendSafMessage(reversal);
			}
			PerfMonManager.setStoreAndForwardQueueDepth(env.getSinkNodeName(), getSafQueueLength());
		}
	}

	/*------------------------------------------------------------------------*/
	/**
	 * This methods handles a message received by the node application from a
	 * Transaction Manager sink node. Processing occurs for the following
	 * scenarios:
	 * <ul>
	 * <li>If we are doing fraud checking and receive an 0200, we need to do a
	 * fraud check.</li>
	 * <li>If we are doing fraud checking and receive an 0420, we need to notify
	 * the fraud system using an 0420.</li>
	 * </ul>
	 *
	 * @param nodeApplication
	 *           Details of the node application associated with this integration
	 *           driver.
	 * 
	 * @param msgFromTm
	 *           The message received.
	 * 
	 * @return An action to be executed.
	 * 
	 * @throws Exception
	 */
	@Override
	public Action processMessageFromTranmgrSinkNode(AIntegrationDriverEnvironment nodeApplication, Iso8583Post msgFromTm)
		throws Exception
	{
		try
		{
			if (shouldProcess(msgFromTm))
			{
				// Scenario 1: If we are doing fraud checking and receive an
				// 0200, we need to do a fraud check.**/
				int msgType = msgFromTm.getMsgType();
				if (msgType == Iso8583.MsgType._0200_TRAN_REQ)
				{
					// Only bother with a fraud request if there's a connection to FMS
					if(connManager.isConnected())
					{
						// Cache the request.
						origReqCache.put(OriginalReqCache.constructKey(msgFromTm,
							getTranFilterConfigInterfaceIndex(msgFromTm), config.htTranFmsData), msgFromTm);
						// Queue a fraud check.
						generateTranReqToFMS(msgFromTm);
						// Skip processing of the message to the sink node until we
						// get a timeout or a reply from FMS
						return new Action();
					}
				}
				// If we are doing fraud checking and receive an 0420, we need
				// to notify the fraud system using an 0420.
				// If we get an 0420 here, it's from TM, so we just need to
				// forward it to the remote host (taken care of by the call to
				// the super).
				// We do however also need to notify the fraud system of the
				// reversal, so generate a reversal.
				else if (msgType == Iso8583.MsgType._0420_ACQUIRER_REV_ADV
					|| msgType == Iso8583.MsgType._0421_ACQUIRER_REV_ADV_REP)
				{
					// If there is already a reversal with the given key & sink node
					// name then don't bother
					if (StoreAndForwardQueueDao.retrieve(OriginalReqCache.constructKey(msgFromTm, 0, config.htTranFmsData),
						nodeApplication.getSinkNodeName()) == null)
					{
						generateReversalToFMS(msgFromTm);
					}
				}
			}
		}
		catch(Exception e)
		{
			logger.recordEvent(
				getApplicationException("An error occurred processing a message from Transaction Manager. ", e));
		}
		// If we either aren't doing or can't do fraud checking or receive an
		// unsupported message type, we just forward it.
		return super.processMessageFromTranmgrSinkNode(nodeApplication, msgFromTm);
	}

	/*------------------------------------------------------------------------*/
	/**
	 * This method handles a message received by the node application from a
	 * remote entity. Processing occurs for the following scenarios:
	 * <ul>
	 * <li>We have received a response from the authorizer and need to update it
	 * with fraud-check information before sending it to TM.</li>
	 * <li>If the authorizer declined the request, so we need to notify the fraud
	 * system using a reversal.</li>
	 * </ul>
	 *
	 * @param nodeApplication
	 *           Details of the node application associated with this integration
	 *           driver.
	 * 
	 * @param msgFromRemote
	 *           The message received.
	 * 
	 * @return An action to be executed.
	 * 
	 * @throws Exception
	 */
	@Override
	public Action processMsgFromRemote(AIntegrationDriverEnvironment nodeApplication, Iso8583Post msgFromRemote)
		throws Exception
	{
		try
		{
			if (shouldProcess(msgFromRemote))
			{
				// If the authorizer declined the request, we need to notify
				// the fraud system using a reversal. Since some of the sink
				// interchanges use "approved" response codes (such as 11) when
				// declining, we treat anything other than 00 as a decline
				if (!RspCode.SUCCESSFUL.equals(msgFromRemote.getResponseCode()))
				{
					generateReversalToFMS(msgFromRemote);
				}
				// We have received a response from the authorizer and need to
				// update it with fraud-check information before sending it to
				// TM.
				String origTranSwitchKey = EMPTY_STRING;
				if (msgFromRemote.isPrivFieldSet(Iso8583Post.PrivBit._002_SWITCH_KEY))
				{
					origTranSwitchKey = msgFromRemote.getPrivField(Iso8583Post.PrivBit._002_SWITCH_KEY);
				}
				// Retrieve the fraud request and response.
				Iso8583FMS fraudReq = fraudReqRspCache.getFraudReq(origTranSwitchKey);
				Iso8583FMS fraudRsp = fraudReqRspCache.getFraudRsp(origTranSwitchKey);
				if (fraudReq == null)
				{
					logger.recordEvent(new ApplicationWarning(
						new String[]{"Unable to locate expected associated " + "fraud-check message/s."}));
					return super.processMsgFromRemote(nodeApplication, msgFromRemote);
				}
				// Update the message destined for TM.
				try
				{
					mapper.processMsgFromRemoteSink(msgFromRemote, fraudReq, fraudRsp);
				}
				catch(Exception e)
				{
					logger.recordEvent(getApplicationException("Error processing message from remote sink", e));
					throw e;
				}
			}
		}
		catch(Exception e)
		{
			logger.recordEvent(
				getApplicationException("An error occurred processing a message from the remote sink node.", e));
		}
		return super.processMsgFromRemote(nodeApplication, msgFromRemote);
	}

	/*------------------------------------------------------------------------*/
	/**
	 * Generates a fraud/transaction request to be sent to the Fraud Management
	 * System.
	 * 
	 * @param msg
	 *           The {@link Iso8583Post} to reference during construction.
	 * 
	 * @throws Exception
	 */
	protected void generateTranReqToFMS(Iso8583Post msg) throws Exception
	{
		Iso8583FMS fraudReqMsg = null;
		try
		{
			fraudReqMsg = mapper.constructTranReqToFMS(msg, getTranFilterConfigInterfaceIndex(msg),
				TranFmsData.getDataForMsg(msg, config.htTranFmsData));
		}
		catch(Exception e)
		{
			logger.recordEvent(getApplicationException("Error constructing transaction request to FMS", e));
			throw e;
		}
		// Capture the time at this stage while we have access to this message
		// object.
		fraudReqMsg.putReqSendTime(System.currentTimeMillis());
		// Cache the fraud request.
		fraudReqRspCache.put(msg.getPrivField(Iso8583Post.PrivBit._002_SWITCH_KEY), fraudReqMsg, null);
		// Send the message to the FMS.
		// Start an action with a timer to eventually forward the message to the
		// sink if the fraud system doesn't respond. 
		// The timer is started before we enqueue:
		// It's the simplest way to avoid problems caused when this
		// thread is briefly suspended while the threads processing the message to
		// and from the fraud system complete processing. If that happens, the
		// timer won't be started by the time the response is received, which
		// means it can't be stopped. If we then start the timer after the response was
		// received, we'd be guaranteed to send a duplicate transaction to the
		// authorizer (because the response will have triggered a message in that
		// direction already)
		String timerId = TimerName.FRAUD_REQ_SEND_TIMER_PREFIX + msg.getPrivField(Iso8583Post.PrivBit._002_SWITCH_KEY);
		TimerManager.INSTANCE.startTimer(new StanchionTimer(timerId, config.fraudSystemTimeout, msg), this);
		// Finally, put the message in the queue for transmission to the FMS
		logger.debug("Enqueue message to fraud system", fraudReqMsg);
		fmsQueue.enqueueMsg(fraudReqMsg);
	}

	/*------------------------------------------------------------------------*/
	/**
	 * Generates a fraud reversal to be sent to the Fraud Management System.
	 * 
	 * @param msg
	 *           The {@link Iso8583Post} to reference during construction.
	 * @throws Exception
	 */
	protected void generateReversalToFMS(Iso8583Post msg) throws Exception
	{
		Iso8583FMS fraudReq = null;
		String origTranSwitchKey = EMPTY_STRING;
		// Get the fraud request using either source of switch key.
		origTranSwitchKey = msg.getPrivField(Iso8583Post.PrivBit._011_ORIGINAL_KEY);
		if (isStrNullOrEmpty(origTranSwitchKey))
		{
			origTranSwitchKey = msg.getPrivField(Iso8583Post.PrivBit._002_SWITCH_KEY);
		}
		if (!isStrNullOrEmpty(origTranSwitchKey))
		{
			fraudReq = fraudReqRspCache.getFraudReq(origTranSwitchKey);
		}
		// Create a reversal here if we found the matching request.
		if (fraudReq != null)
		{
			storeAndForwardQueue.insertOrUpdate(mapper.constructAcquirerRevAdvToFMS(fraudReq), null, null,
				config.retransmissionLimit, StoreAndForwardQueue.FALSE_NOT_SENT, StoreAndForwardQueue.TRUE_INSERT);
		}
	}

	/*------------------------------------------------------------------------*/
	/**
	 * Determines if a transaction qualifies for processing by this NID.
	 * 
	 * Request requirements:
	 * <ul>
	 * <li>The NID filter/processing mode should allow processing.</li>
	 * <li>The message type should be 020x, 02x or 042x.</li>
	 * <li>The FMS ID should be present in StructuredData.FIELD_9998.</li>
	 * <li>The FMS ID should exist in the FMS configuration in the database.</li>
	 * </ul>
	 * 
	 * Response requirements:
	 * <ul>
	 * <li>The NID filter/processing mode should allow processing.</li>
	 * <li>The message type should be 0210.</li>
	 * <li>An entry should exist in the Fraud Request/Response Cache associated
	 * to the transaction.</li>
	 * </ul>
	 * 
	 * @param msg
	 *           The {@link Iso8583Post} to evaluate.
	 * 
	 * @return <code>True</code> if the transaction qualifies, else
	 *         <code>false</code>.
	 * 
	 * @throws Exception
	 */
	protected boolean shouldProcess(Iso8583Post msg) throws Exception
	{
		if (FilterMode.DISABLE_CHECKING.equals(config.tranFilterMode))
		{
			return false;
		}
		int msgType = msg.getMsgType();
		if (msgType == Iso8583Post.MsgType._0200_TRAN_REQ || msgType == Iso8583Post.MsgType._0201_TRAN_REQ_REP
			|| msgType == Iso8583Post.MsgType._0420_ACQUIRER_REV_ADV
			|| msgType == Iso8583Post.MsgType._0421_ACQUIRER_REV_ADV_REP)
		{
			boolean checkAllTrans = FilterMode.ALL_TRANS.equals(config.tranFilterMode);
			boolean filterTrans = FilterMode.FILTER_TRANS.equals(config.tranFilterMode);
			boolean tranMatchesFilter = config.htTranFilterConfig.containsKey(getTranFilterConfigKey(msg));
			if (!checkAllTrans && filterTrans && !tranMatchesFilter)
			{
				return false;
			}
			// Check/Retrieve the FMS ID and configuration.
			if (isFmsIdAndConfigPresent(msg))
			{
				return true;
			}
			else
			{
				logger.recordEvent(new ApplicationWarning(new String[]{"Incomplete/Missing FMS configuration for "
					+ "the following transaction: " + EventTranInfo.getString(msg)}));
				return false;
			}
		}
		else if (msgType == Iso8583Post.MsgType._0210_TRAN_REQ_RSP
			&& fraudReqRspCache.containsKey(msg.getPrivField(Iso8583Post.PrivBit._002_SWITCH_KEY)))
		{
			return true;
		}
		return false;
	}

	/*------------------------------------------------------------------------*/
	/**
	 * Evaluates the given transaction to determine if matching FMS configuration
	 * exists for it in the {@link TranFmsData} {@link Hashtable} cache.
	 * 
	 * @param msgFromTM
	 *           The {@link Iso8583Post} to evaluate.
	 * 
	 * @return <code>True</code> if the configuration exists, else
	 *         <code>false</code>.
	 * 
	 * @throws Exception
	 */
	protected boolean isFmsIdAndConfigPresent(Iso8583Post msgFromTM) throws Exception
	{
		TranFmsData tranFmsData = TranFmsData.getDataForMsg(msgFromTM, config.htTranFmsData);
		if (tranFmsData != null)
		{
			return true;
		}
		return false;
	}

	/*------------------------------------------------------------------------*/
	/**
	 * Evaluates the given transaction to retrieve the interface index, required
	 * for fraud message construction, from the cached database configuration.
	 * 
	 * @param msg
	 *           The {@link Iso8583Post} to evaluate.
	 * 
	 * @return The interface index.
	 * 
	 * @throws Exception
	 */
	protected int getTranFilterConfigInterfaceIndex(Iso8583Post msg) throws Exception
	{
		if (msg != null)
		{
			Integer interfaceIndex = config.htTranFilterConfig.get(getTranFilterConfigKey(msg));
			if (interfaceIndex != null && interfaceIndex > 0)
			{
				// Subtract 1 to make it a valid Java index reference.
				interfaceIndex = interfaceIndex - 1;
				return interfaceIndex;
			}
		}
		return 0;
	}

	/*------------------------------------------------------------------------*/
	/**
	 * Evaluates the given transaction to retrieve the transaction filter
	 * configuration key. This key consists of the transaction type and the
	 * extended transaction type, if present.
	 * 
	 * @param msg
	 *           The {@link Iso8583Post} to evaluate.
	 * 
	 * @return The key to use for lookups.
	 * 
	 * @throws Exception
	 */
	protected String getTranFilterConfigKey(Iso8583Post msg) throws Exception
	{
		String tranType = FraudNID.EMPTY_STRING;
		ProcessingCode procCode = msg.getProcessingCode();
		if (procCode != null)
		{
			tranType = procCode.getTranType();
		}
		String extTranType = FraudNID.EMPTY_STRING;
		if (msg.isPrivFieldSet(Iso8583Post.PrivBit._033_EXTENDED_TRAN_TYPE))
		{
			extTranType = msg.getPrivField(Iso8583Post.PrivBit._033_EXTENDED_TRAN_TYPE);
		}
		return tranType + extTranType;
	}

	/**
	 * Creates an application error with the correct contexts so that exceptions
	 * are neatly presented in logs and traces
	 * 
	 * @param text
	 *           Headline text for the error
	 * @param exception
	 *           The exception to log
	 */
	public static AEventRecord getApplicationException(String text, Throwable exception)
	{
		return new ApplicationException(new AContext[]{new ExceptionContext(exception)}, new Object[]{text});
	}

	/*------------------------------------------------------------------------*/
	/**
	 * Utility method to check if a {@link String} is <code>null</code> or empty
	 * (equal to an empty {@link String}, I.e. "").
	 * 
	 * @param value
	 *           The {@link String} to evaluate.
	 * 
	 * @return <code>True</code> if the given {@link String} is <code>null</code>
	 *         or empty, else <code>false</code>.
	 */
	public static boolean isStrNullOrEmpty(String value)
	{
		if (value == null || EMPTY_STRING.equals(value))
		{
			return true;
		}
		return false;
	}
	/*------------------------------------------------------------------------*/
	public static final String	EMPTY_STRING					= "";
	public static final String	MAPPER_CLASS_NAME				= "MapperClassName";
	public static final String	CONTROL_FRAUDES_NODE_NAME	= "CtrlFraudes";

	/*------------------------------------------------------------------------*/
	public static final class FilterMode
	{
		public static final String	DISABLE_CHECKING	= "DISABLE";
		public static final String	FILTER_TRANS		= "FILTER";
		public static final String	ALL_TRANS			= "ALL";
	}

	/*------------------------------------------------------------------------*/
	public static final class TimerName
	{
		public static final String	POLL_FRAUD_SAFQ					= "POLL_FRAUD_SAFQ";
		public static final String	SAF_TIMEOUT_TIMER_PREFIX		= "TIMEOUT_FRAUD_SAFQ";
		public static final String	FRAUD_REQ_SEND_TIMER_PREFIX	= "FRAUD_REQ_SEND_TIMER_";
	}

	/*------------------------------------------------------------------------*/
	public static final class SetCommands
	{
		public static final String	RELOADMAPPER	= "RELOADMAPPER";
		public static final String	FRAUDTRACE		= "FRAUDTRACE";
		public static final String	ON_FRAUDTRACE	= "ON";
		public static final String	OFF_FRAUDTRACE	= "OFF";
	}
	/*------------------------------------------------------------------------*/

	/**
	 * Called by {@link TimerManager.TimerUser} when a timer expires. We insert
	 * the expired timer onto the queue as an {@link IntegrationEvent} with an
	 * embedded {@link StanchionTimer} to force the interchange to handle the
	 * timer
	 * 
	 * @param timer
	 *           The timer that expired
	 */
	@Override
	public void onTimerExpired(StanchionTimer timer, long elapsedTime)
	{
		try
		{
			String id = timer.getId();
			if (id.startsWith(TimerName.FRAUD_REQ_SEND_TIMER_PREFIX))
			{
				// Timeouts of fraud messages must be added to the NID's queue
				App.getQueue().append(new IntegrationEvent(this, timer));
			}
			else if (TimerName.POLL_FRAUD_SAFQ.equals(id))
			{
				try
				{
					// Poll the SAFQ (it will check for connectivity first)
					pollSafq();
				}
				finally
				{
					// Queue the next poll, no matter what
					startSafQpollTimer();
				}
			}
			else if (id.startsWith(TimerName.SAF_TIMEOUT_TIMER_PREFIX))
			{
				String tranId = id.substring(TimerName.SAF_TIMEOUT_TIMER_PREFIX.length());
				storeAndForwardQueue.processTimeout(tranId);
			}
		}
		catch(Exception e)
		{
			logger.recordEvent(getApplicationException("An error occurred processing a timeout event. ", e));
		}
	}
}