/*----------------------------------------------------------------------------*\
 *                                                                            *
 * Copyright (c) 2018 by Montant Limited. All rights reserved.                *
 *                                                                            *
 * This software is the confidential and proprietary property of              *
 * Montant Limited and may not be disclosed, copied or distributed            *
 * in any form without the express written permission of Montant Limited.     *
 *                                                                            *
\*----------------------------------------------------------------------------*/
package postilion.realtime.fraudnid.eventrecorder;

import postilion.realtime.fraudnid.msg.Iso8583FMS;
import postilion.realtime.sdk.eventrecorder.AEventRecord;
import postilion.realtime.sdk.eventrecorder.EventRecorder;
import postilion.realtime.sdk.eventrecorder.contexts.InterchangeContext;
import postilion.realtime.sdk.ipc.IEndpoint;

/**
 * Logger interface
 */
public interface ILogger
{
	/*------------------------------------------------------------------------*/
	void enableMsgTracing();

	/*------------------------------------------------------------------------*/
	void disableMsgTracing();

	/*------------------------------------------------------------------------*/
	/**
	 * Call the {@link EventRecorder} to record the given event after 
	 * associating the sink node's {@link InterchangeContext}.
	 * 
	 * @param event
	 * 			The {@link AEventRecord} to record.
	 */
	void recordEvent(AEventRecord event);

	/**
	 * Write the given information to the log (headline only)
	 * @param text
	 */
	void debug(Object text);
	
	/**
	 * Write the given information to the log with details
	 * @param text
	 */
	void debug(String text, Object details);
	
	

	/**
	 * @return true if tracing is on
	 */
	boolean isTraceOn();

	/*------------------------------------------------------------------------*/
	/**
	 * Log the given request message to the event/tracing framework.
	 * 
	 * @param msg
	 * 			The {@link Iso8583FMS} message to log.
	 * 
	 * @param endpoint
	 * 			The {@link IEndpoint} to which the data was transmitted.
	 * 
	 * @throws Exception
	 */
	void logRequest(Iso8583FMS msg, IEndpoint endpoint) throws Exception;

	/*------------------------------------------------------------------------*/
	/**
	 * Log the given response message to the event/tracing framework.
	 * 
	 * @param msg
	 * 			The {@link Iso8583FMS} message to log.
	 * 
	 * @param endpoint
	 * 			The {@link IEndpoint} to which the data was transmitted.
	 * 
	 * @throws Exception
	 */
	void logResponse(Iso8583FMS msg, IEndpoint endpoint) throws Exception;
}
