/*----------------------------------------------------------------------------*
 *                                                 
 * Copyright (c) 2018 by Montant Limited. All rights reserved.
 *                                                                             
 * This software is the confidential and proprietary property of 
 * Montant Limited and may not be disclosed, copied or distributed 
 * in any form without the express written permission of Montant Limited.
 *----------------------------------------------------------------------------*/

package postilion.realtime.fraudnid.eventrecorder;

import java.io.PrintWriter;
import java.io.StringWriter;

import postilion.realtime.sdk.message.bitmap.Iso8583Post;
import postilion.realtime.sdk.message.bitmap.XFieldUnableToConstruct;

/**
 * Utility class providing basic functionality to convert message information 
 * into Event-compatible data.
 */
public class EventTranInfo
{
	/*------------------------------------------------------------------------*/
	/**
	 * Extracts the below fields from an {@link Iso8583Post} message and places 
	 * it in a user-friendly {@link String} which can be used when storing 
	 * transaction information in events.
	 * <br>
	 * <ul>
	 * <li>Switch Key (Field 127.22)</li>
	 * <li>Sink Node Name (Field 127.3.SinkNode)</li>
	 * <li>Message Type (MTI)</li>
	 * <li>Transaction Type (Field 3.1)</li>
	 * <li>Extended Transaction Type (Field 127.33)</li>
	 * <ul>
	 * <br>
	 * 
	 * @param msg
	 * 			The {@link Iso8583Post} to evaluate.
	 * 
	 * @return
	 * 			A {@String} containing important transaction information.
	 */
	public static String getString(Iso8583Post msg)
	{
		try
		{
			StringBuilder strBuilder = new StringBuilder();
			
			strBuilder.append("Switch Key: '");
			strBuilder.append(msg.getPrivField(Iso8583Post.PrivBit._002_SWITCH_KEY));
			strBuilder.append("', Sink Node Name: '"); 
			strBuilder.append(msg.getRoutingInfo().getSinkNode()); 
			strBuilder.append("', Message Type: '"); 
			strBuilder.append(msg.getMessageType());
			strBuilder.append("', Transaction Type: '"); 
			strBuilder.append(msg.getProcessingCode().getTranType()); 
			strBuilder.append("', Extended Transaction Type: '"); 
			strBuilder.append(msg.getPrivField(Iso8583Post.PrivBit._033_EXTENDED_TRAN_TYPE)); 
			strBuilder.append("'.");
			
			return strBuilder.toString();
		}
		catch (XFieldUnableToConstruct e)
		{
			return 
				"Construction failed. Exception: " +
				getExceptionAsString(e);
		}
	}
	
	/**
	 * Utility method to represent an exception as a string.
	 * 
	 * @param e
	 * 			The {@link Exception} to process.
	 * @return
	 * 			A {@link String} representation of the exception.
	 */
	public static String getExceptionAsString(Exception e)
	{
		StringWriter exceptionWriter = new StringWriter();
		e.printStackTrace(new PrintWriter(exceptionWriter));
		return exceptionWriter.toString();
	}	
	
	/*------------------------------------------------------------------------*/
}
