/*----------------------------------------------------------------------------*
 *                                                 
 * Copyright (c) 2018 by Montant Limited. All rights reserved.
 *                                                                             
 * This software is the confidential and proprietary property of 
 * Montant Limited and may not be disclosed, copied or distributed 
 * in any form without the express written permission of Montant Limited.
 *----------------------------------------------------------------------------*/

package postilion.realtime.fraudnid.eventrecorder;

import postilion.realtime.fraudnid.FraudNID;
import postilion.realtime.fraudnid.eventrecorder.events.DebugInfo;
import postilion.realtime.fraudnid.eventrecorder.events.FmsRequestMessage;
import postilion.realtime.fraudnid.eventrecorder.events.FmsResponseMessage;
import postilion.realtime.fraudnid.msg.Iso8583FMS;
import postilion.realtime.sdk.env.SystemProperties;
import postilion.realtime.sdk.eventrecorder.AContext;
import postilion.realtime.sdk.eventrecorder.AEventRecord;
import postilion.realtime.sdk.eventrecorder.EventRecorder;
import postilion.realtime.sdk.eventrecorder.contexts.EndpointContext;
import postilion.realtime.sdk.eventrecorder.contexts.InterchangeContext;
import postilion.realtime.sdk.eventrecorder.contexts.MessageContext;
import postilion.realtime.sdk.ipc.IEndpoint;
import postilion.realtime.sdk.message.IMessage;
import postilion.realtime.sdk.node.AIntegrationDriverEnvironment;

/**
 * The {@link Logger} class is responsible for interacting with the Realtime 
 * Event/Tracing framework. It is used to log events from this application and 
 * to trace request/response message to/from the Fraud Management System.
 */
public class Logger implements ILogger
{
	protected AIntegrationDriverEnvironment env = null;
	protected boolean traceOn = false;
	
	/*------------------------------------------------------------------------*/
	/**
	 * Default Constructor. If the system is configured to enable tracing on 
	 * startup, this constructor will enable {@link FraudNID} message tracing.
	 * @param env
	 */
	public Logger(AIntegrationDriverEnvironment env)
	{
		this.env = env;

		if (SystemProperties.shouldEnableTracingAtStartup())
		{
			enableMsgTracing();
		}
	}
	
	/*------------------------------------------------------------------------*/
	/* (non-Javadoc)
	 * @see postilion.realtime.fraudnid.eventrecorder.ILogger#enableMsgTracing()
	 */
	@Override
	public void enableMsgTracing()
    {
		traceOn = true;
    }
	
	/*------------------------------------------------------------------------*/
	/* (non-Javadoc)
	 * @see postilion.realtime.fraudnid.eventrecorder.ILogger#disableMsgTracing()
	 */
	@Override
	public void disableMsgTracing()
    {
		traceOn = false;
    }
	
	/*------------------------------------------------------------------------*/
	/* (non-Javadoc)
	 * @see postilion.realtime.fraudnid.eventrecorder.ILogger#recordEvent(postilion.realtime.sdk.eventrecorder.AEventRecord)
	 */
	@Override
	public void recordEvent(AEventRecord event)
	{
		event.addContext(new InterchangeContext(env.getSinkNodeName()));
		EventRecorder.recordEvent(event);
	}
	
	/* (non-Javadoc)
	 * @see postilion.realtime.fraudnid.eventrecorder.ILogger#debug(java.lang.String)
	 * Defer the toString to when we know trace is on to avoid expensive calls
	 */
	@Override
	public void debug(Object text) 
	{
		if (traceOn)
		{
			recordEvent(new DebugInfo(new Object[]{text.toString()}));
		}
	}

	/* (non-Javadoc)
	 * @see postilion.realtime.fraudnid.eventrecorder.ILogger#debug(java.lang.String)
	 */
	@Override
	public void debug(String text, Object detail) 
	{
		AEventRecord event = null;
		if (traceOn)
		{
			if(detail != null && detail instanceof IMessage)
			{
				// Special tracing for messages - don't convert the detail to a String
				event = new DebugInfo(new Object[] {text, detail});
			}
			else
			{
				event = new DebugInfo(new Object[]{text, (detail != null ? detail.toString() : "<null>")});
			}
			recordEvent(event);
		}
	}
	
	/* (non-Javadoc)
	 * @see postilion.realtime.fraudnid.eventrecorder.ILogger#isTraceOn()
	 */
	@Override
	public boolean isTraceOn() {
		return traceOn == true;
	}
	
	/*------------------------------------------------------------------------*/
	/* (non-Javadoc)
	 * @see postilion.realtime.fraudnid.eventrecorder.ILogger#logRequest(postilion.realtime.fraudnid.msg.Iso8583FMS, postilion.realtime.sdk.ipc.IEndpoint)
	 */
	@Override
	public void logRequest(
			Iso8583FMS msg, 
			IEndpoint endpoint) 
	throws Exception
	{
		if (!traceOn)
		{
			return;
		}

		EventRecorder.recordEvent(
			new FmsRequestMessage(
				new AContext[]{
					new InterchangeContext(env.getName()), 
					new MessageContext(msg.getMessageType()), 
					new EndpointContext(endpoint)}, 
				new Object[]{msg}, 
				msg.getBinaryData()));
	}
	
	/*------------------------------------------------------------------------*/
	/* (non-Javadoc)
	 * @see postilion.realtime.fraudnid.eventrecorder.ILogger#logResponse(postilion.realtime.fraudnid.msg.Iso8583FMS, postilion.realtime.sdk.ipc.IEndpoint)
	 */
	@Override
	public void logResponse(
			Iso8583FMS msg, 
			IEndpoint endpoint) 
	throws Exception
	{
		if (!traceOn)
		{
			return;
		}
		
		EventRecorder.recordEvent(
			new FmsResponseMessage(
				new AContext[]{
					new InterchangeContext(env.getName()), 
					new MessageContext(msg.getMessageType()), 
					new EndpointContext(endpoint)}, 
				new Object[]{msg}, 
				msg.getBinaryData()));
	}

	/*------------------------------------------------------------------------*/
}