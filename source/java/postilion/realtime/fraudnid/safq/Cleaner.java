/*----------------------------------------------------------------------------*\
 *                                                                            *
 * Copyright (c) 2018 by Montant Limited. All rights reserved.                *
 *                                                                            *
 * This software is the confidential and proprietary property of              *
 * Montant Limited and may not be disclosed, copied or distributed            *
 * in any form without the express written permission of Montant Limited.     *
 *                                                                            *
\*----------------------------------------------------------------------------*/
package postilion.realtime.fraudnid.safq;

import java.sql.Date;
import java.sql.SQLException;
import java.util.Calendar;

import postilion.realtime.fraudnid.dao.StoreAndForwardQueueDao;
import postilion.realtime.fraudnid.eventrecorder.ILogger;
import postilion.realtime.fraudnid.eventrecorder.events.ApplicationException;
import postilion.realtime.sdk.eventrecorder.AContext;
import postilion.realtime.sdk.eventrecorder.EventRecorder;
import postilion.realtime.sdk.eventrecorder.contexts.ExceptionContext;
import postilion.realtime.sdk.util.convert.Format;

/**
 * Implements a thread-able store-and-forward queue cleaner. Running outside the
 * thread context of the NID & its interchange reduces the impact of the cleaner
 * on the interchange threads
 * <ul>
 * <li>Removes entries older than the configured retention period for the sink
 * node upon which this instance of the NID operates that are:
 * <ul>
 * <li>Completed (sent and responded to)
 * <li>Aborted
 * </ul>
 */
public class Cleaner
	implements
	Runnable
{
	private Object		lock			= new Object();
	// How often to wake up and perform housekeeping
	protected long		period;
	protected ILogger	logger;
	protected boolean	stop			= false;
	protected boolean	stopped		= false;
	protected long		cleans		= 0;
	protected String	sinkNode		= null;
	// Test mode - only set when a junit wants to skip the tough DB stuff
	boolean				testMode		= false;
	boolean				exception	= false;
	protected int		retentionPeriod;

	public int getRetentionPeriod()
	{
		return retentionPeriod;
	}

	public void setRetentionPeriod(int retentionPeriod)
	{
		this.retentionPeriod = retentionPeriod;
	}

	/**
	 * Construct with parameters
	 * 
	 * @param period
	 *           Number of milliseconds in between cleaner cycles
	 * @param logger
	 *           Trace writer
	 */
	public Cleaner(long period, String sinkNode, ILogger logger, int retentionPeriod)
	{
		super();
		this.period = period;
		this.logger = logger;
		this.sinkNode = sinkNode;
		this.retentionPeriod = retentionPeriod;
	}

	public void stop()
	{
		synchronized(lock)
		{
			stop = true;
		}
	}

	public boolean hasStopped()
	{
		synchronized(lock)
		{
			return stopped == true;
		}
	}

	public void setPeriod(long period)
	{
		synchronized(lock)
		{
			this.period = period;
		}
	}

	/**
	 * Sleep for a second, determine if the period has lapsed, and if so, do the
	 * clean. We do it this way so that the period can be changed without
	 * stopping and restarting the cleaner. The 1-second wake up is no bother
	 */
	@Override
	public void run()
	{
		boolean done = false;
		stopped = false;
		stop = false;
		cleans = 0;
		// First time in, last time the system ran the cleaner is "now"
		long lastActivation = System.currentTimeMillis();
		while(!done)
		{
			synchronized(lock)
			{
				if (stop)
				{
					done = stop;
					logger.debug("Cleaner stopping");
					continue;
				}
			}
			sleep();
			long elapsed = System.currentTimeMillis() - lastActivation;
			if (elapsed >= period)
			{
				try
				{
					doClean();
				}
				catch(Exception e)
				{
					logger
						.debug("Cleaner caught an exception: " + e.getClass().getName() + " " + Format.toStackBackTrace(e));
					EventRecorder.recordEvent(new ApplicationException(new AContext[]{new ExceptionContext(e)},
						new String[]{"Cleaner detected an exception"}));
				}
				// Reset last activation to "now"
				lastActivation = System.currentTimeMillis();
			}
		}
		synchronized(lock)
		{
			stopped = true;
			logger.debug("Cleaner stopped");
		}
	}

	private void sleep()
	{
		try
		{
			Thread.sleep(1000);
		}
		catch(InterruptedException e)
		{
		}
	}

	/**
	 * Handles the actual cleaning.
	 * <ul>
	 * <li>Determine Realtime's retention period in days
	 * <li>Go and delete stale records from the SAFQ table
	 * </ul>
	 * 
	 * @throws SQLException
	 */
	protected void doClean() throws SQLException
	{
		cleans++;
		if (testMode)
		{
			logger.debug("Cleaner in test mode, skip processing");
			if (exception)
			{
				logger.debug("Test exception requested");
				throw new SQLException("Forced exception");
			}
			return;
		}
		// Calculate the oldest record to delete - now minus the retention days,
		// to the second
		Calendar cal = Calendar.getInstance();
		// Subtract the days
		cal.add(Calendar.DAY_OF_YEAR, retentionPeriod * -1);
		Date cleanDate = new Date(cal.getTime().getTime());
		long cleaned = StoreAndForwardQueueDao.clean(cleanDate, sinkNode);
		logger.debug("Cleaner cleaned " + cleaned + " records.");
	}
}
