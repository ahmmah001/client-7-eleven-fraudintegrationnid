/*----------------------------------------------------------------------------*
 *                                                 
 * Copyright (c) 2018 by Montant Limited. All rights reserved.
 *                                                                             
 * This software is the confidential and proprietary property of 
 * Montant Limited and may not be disclosed, copied or distributed 
 * in any form without the express written permission of Montant Limited.
 *----------------------------------------------------------------------------*/
package postilion.realtime.fraudnid.safq;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.concurrent.LinkedBlockingDeque;

/**
 * Wrapper/Container class for Store & Forward Queue transactions.
 */
public class SAFQTran
	implements
	Serializable
{
	private static final long	serialVersionUID	= -6215529436820293258L;
	protected String				id						= null;
	protected String				msg					= null;
	protected String				sinkNodeName		= null;
	protected Timestamp			queueTime			= null;
	protected int					retriesRemaining	= 0;
	protected boolean				sent					= false;
	protected String				mti					= null;
	protected String				requestTime			= null;
	protected String				responseTime		= null;
	protected String				responseCode		= null;

	public String getMti()
	{
		return mti;
	}

	public void setMti(String mti)
	{
		this.mti = mti;
	}

	public String getRequestTime()
	{
		return requestTime;
	}

	public void setRequestTime(String requestTime)
	{
		this.requestTime = requestTime;
	}

	public String getResponseTime()
	{
		return responseTime;
	}

	public void setResponseTime(String responseTime)
	{
		this.responseTime = responseTime;
	}

	public String getResponseCode()
	{
		return responseCode;
	}

	public void setResponseCode(String responseCode)
	{
		this.responseCode = responseCode;
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getMsgData()
	{
		return msg;
	}

	public void setMsgData(String msg)
	{
		this.msg = msg;
	}

	public String getSinkNodeName()
	{
		return sinkNodeName;
	}

	public void setSinkNodeName(String sinkNodeName)
	{
		this.sinkNodeName = sinkNodeName;
	}

	public Timestamp getQueueTime()
	{
		return queueTime;
	}

	public void setQueueTime(Timestamp queueTime)
	{
		this.queueTime = queueTime;
	}

	public int getRetriesRemaining()
	{
		return retriesRemaining;
	}

	public void setRetriesRemaining(int retries)
	{
		this.retriesRemaining = retries;
	}

	public boolean getSent()
	{
		return sent;
	}

	public void setSent(boolean sent)
	{
		this.sent = sent;
	}

	public void setQueueTime(String format)
	{
		this.queueTime = Timestamp.valueOf(format);
	}

	public void setQueueTime(Date date)
	{
		this.queueTime = new Timestamp(date.getTime());
	}

	/**
	 * We need this to satisfy {@link LinkedBlockingDeque#remove(Object)}, used
	 * in {@link StoreAndForwardQueue}<br>
	 * Two transactions are equal if they have the same ID
	 */
	@Override
	public boolean equals(Object obj)
	{
		if (obj != null)
		{
			if (obj instanceof SAFQTran)
			{
				if (id != null)
				{
					return id.equals(((SAFQTran)obj).getId());
				}
			}
			else if (obj instanceof String)
			{
				if (id != null)
				{
					return id.equals((String)obj);
				}
			}
		}
		return false;
	}

	/**
	 * Satisfy the requirement that equal objects have equivalent hashcodes
	 */
	@Override
	public int hashCode()
	{
		if (id != null)
		{
			return id.hashCode();
		}
		return super.hashCode();
	}

	@Override
	public String toString()
	{
		StringBuilder builder = new StringBuilder();
		builder.append("SAFQTran [id=").append(id).append(", sinkNodeName=").append(sinkNodeName).append(", queueTime=")
			.append(queueTime).append(", retriesRemaining=").append(retriesRemaining).append(", sent=").append(sent)
			.append(", mti=").append(mti).append(", requestTime=").append(requestTime).append(", responseTime=")
			.append(responseTime).append(", responseCode=").append(responseCode).append("]");
		return builder.toString();
	}
}