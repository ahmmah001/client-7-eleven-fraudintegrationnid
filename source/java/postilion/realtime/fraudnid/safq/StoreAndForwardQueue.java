/*----------------------------------------------------------------------------*
 *                                                 
 * Copyright (c) 2018 by Montant Limited. All rights reserved.
 *                                                                             
 * This software is the confidential and proprietary property of 
 * Montant Limited and may not be disclosed, copied or distributed 
 * in any form without the express written permission of Montant Limited.
 *----------------------------------------------------------------------------*/
package postilion.realtime.fraudnid.safq;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.LinkedBlockingDeque;

import postilion.realtime.fraudnid.cache.OriginalReqCache;
import postilion.realtime.fraudnid.config.NIDConfig;
import postilion.realtime.fraudnid.dao.StoreAndForwardQueueDao;
import postilion.realtime.fraudnid.eventrecorder.ILogger;
import postilion.realtime.fraudnid.eventrecorder.Logger;
import postilion.realtime.fraudnid.eventrecorder.events.ApplicationError;
import postilion.realtime.fraudnid.eventrecorder.events.ApplicationWarning;
import postilion.realtime.fraudnid.msg.Iso8583FMS;
import postilion.realtime.fraudnid.msg.Iso8583FMS.MsgType;
import postilion.realtime.sdk.ipc.SecurityManager;
import postilion.realtime.sdk.node.AIntegrationDriverEnvironment;
import postilion.realtime.sdk.util.XPostilion;

/**
 * This class is a custom implementation of a persistent Store & Forward Queue
 * for the FraudNID. It has one backing table in the database shared by all
 * instances of the NID. In order to store data safely, it utilizes the SDK's
 * encryption services to encrypt and decrypt message data to/from the database.
 */
public class StoreAndForwardQueue
{
	protected AIntegrationDriverEnvironment	environment		= null;
	protected String									sinkNodeName	= null;
	protected ILogger									logger			= null;
	protected int										maxRetries		= 0;
	protected SecurityManager						secMgr			= null;
	protected NIDConfig								config			= null;
	protected LinkedBlockingDeque<SAFQTran>	safQ				= new LinkedBlockingDeque<SAFQTran>();
	// Bookmarks in the SAFQ table
	protected Timestamp								bookmarkStamp	= null;
	protected String									bookmarkId		= null;
	// Hold the in-flight transactions
	protected Hashtable<String, SAFQTran>		inFlight			= new Hashtable<String, SAFQTran>();
	// Length of the store-and-forward queue (-1 will force a count of records in
	// the table)
	protected int										currentLength	= -1;	
	// sync object for queue operations
	private static final Object					safLock			= new Object();

	public void setConfig(NIDConfig config)
	{
		this.config = config;
	}

	/*------------------------------------------------------------------------*/
	/**
	 * Default Constructor.
	 * 
	 * @param environment
	 *           The {@link AIntegrationDriverEnvironment} to associate with the
	 *           new instance.
	 * 
	 * @param config
	 *           The {@link NIDConfig} to reference for additional configuration.
	 * 
	 * @param logger
	 *           The {@link Logger} to use for interacting with the Realtime
	 *           Support Event Framework.
	 * 
	 * @throws Exception
	 */
	public StoreAndForwardQueue(AIntegrationDriverEnvironment environment, NIDConfig config, ILogger logger)
		throws Exception
	{
		this.environment = environment;
		this.sinkNodeName = environment.getSinkNodeName();
		this.logger = logger;
		this.maxRetries = config.retransmissionLimit;
		this.secMgr = new SecurityManager();
		this.config = config;
	}

	/*------------------------------------------------------------------------*/
	/**
	 * Use to insert or update a transaction, using a {@link SAFQTran} object,
	 * in/into the SAFQ database table.
	 * 
	 * @param msg
	 *           The {@link Iso8583FMS} message to encrypt insert/update into/in
	 *           the database.
	 * 
	 * @param retries
	 *           The number of times a transaction in the SAFQ may be retried.
	 * 
	 * @param sent
	 *           A flag indicating if the transaction has been sent or not.
	 * 
	 * @param insert
	 *           Set to <code>true</code> if a transaction should be inserted or
	 *           <code>false</code> if the transaction should be updated.
	 * @param msgIn
	 *           A message received from FMS that should be logged in String form
	 *           to the table (optional)
	 * @param msgOut
	 *           A message sent to FMS that should be logged in String form
	 *           to the table (optional)
	 * 
	 * @throws Exception
	 */
	public void insertOrUpdate(Iso8583FMS msg, Iso8583FMS msgIn, Iso8583FMS msgOut, int retries, boolean sent,
		boolean insert)
		throws Exception
	{
		// Encrypt the message data.
		byte[] decrypted = msg.toMsg();
		String encrypted = secMgr.encrypt(decrypted);
		// Construct the SAFQTran record.
		SAFQTran record = new SAFQTran();
		record.setId(OriginalReqCache.constructKey(msg, 0, null));
		record.setMsgData(encrypted);
		record.setSinkNodeName(sinkNodeName);
		record.setMti(msg.getMessageType());
		SimpleDateFormat dateFormat = new SimpleDateFormat(SAFQ_DATETIME_FORMAT);
		if (insert)
		{
			record.setQueueTime(new Date());
			record.setRetriesRemaining(maxRetries);
			record.setSent(false);
			record.setRequestTime(null);
			record.setResponseCode(null);
			record.setResponseTime(null);
			// Increase the length tracker for this new entry
			currentLength++;
		}
		else
		{
			record.setRetriesRemaining(retries);
			record.setSent(sent);
			// For advice responses, update the time we received the response and
			// the response code
			if (MsgType.isAdviceResponse(msg.getMsgType()))
			{
				record.setResponseTime(dateFormat.format(new Date()));
				record.setResponseCode(msg.getResponseCode());
			}
		}
		StoreAndForwardQueueDao.insertOrUpdateTranInSAFQ(record, insert, logger, msgIn, msgOut, config.accountHashAlgorithm);
	}

	/**
	 * Determine if there's a transaction in the queue to send and the in-flight
	 * limit hasn't been reached. If necessary, builds up the cache to the
	 * configured maximum
	 * 
	 * @return true if there's a transaction waiting, false if not
	 * @throws Exception
	 */
	public boolean peek() throws Exception
	{
		// Make sure we only try this from one thread (and also not while poll()
		// is being called), otherwise the queue will
		// end up with duplicate transactions in it
		synchronized(safLock)
		{
			// Don't do anything if we already have the maximum transactions in
			// flight
			if (inFlight.size() >= config.safqMaxInFlight)
			{
				return false;
			}
			SAFQTran record = null;
			// Do we need to cache more transactions?
			if (safQ.size() < 1)
			{
				int need = config.safqCacheSize - safQ.size();
				List<SAFQTran> records = StoreAndForwardQueueDao.getNextTransFromStoreAndForwardQueue(sinkNodeName, need,
					bookmarkStamp, bookmarkId);
				for (Iterator<SAFQTran> i = records.iterator(); i.hasNext();)
				{
					record = i.next();
					// Save the latest and greatest timestamp and ID so we can use
					// them
					// as a
					// bookmark for the next retrieval
					bookmarkStamp = record.getQueueTime();
					bookmarkId = record.getId();
					safQ.add(record);
				}
			}
			return safQ.size() >= 1;
		}
	}

	/**
	 * "Polls" the queue to to check if there is a transaction which should be
	 * sent the remote. If there are no entries in the queue, more are fetched
	 * from the database. If the maximum transactions are in flight then no
	 * further transactions are sent
	 * 
	 * @return If found, an {@link Iso8583FMS} message to send, else
	 *         <code>null</code>.
	 * 
	 * @throws Exception
	 */
	public Iso8583FMS poll() throws Exception
	{
		// Check (and populate if necessary) the cached queue. False means nothing
		// to do (either because the queue is empty, or the maximum number of
		// in-flight transactions has been reached)
		if (!peek())
		{
			return null;
		}
		// Don't do this from multiple threads, and not while peeking either
		synchronized(safLock)
		{
			// Work with the next one off the queue, if there's something. We check
			// the size because this queue will block if there's nothing in it
			SAFQTran record = safQ.peek();
			if (record != null)
			{
				// If a record is found, decrypt the message data.
				Iso8583FMS reversal = new Iso8583FMS();
				String encrypted = record.getMsgData();
				byte[] decrypted = secMgr.decrypt(encrypted);
				reversal.fromMsg(decrypted);
				// Update the retry counter.
				int remaining = record.getRetriesRemaining() - 1;
				record.setRetriesRemaining(remaining);
				insertOrUpdate(reversal, null, null, remaining, FALSE_NOT_SENT, FALSE_UPDATE);
				// Reversal is now in flight
				inFlight.put(record.id, record);
				reversal.setId(record.getId());
				return reversal;
			}
		}
		return null;
	}
	
	/**
	 * Handle a reversal response:
	 * <ul><li>Remove it from "in flight" collection
	 * <li>Update the database to reflect the completion status
	 * @param msg The reversal response message to process
	 * @throws Exception
	 */
	public void process(Iso8583FMS msg) throws Exception
	{
		String id = OriginalReqCache.constructKey(msg, 0, null);
		// Remove from the queue
		synchronized(safLock)
		{
			SAFQTran tran = inFlight.remove(id);
			if (tran == null)
			{
				logger.debug("Transaction was not in in-flight collection!", id);
			}
			else if (!safQ.remove(tran))
			{
				logger.debug("Transaction was not in the safq!", tran);
			}
			else
			{
				currentLength--;
			}
		}		
		// Update the 0420 in the SAFQ
		insertOrUpdate(
				msg, 
				msg,
				null,
				-1, 
				StoreAndForwardQueue.TRUE_SENT, 
				StoreAndForwardQueue.FALSE_UPDATE);
	}
	
	/**
	 * Called when a timeout occurs waiting for a response to a store-and-forward
	 * message
	 * <br>Removed from the in-flight list, 
	 * @param id The identity of the timeout out reversal
	 * @return A reversal message to send - could be the same one, or it could be a new one
	 * @throws Exception 
	 */
	public void processTimeout(String id) throws Exception
	{
		// Remove from in-flight collection and update the remaining attempts
		SAFQTran tran = inFlight.remove(id);
		if (tran == null)
		{
			throw new XPostilion(
				new ApplicationError(new String[]{"A timeout occurred for a store-and-forward transaction with identity "
					+ id + " that wasn't listed as in-flight"}));
		}
		// If we've exhausted the retries then dump it, otherwise leave it on the
		// queue to try again
		if (tran.getRetriesRemaining() <= 0)
		{
			logger.recordEvent(new ApplicationWarning(
				new String[]{"FMS repeatedly failed to respond to a store-and-forward transaction with identity " + id
					+ ". The transaction has been aborted."}));
			synchronized(safLock)
			{
				if (!safQ.remove(tran))
				{
					logger.debug("Timed out transaction was not in safq!");
				}
			}
			// This aborted transaction isn't in the queue now
			currentLength--;
			// Make sure it isn't loaded again
			StoreAndForwardQueueDao.updateRetriesRemaining(sinkNodeName, id, -1);
		}
		else
		{
			// Warn of the timeout
			logger.recordEvent(new ApplicationWarning(new String[]{"A store-and-forward transaction with identity "
				+ id + " was not responded to within the configured timeout (" + config.safqTimeout + "ms)"}));
		}
	}
	
	/**
	 * Provides callers with the current SAF q length. The length is initially
	 * maintained from the database and then tracked locally to avoid too many
	 * calls to the database (previously, regular SELECT COUNT(*) calls were
	 * made)
	 * 
	 * @return Length of the queue
	 * @throws Exception 
	 */
	public long getLength() throws Exception
	{
		synchronized(safLock)
		{
			if (currentLength == -1)
			{
				currentLength = StoreAndForwardQueueDao.getSAFQRecordCount(environment.getSinkNodeName());
			}
		}
		return currentLength;
	}
	
	public static final String		SAFQ_DATETIME_FORMAT	= "yyMMdd HH:mm:sss.sss";
	public static final boolean	TRUE_SENT				= true;
	public static final boolean	FALSE_NOT_SENT			= false;
	public static final boolean	TRUE_INSERT				= true;
	public static final boolean	FALSE_UPDATE			= false;
}