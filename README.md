# FraudIntegrationNid

## Description

The Fraud Integration Node Integration Driver is a NID designed to intercept transactions from Transaction Manager destined for a remote entity.
Upon receipt of a qualifying transaction, a fraud request message will be generated and sent to the Fraud Management System, directly from within the NID, for processing.
If the fraud check passes, the original transaction will be forwarded to the sink, if not, the NID will decline the original transaction and return it to Transaction Manager.

This NID is also responsible for managing reversal messages to the Fraud Management System.
Reversals will be generated and sent if the sink entity declines a transaction for which a fraud check was performed or if the source entity reverses a previous transaction.
These transactions are served by a Store & Forward Queue integrated into the NID.

The implementation includes a "versioned" class approach to manage message construction/processing/mapping.
Each change/update/fix will be placed in a new Mapper_v# class, allowing the mapper class to be loaded using the 'RELOADMAPPER' command without restarting the service associated to the NID.

## Additional Notes
- The NID's functionality is derived from and should honor the functionality contained within the Control Fraudes interchange. A link to this original source has been placed in a zip file in the [techspec folder](./doc/techspec/ctrlraudes_source20170906.zip)
- On the 19th of October 2018 the customer requested a copy of the source code. This copy of the code was previously committed to the repo as a zip file (7-eleven-fraud-nid-src-2018-10-19-16.01.42.84.zip). This has been moved to [SharePoint](https://stanchionza.sharepoint.com/:f:/g/americas/El0Vqc1w-m1Pk62IY-X_AwEBhI1jpoi-OcCSbDdTNCDP5Q?e=cAcBd1). The aforementioned zip file was removed from the repo in commit <b>506cf3b</b>.
