:: Creates a zip file with the right content for providing the source to the client.
:: We exclude Stanchion's proprietary build tools, technical specs and so on
:: Uses 7z.exe from 7zip (7zip.org) - go get and install it first
@ECHO OFF
SETLOCAL
ECHO %DATE:/=.%-%TIME::=.%
SET archiveName=7-eleven-fraud-nid-src-%DATE:/=.%-%TIME::=.%.zip
SET PATH=%PATH%;%ProgramFiles%\7-zip
IF EXIST %archiveName% DEL %archiveName%
7z a -r -tzip -x!.git -x!build -x!*.zip -x!bin -x!.* -x!build.py -x!build.cmd -x!build_output.txt -x!prepare* -x!SystemInfo.txt -x!applyEvents.cmd -x!copy_classes.cmd -x!README.md -x!doc\techspec %archiveName%
ENDLOCAL
